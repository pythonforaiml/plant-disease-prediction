{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e430a9b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import streamlit as st\n",
    "from PIL import Image\n",
    "import matplotlib.pyplot as plt\n",
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "from tensorflow import keras\n",
    "from tensorflow.keras.models import load_model\n",
    "from tensorflow.keras import preprocessing\n",
    "import time\n",
    "fig = plt.figure()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e875d5e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"custom.css\") as f:\n",
    "    st.markdown(f\"<style>{f.read()}</style>\", unsafe_allow_html=True)\n",
    "\n",
    "st.title('Bag Classifier')\n",
    "\n",
    "st.markdown(\"Welcome to this application that classifies tomato plant disease. The plants are classified into ten different classes namely: Tomato Bacterial spot, Tomato Early blight, Tomato Late blight,Tomato Leaf Mold,Tomato Septoria leaf spot,Tomato_Spider_mites_Two spotted spider mite,Tomato Target Spot,Tomato Tomato YellowLeaf Curl Virus,Tomato Tomato mosaic virus and Tomato healthy.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ffd1664a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def main():\n",
    "    file_uploaded = st.file_uploader(\"Choose File\", type=[\"png\",\"jpg\",\"jpeg\"])\n",
    "    class_btn = st.button(\"Classify\")\n",
    "    if file_uploaded is not None:    \n",
    "        image = Image.open(file_uploaded)\n",
    "        st.image(image, caption='Uploaded Image', use_column_width=True)\n",
    "        \n",
    "    if class_btn:\n",
    "        if file_uploaded is None:\n",
    "            st.write(\"Invalid command, please upload an image\")\n",
    "        else:\n",
    "            with st.spinner('Model working....'):\n",
    "                plt.imshow(image)\n",
    "                plt.axis(\"off\")\n",
    "                predictions = predict(image)\n",
    "                time.sleep(1)\n",
    "                st.success('Classified')\n",
    "                st.write(predictions)\n",
    "                st.pyplot(fig)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e14a062d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def predict(image):\n",
    "    classifier_model = \"base_dir.h5\"\n",
    "    IMAGE_SHAPE = (224, 224,3)\n",
    "    model = load_model(classifier_model, compile=False)\n",
    "    test_image = image.resize((224,224))\n",
    "    test_image = preprocessing.image.img_to_array(test_image)\n",
    "    test_image = test_image / 255.0\n",
    "    test_image = np.expand_dims(test_image, axis=0)\n",
    "    class_names = ['Tomato_Bacterial_spot','Tomato_Early_blight','Tomato_Late_blight','Tomato_Leaf_Mold','Tomato_Septoria_leaf_spot','Tomato_Spider_mites_Two_spotted_spider_mite','Tomato__Target_Spot','Tomato__Tomato_YellowLeaf__Curl_Virus','Tomato__Tomato_mosaic_virus','Tomato_healthy']\n",
    "    predictions = model.predict(test_image)\n",
    "    scores = tf.nn.softmax(predictions[0])\n",
    "    scores = scores.numpy()\n",
    "    results = {\n",
    "          'Backpack': 0,\n",
    "          'Briefcase': 0,\n",
    "          'Duffle': 0, \n",
    "          'Handbag': 0, \n",
    "          'Purse': 0\n",
    "}\n",
    "    result = f\"{class_names[np.argmax(scores)]} with a { (100 * np.max(scores)).round(2) } % confidence.\" \n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3395dbea",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d93b4ac1",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.9 (tensorflow)",
   "language": "python",
   "name": "tensorflow"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
