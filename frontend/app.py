#!/usr/bin/env python
# coding: utf-8

# In[40]:


import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder
import streamlit as st
from PIL import Image


# In[35]:

def run():
    st.title('Tomato Plant Disease Classifier')

    st.markdown("Welcome to this application that classifies tomato plant disease. The plants are classified into ten different classes namely: Tomato Bacterial spot, Tomato Early blight, Tomato Late blight,Tomato Leaf Mold,Tomato Septoria leaf spot,Tomato_Spider_mites_Two spotted spider mite,Tomato Target Spot,Tomato Tomato YellowLeaf Curl Virus,Tomato Tomato mosaic virus and Tomato healthy.")


    # In[36]:


    # displays a file uploader widget
    image = st.file_uploader("Choose an image")


    # In[56]:


    server_url = 'https://us-central1-tomato-disease-prediction.cloudfunctions.net/predict'
    # displays a button
    if st.button("Predict"):
        if image is not None:
            m = MultipartEncoder(fields={'file': ('filename', image, 'image/jpeg')})
            r = requests.post(server_url,data=m,headers={'Content-Type': m.content_type},timeout=8000)
            results = r.json()
            cname = results.get('class')
            cname = cname.replace('_',' ')
            cname = cname.replace('Tomato','')
            con = results.get('confidence')
            col1, col2 = st.columns(2)
            col1.header("Image")
            col1.image(image, width=300)
            col2.header("Prediction")
            output = f'<p style="font-family:sans-serif; font-size: 20px;">Predicted Class:{cname}</p>\n <p style="font-family:sans-serif; font-size: 20px;">Confidence: {con}</p>'
            col2.markdown(f"{output}",unsafe_allow_html=True)
       
    
if __name__ == "__main__":
    run()


