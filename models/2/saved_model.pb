��"
��
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
�
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 ":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
�
ResizeBilinear
images"T
size
resized_images"
Ttype:
2	"
align_cornersbool( "
half_pixel_centersbool( 
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
9
Softmax
logits"T
softmax"T"
Ttype:
2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.02unknown8�� 
�
conv2d_72/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: *!
shared_nameconv2d_72/kernel
}
$conv2d_72/kernel/Read/ReadVariableOpReadVariableOpconv2d_72/kernel*&
_output_shapes
: *
dtype0
t
conv2d_72/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameconv2d_72/bias
m
"conv2d_72/bias/Read/ReadVariableOpReadVariableOpconv2d_72/bias*
_output_shapes
: *
dtype0
�
conv2d_73/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: @*!
shared_nameconv2d_73/kernel
}
$conv2d_73/kernel/Read/ReadVariableOpReadVariableOpconv2d_73/kernel*&
_output_shapes
: @*
dtype0
t
conv2d_73/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_nameconv2d_73/bias
m
"conv2d_73/bias/Read/ReadVariableOpReadVariableOpconv2d_73/bias*
_output_shapes
:@*
dtype0
�
conv2d_74/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@@*!
shared_nameconv2d_74/kernel
}
$conv2d_74/kernel/Read/ReadVariableOpReadVariableOpconv2d_74/kernel*&
_output_shapes
:@@*
dtype0
t
conv2d_74/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_nameconv2d_74/bias
m
"conv2d_74/bias/Read/ReadVariableOpReadVariableOpconv2d_74/bias*
_output_shapes
:@*
dtype0
|
dense_38/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��@* 
shared_namedense_38/kernel
u
#dense_38/kernel/Read/ReadVariableOpReadVariableOpdense_38/kernel* 
_output_shapes
:
��@*
dtype0
r
dense_38/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_38/bias
k
!dense_38/bias/Read/ReadVariableOpReadVariableOpdense_38/bias*
_output_shapes
:@*
dtype0
z
dense_39/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@
* 
shared_namedense_39/kernel
s
#dense_39/kernel/Read/ReadVariableOpReadVariableOpdense_39/kernel*
_output_shapes

:@
*
dtype0
r
dense_39/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namedense_39/bias
k
!dense_39/bias/Read/ReadVariableOpReadVariableOpdense_39/bias*
_output_shapes
:
*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
h
VariableVarHandleOp*
_output_shapes
: *
dtype0	*
shape:*
shared_name
Variable
a
Variable/Read/ReadVariableOpReadVariableOpVariable*
_output_shapes
:*
dtype0	
l

Variable_1VarHandleOp*
_output_shapes
: *
dtype0	*
shape:*
shared_name
Variable_1
e
Variable_1/Read/ReadVariableOpReadVariableOp
Variable_1*
_output_shapes
:*
dtype0	
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/conv2d_72/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameAdam/conv2d_72/kernel/m
�
+Adam/conv2d_72/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_72/kernel/m*&
_output_shapes
: *
dtype0
�
Adam/conv2d_72/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: *&
shared_nameAdam/conv2d_72/bias/m
{
)Adam/conv2d_72/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_72/bias/m*
_output_shapes
: *
dtype0
�
Adam/conv2d_73/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: @*(
shared_nameAdam/conv2d_73/kernel/m
�
+Adam/conv2d_73/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_73/kernel/m*&
_output_shapes
: @*
dtype0
�
Adam/conv2d_73/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*&
shared_nameAdam/conv2d_73/bias/m
{
)Adam/conv2d_73/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_73/bias/m*
_output_shapes
:@*
dtype0
�
Adam/conv2d_74/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@@*(
shared_nameAdam/conv2d_74/kernel/m
�
+Adam/conv2d_74/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_74/kernel/m*&
_output_shapes
:@@*
dtype0
�
Adam/conv2d_74/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*&
shared_nameAdam/conv2d_74/bias/m
{
)Adam/conv2d_74/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv2d_74/bias/m*
_output_shapes
:@*
dtype0
�
Adam/dense_38/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��@*'
shared_nameAdam/dense_38/kernel/m
�
*Adam/dense_38/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_38/kernel/m* 
_output_shapes
:
��@*
dtype0
�
Adam/dense_38/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_nameAdam/dense_38/bias/m
y
(Adam/dense_38/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_38/bias/m*
_output_shapes
:@*
dtype0
�
Adam/dense_39/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@
*'
shared_nameAdam/dense_39/kernel/m
�
*Adam/dense_39/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_39/kernel/m*
_output_shapes

:@
*
dtype0
�
Adam/dense_39/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_39/bias/m
y
(Adam/dense_39/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_39/bias/m*
_output_shapes
:
*
dtype0
�
Adam/conv2d_72/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameAdam/conv2d_72/kernel/v
�
+Adam/conv2d_72/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_72/kernel/v*&
_output_shapes
: *
dtype0
�
Adam/conv2d_72/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: *&
shared_nameAdam/conv2d_72/bias/v
{
)Adam/conv2d_72/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_72/bias/v*
_output_shapes
: *
dtype0
�
Adam/conv2d_73/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: @*(
shared_nameAdam/conv2d_73/kernel/v
�
+Adam/conv2d_73/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_73/kernel/v*&
_output_shapes
: @*
dtype0
�
Adam/conv2d_73/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*&
shared_nameAdam/conv2d_73/bias/v
{
)Adam/conv2d_73/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_73/bias/v*
_output_shapes
:@*
dtype0
�
Adam/conv2d_74/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@@*(
shared_nameAdam/conv2d_74/kernel/v
�
+Adam/conv2d_74/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_74/kernel/v*&
_output_shapes
:@@*
dtype0
�
Adam/conv2d_74/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*&
shared_nameAdam/conv2d_74/bias/v
{
)Adam/conv2d_74/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv2d_74/bias/v*
_output_shapes
:@*
dtype0
�
Adam/dense_38/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��@*'
shared_nameAdam/dense_38/kernel/v
�
*Adam/dense_38/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_38/kernel/v* 
_output_shapes
:
��@*
dtype0
�
Adam/dense_38/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*%
shared_nameAdam/dense_38/bias/v
y
(Adam/dense_38/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_38/bias/v*
_output_shapes
:@*
dtype0
�
Adam/dense_39/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@
*'
shared_nameAdam/dense_39/kernel/v
�
*Adam/dense_39/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_39/kernel/v*
_output_shapes

:@
*
dtype0
�
Adam/dense_39/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_39/bias/v
y
(Adam/dense_39/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_39/bias/v*
_output_shapes
:
*
dtype0

NoOpNoOp
�P
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�P
value�OB�O B�O
�
layer-0
layer-1
layer_with_weights-0
layer-2
layer-3
layer_with_weights-1
layer-4
layer-5
layer_with_weights-2
layer-6
layer-7
	layer-8

layer_with_weights-3

layer-9
layer_with_weights-4
layer-10
	optimizer
trainable_variables
	variables
regularization_losses
	keras_api

signatures
l
layer-0
layer-1
trainable_variables
	variables
regularization_losses
	keras_api
l
layer-0
layer-1
trainable_variables
	variables
regularization_losses
	keras_api
h

kernel
bias
 trainable_variables
!	variables
"regularization_losses
#	keras_api
R
$trainable_variables
%	variables
&regularization_losses
'	keras_api
h

(kernel
)bias
*trainable_variables
+	variables
,regularization_losses
-	keras_api
R
.trainable_variables
/	variables
0regularization_losses
1	keras_api
h

2kernel
3bias
4trainable_variables
5	variables
6regularization_losses
7	keras_api
R
8trainable_variables
9	variables
:regularization_losses
;	keras_api
R
<trainable_variables
=	variables
>regularization_losses
?	keras_api
h

@kernel
Abias
Btrainable_variables
C	variables
Dregularization_losses
E	keras_api
h

Fkernel
Gbias
Htrainable_variables
I	variables
Jregularization_losses
K	keras_api
�
Liter

Mbeta_1

Nbeta_2
	Odecay
Plearning_ratem�m�(m�)m�2m�3m�@m�Am�Fm�Gm�v�v�(v�)v�2v�3v�@v�Av�Fv�Gv�
F
0
1
(2
)3
24
35
@6
A7
F8
G9
F
0
1
(2
)3
24
35
@6
A7
F8
G9
 
�
trainable_variables
Qmetrics
Rlayer_metrics
Snon_trainable_variables
	variables
regularization_losses

Tlayers
Ulayer_regularization_losses
 
R
Vtrainable_variables
W	variables
Xregularization_losses
Y	keras_api
R
Ztrainable_variables
[	variables
\regularization_losses
]	keras_api
 
 
 
�
trainable_variables
^metrics
_layer_metrics
`non_trainable_variables
	variables
regularization_losses

alayers
blayer_regularization_losses
\
c_rng
dtrainable_variables
e	variables
fregularization_losses
g	keras_api
\
h_rng
itrainable_variables
j	variables
kregularization_losses
l	keras_api
 
 
 
�
trainable_variables
mmetrics
nlayer_metrics
onon_trainable_variables
	variables
regularization_losses

players
qlayer_regularization_losses
\Z
VARIABLE_VALUEconv2d_72/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEconv2d_72/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
�
 trainable_variables
rmetrics
slayer_metrics
tnon_trainable_variables
!	variables
"regularization_losses

ulayers
vlayer_regularization_losses
 
 
 
�
$trainable_variables
wmetrics
xlayer_metrics
ynon_trainable_variables
%	variables
&regularization_losses

zlayers
{layer_regularization_losses
\Z
VARIABLE_VALUEconv2d_73/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEconv2d_73/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

(0
)1

(0
)1
 
�
*trainable_variables
|metrics
}layer_metrics
~non_trainable_variables
+	variables
,regularization_losses

layers
 �layer_regularization_losses
 
 
 
�
.trainable_variables
�metrics
�layer_metrics
�non_trainable_variables
/	variables
0regularization_losses
�layers
 �layer_regularization_losses
\Z
VARIABLE_VALUEconv2d_74/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEconv2d_74/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

20
31

20
31
 
�
4trainable_variables
�metrics
�layer_metrics
�non_trainable_variables
5	variables
6regularization_losses
�layers
 �layer_regularization_losses
 
 
 
�
8trainable_variables
�metrics
�layer_metrics
�non_trainable_variables
9	variables
:regularization_losses
�layers
 �layer_regularization_losses
 
 
 
�
<trainable_variables
�metrics
�layer_metrics
�non_trainable_variables
=	variables
>regularization_losses
�layers
 �layer_regularization_losses
[Y
VARIABLE_VALUEdense_38/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_38/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

@0
A1

@0
A1
 
�
Btrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
C	variables
Dregularization_losses
�layers
 �layer_regularization_losses
[Y
VARIABLE_VALUEdense_39/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_39/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE

F0
G1

F0
G1
 
�
Htrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
I	variables
Jregularization_losses
�layers
 �layer_regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
 
 
N
0
1
2
3
4
5
6
7
	8

9
10
 
 
 
 
�
Vtrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
W	variables
Xregularization_losses
�layers
 �layer_regularization_losses
 
 
 
�
Ztrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
[	variables
\regularization_losses
�layers
 �layer_regularization_losses
 
 
 

0
1
 

�
_state_var
 
 
 
�
dtrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
e	variables
fregularization_losses
�layers
 �layer_regularization_losses

�
_state_var
 
 
 
�
itrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
j	variables
kregularization_losses
�layers
 �layer_regularization_losses
 
 
 

0
1
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
 
 
 
 
 
 
 
 
 
 
XV
VARIABLE_VALUEVariable:layer-1/layer-0/_rng/_state_var/.ATTRIBUTES/VARIABLE_VALUE
 
 
 
 
 
ZX
VARIABLE_VALUE
Variable_1:layer-1/layer-1/_rng/_state_var/.ATTRIBUTES/VARIABLE_VALUE
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
}
VARIABLE_VALUEAdam/conv2d_72/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_72/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/conv2d_73/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_73/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/conv2d_74/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_74/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_38/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_38/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_39/kernel/mRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_39/bias/mPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/conv2d_72/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_72/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/conv2d_73/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_73/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUEAdam/conv2d_74/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/conv2d_74/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_38/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_38/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_39/kernel/vRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_39/bias/vPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
#serving_default_sequential_11_inputPlaceholder*1
_output_shapes
:�����������*
dtype0*&
shape:�����������
�
StatefulPartitionedCallStatefulPartitionedCall#serving_default_sequential_11_inputconv2d_72/kernelconv2d_72/biasconv2d_73/kernelconv2d_73/biasconv2d_74/kernelconv2d_74/biasdense_38/kerneldense_38/biasdense_39/kerneldense_39/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*,
_read_only_resource_inputs

	
*/
config_proto

CPU

GPU 2J 8@� *,
f'R%
#__inference_signature_wrapper_61037
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename$conv2d_72/kernel/Read/ReadVariableOp"conv2d_72/bias/Read/ReadVariableOp$conv2d_73/kernel/Read/ReadVariableOp"conv2d_73/bias/Read/ReadVariableOp$conv2d_74/kernel/Read/ReadVariableOp"conv2d_74/bias/Read/ReadVariableOp#dense_38/kernel/Read/ReadVariableOp!dense_38/bias/Read/ReadVariableOp#dense_39/kernel/Read/ReadVariableOp!dense_39/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOpVariable/Read/ReadVariableOpVariable_1/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp+Adam/conv2d_72/kernel/m/Read/ReadVariableOp)Adam/conv2d_72/bias/m/Read/ReadVariableOp+Adam/conv2d_73/kernel/m/Read/ReadVariableOp)Adam/conv2d_73/bias/m/Read/ReadVariableOp+Adam/conv2d_74/kernel/m/Read/ReadVariableOp)Adam/conv2d_74/bias/m/Read/ReadVariableOp*Adam/dense_38/kernel/m/Read/ReadVariableOp(Adam/dense_38/bias/m/Read/ReadVariableOp*Adam/dense_39/kernel/m/Read/ReadVariableOp(Adam/dense_39/bias/m/Read/ReadVariableOp+Adam/conv2d_72/kernel/v/Read/ReadVariableOp)Adam/conv2d_72/bias/v/Read/ReadVariableOp+Adam/conv2d_73/kernel/v/Read/ReadVariableOp)Adam/conv2d_73/bias/v/Read/ReadVariableOp+Adam/conv2d_74/kernel/v/Read/ReadVariableOp)Adam/conv2d_74/bias/v/Read/ReadVariableOp*Adam/dense_38/kernel/v/Read/ReadVariableOp(Adam/dense_38/bias/v/Read/ReadVariableOp*Adam/dense_39/kernel/v/Read/ReadVariableOp(Adam/dense_39/bias/v/Read/ReadVariableOpConst*6
Tin/
-2+			*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *'
f"R 
__inference__traced_save_62965
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameconv2d_72/kernelconv2d_72/biasconv2d_73/kernelconv2d_73/biasconv2d_74/kernelconv2d_74/biasdense_38/kerneldense_38/biasdense_39/kerneldense_39/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_rateVariable
Variable_1totalcounttotal_1count_1Adam/conv2d_72/kernel/mAdam/conv2d_72/bias/mAdam/conv2d_73/kernel/mAdam/conv2d_73/bias/mAdam/conv2d_74/kernel/mAdam/conv2d_74/bias/mAdam/dense_38/kernel/mAdam/dense_38/bias/mAdam/dense_39/kernel/mAdam/dense_39/bias/mAdam/conv2d_72/kernel/vAdam/conv2d_72/bias/vAdam/conv2d_73/kernel/vAdam/conv2d_73/bias/vAdam/conv2d_74/kernel/vAdam/conv2d_74/bias/vAdam/dense_38/kernel/vAdam/dense_38/bias/vAdam/dense_39/kernel/vAdam/dense_39/bias/v*5
Tin.
,2**
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� **
f%R#
!__inference__traced_restore_63098��
�
g
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_60580

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
MaxPool�
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
L
0__inference_max_pooling2d_73_layer_call_fn_62419

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������>>@* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_606462
PartitionedCallt
IdentityIdentityPartitionedCall:output:0*
T0*/
_output_shapes
:���������>>@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������||@:W S
/
_output_shapes
:���������||@
 
_user_specified_nameinputs
�
d
H__inference_random_flip_1_layer_call_and_return_conditional_losses_60207

inputs
identityd
IdentityIdentityinputs*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
)__inference_conv2d_74_layer_call_fn_62438

inputs!
unknown:@@
	unknown_0:@
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<<@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_74_layer_call_and_return_conditional_losses_606592
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������<<@2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������>>@: : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:���������>>@
 
_user_specified_nameinputs
�	
d
H__inference_sequential_11_layer_call_and_return_conditional_losses_60148

inputs
identity�
resizing_1/PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *N
fIRG
E__inference_resizing_1_layer_call_and_return_conditional_losses_601352
resizing_1/PartitionedCall�
rescaling_1/PartitionedCallPartitionedCall#resizing_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *O
fJRH
F__inference_rescaling_1_layer_call_and_return_conditional_losses_601452
rescaling_1/PartitionedCall�
IdentityIdentity$rescaling_1/PartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�

d
H__inference_sequential_11_layer_call_and_return_conditional_losses_61835

inputs
identity�
resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2
resizing_1/resize/size�
 resizing_1/resize/ResizeBilinearResizeBilinearinputsresizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(2"
 resizing_1/resize/ResizeBilinearm
rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2
rescaling_1/Cast/xq
rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2
rescaling_1/Cast_1/x�
rescaling_1/mulMul1resizing_1/resize/ResizeBilinear:resized_images:0rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2
rescaling_1/mul�
rescaling_1/addAddV2rescaling_1/mul:z:0rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
rescaling_1/addq
IdentityIdentityrescaling_1/add:z:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
b
F__inference_rescaling_1_layer_call_and_return_conditional_losses_62544

inputs
identityU
Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2
Cast/xY
Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2

Cast_1/xf
mulMulinputsCast/x:output:0*
T0*1
_output_shapes
:�����������2
mulk
addAddV2mul:z:0Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
adde
IdentityIdentityadd:z:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
��
�
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_62809

inputs6
(stateful_uniform_rngreadandskip_resource:	
identity��stateful_uniform/RngReadAndSkipD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice�
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������2
strided_slice_1/stack�
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1^
CastCaststrided_slice_1:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast�
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������2
strided_slice_2/stack�
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2b
Cast_1Caststrided_slice_2:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1~
stateful_uniform/shapePackstrided_slice:output:0*
N*
T0*
_output_shapes
:2
stateful_uniform/shapeq
stateful_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *|٠�2
stateful_uniform/minq
stateful_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *|٠?2
stateful_uniform/maxz
stateful_uniform/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
stateful_uniform/Const�
stateful_uniform/ProdProdstateful_uniform/shape:output:0stateful_uniform/Const:output:0*
T0*
_output_shapes
: 2
stateful_uniform/Prodt
stateful_uniform/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2
stateful_uniform/Cast/x�
stateful_uniform/Cast_1Caststateful_uniform/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
stateful_uniform/Cast_1�
stateful_uniform/RngReadAndSkipRngReadAndSkip(stateful_uniform_rngreadandskip_resource stateful_uniform/Cast/x:output:0stateful_uniform/Cast_1:y:0*
_output_shapes
:2!
stateful_uniform/RngReadAndSkip�
$stateful_uniform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$stateful_uniform/strided_slice/stack�
&stateful_uniform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&stateful_uniform/strided_slice/stack_1�
&stateful_uniform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&stateful_uniform/strided_slice/stack_2�
stateful_uniform/strided_sliceStridedSlice'stateful_uniform/RngReadAndSkip:value:0-stateful_uniform/strided_slice/stack:output:0/stateful_uniform/strided_slice/stack_1:output:0/stateful_uniform/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2 
stateful_uniform/strided_slice�
stateful_uniform/BitcastBitcast'stateful_uniform/strided_slice:output:0*
T0	*
_output_shapes
:*

type02
stateful_uniform/Bitcast�
&stateful_uniform/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&stateful_uniform/strided_slice_1/stack�
(stateful_uniform/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(stateful_uniform/strided_slice_1/stack_1�
(stateful_uniform/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(stateful_uniform/strided_slice_1/stack_2�
 stateful_uniform/strided_slice_1StridedSlice'stateful_uniform/RngReadAndSkip:value:0/stateful_uniform/strided_slice_1/stack:output:01stateful_uniform/strided_slice_1/stack_1:output:01stateful_uniform/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2"
 stateful_uniform/strided_slice_1�
stateful_uniform/Bitcast_1Bitcast)stateful_uniform/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02
stateful_uniform/Bitcast_1�
-stateful_uniform/StatelessRandomUniformV2/algConst*
_output_shapes
: *
dtype0*
value	B :2/
-stateful_uniform/StatelessRandomUniformV2/alg�
)stateful_uniform/StatelessRandomUniformV2StatelessRandomUniformV2stateful_uniform/shape:output:0#stateful_uniform/Bitcast_1:output:0!stateful_uniform/Bitcast:output:06stateful_uniform/StatelessRandomUniformV2/alg:output:0*#
_output_shapes
:���������2+
)stateful_uniform/StatelessRandomUniformV2�
stateful_uniform/subSubstateful_uniform/max:output:0stateful_uniform/min:output:0*
T0*
_output_shapes
: 2
stateful_uniform/sub�
stateful_uniform/mulMul2stateful_uniform/StatelessRandomUniformV2:output:0stateful_uniform/sub:z:0*
T0*#
_output_shapes
:���������2
stateful_uniform/mul�
stateful_uniformAddV2stateful_uniform/mul:z:0stateful_uniform/min:output:0*
T0*#
_output_shapes
:���������2
stateful_uniforms
rotation_matrix/sub/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub/y~
rotation_matrix/subSub
Cast_1:y:0rotation_matrix/sub/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/subu
rotation_matrix/CosCosstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Cosw
rotation_matrix/sub_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_1/y�
rotation_matrix/sub_1Sub
Cast_1:y:0 rotation_matrix/sub_1/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_1�
rotation_matrix/mulMulrotation_matrix/Cos:y:0rotation_matrix/sub_1:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/mulu
rotation_matrix/SinSinstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Sinw
rotation_matrix/sub_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_2/y�
rotation_matrix/sub_2SubCast:y:0 rotation_matrix/sub_2/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_2�
rotation_matrix/mul_1Mulrotation_matrix/Sin:y:0rotation_matrix/sub_2:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/mul_1�
rotation_matrix/sub_3Subrotation_matrix/mul:z:0rotation_matrix/mul_1:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/sub_3�
rotation_matrix/sub_4Subrotation_matrix/sub:z:0rotation_matrix/sub_3:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/sub_4{
rotation_matrix/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2
rotation_matrix/truediv/y�
rotation_matrix/truedivRealDivrotation_matrix/sub_4:z:0"rotation_matrix/truediv/y:output:0*
T0*#
_output_shapes
:���������2
rotation_matrix/truedivw
rotation_matrix/sub_5/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_5/y�
rotation_matrix/sub_5SubCast:y:0 rotation_matrix/sub_5/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_5y
rotation_matrix/Sin_1Sinstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Sin_1w
rotation_matrix/sub_6/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_6/y�
rotation_matrix/sub_6Sub
Cast_1:y:0 rotation_matrix/sub_6/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_6�
rotation_matrix/mul_2Mulrotation_matrix/Sin_1:y:0rotation_matrix/sub_6:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/mul_2y
rotation_matrix/Cos_1Cosstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Cos_1w
rotation_matrix/sub_7/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_7/y�
rotation_matrix/sub_7SubCast:y:0 rotation_matrix/sub_7/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_7�
rotation_matrix/mul_3Mulrotation_matrix/Cos_1:y:0rotation_matrix/sub_7:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/mul_3�
rotation_matrix/addAddV2rotation_matrix/mul_2:z:0rotation_matrix/mul_3:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/add�
rotation_matrix/sub_8Subrotation_matrix/sub_5:z:0rotation_matrix/add:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/sub_8
rotation_matrix/truediv_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2
rotation_matrix/truediv_1/y�
rotation_matrix/truediv_1RealDivrotation_matrix/sub_8:z:0$rotation_matrix/truediv_1/y:output:0*
T0*#
_output_shapes
:���������2
rotation_matrix/truediv_1r
rotation_matrix/ShapeShapestateful_uniform:z:0*
T0*
_output_shapes
:2
rotation_matrix/Shape�
#rotation_matrix/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2%
#rotation_matrix/strided_slice/stack�
%rotation_matrix/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2'
%rotation_matrix/strided_slice/stack_1�
%rotation_matrix/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2'
%rotation_matrix/strided_slice/stack_2�
rotation_matrix/strided_sliceStridedSlicerotation_matrix/Shape:output:0,rotation_matrix/strided_slice/stack:output:0.rotation_matrix/strided_slice/stack_1:output:0.rotation_matrix/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
rotation_matrix/strided_slicey
rotation_matrix/Cos_2Cosstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Cos_2�
%rotation_matrix/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_1/stack�
'rotation_matrix/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_1/stack_1�
'rotation_matrix/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_1/stack_2�
rotation_matrix/strided_slice_1StridedSlicerotation_matrix/Cos_2:y:0.rotation_matrix/strided_slice_1/stack:output:00rotation_matrix/strided_slice_1/stack_1:output:00rotation_matrix/strided_slice_1/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_1y
rotation_matrix/Sin_2Sinstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Sin_2�
%rotation_matrix/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_2/stack�
'rotation_matrix/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_2/stack_1�
'rotation_matrix/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_2/stack_2�
rotation_matrix/strided_slice_2StridedSlicerotation_matrix/Sin_2:y:0.rotation_matrix/strided_slice_2/stack:output:00rotation_matrix/strided_slice_2/stack_1:output:00rotation_matrix/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_2�
rotation_matrix/NegNeg(rotation_matrix/strided_slice_2:output:0*
T0*'
_output_shapes
:���������2
rotation_matrix/Neg�
%rotation_matrix/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_3/stack�
'rotation_matrix/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_3/stack_1�
'rotation_matrix/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_3/stack_2�
rotation_matrix/strided_slice_3StridedSlicerotation_matrix/truediv:z:0.rotation_matrix/strided_slice_3/stack:output:00rotation_matrix/strided_slice_3/stack_1:output:00rotation_matrix/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_3y
rotation_matrix/Sin_3Sinstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Sin_3�
%rotation_matrix/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_4/stack�
'rotation_matrix/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_4/stack_1�
'rotation_matrix/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_4/stack_2�
rotation_matrix/strided_slice_4StridedSlicerotation_matrix/Sin_3:y:0.rotation_matrix/strided_slice_4/stack:output:00rotation_matrix/strided_slice_4/stack_1:output:00rotation_matrix/strided_slice_4/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_4y
rotation_matrix/Cos_3Cosstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Cos_3�
%rotation_matrix/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_5/stack�
'rotation_matrix/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_5/stack_1�
'rotation_matrix/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_5/stack_2�
rotation_matrix/strided_slice_5StridedSlicerotation_matrix/Cos_3:y:0.rotation_matrix/strided_slice_5/stack:output:00rotation_matrix/strided_slice_5/stack_1:output:00rotation_matrix/strided_slice_5/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_5�
%rotation_matrix/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_6/stack�
'rotation_matrix/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_6/stack_1�
'rotation_matrix/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_6/stack_2�
rotation_matrix/strided_slice_6StridedSlicerotation_matrix/truediv_1:z:0.rotation_matrix/strided_slice_6/stack:output:00rotation_matrix/strided_slice_6/stack_1:output:00rotation_matrix/strided_slice_6/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_6|
rotation_matrix/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
rotation_matrix/zeros/mul/y�
rotation_matrix/zeros/mulMul&rotation_matrix/strided_slice:output:0$rotation_matrix/zeros/mul/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/zeros/mul
rotation_matrix/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :�2
rotation_matrix/zeros/Less/y�
rotation_matrix/zeros/LessLessrotation_matrix/zeros/mul:z:0%rotation_matrix/zeros/Less/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/zeros/Less�
rotation_matrix/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :2 
rotation_matrix/zeros/packed/1�
rotation_matrix/zeros/packedPack&rotation_matrix/strided_slice:output:0'rotation_matrix/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
rotation_matrix/zeros/packed
rotation_matrix/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
rotation_matrix/zeros/Const�
rotation_matrix/zerosFill%rotation_matrix/zeros/packed:output:0$rotation_matrix/zeros/Const:output:0*
T0*'
_output_shapes
:���������2
rotation_matrix/zeros|
rotation_matrix/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
rotation_matrix/concat/axis�
rotation_matrix/concatConcatV2(rotation_matrix/strided_slice_1:output:0rotation_matrix/Neg:y:0(rotation_matrix/strided_slice_3:output:0(rotation_matrix/strided_slice_4:output:0(rotation_matrix/strided_slice_5:output:0(rotation_matrix/strided_slice_6:output:0rotation_matrix/zeros:output:0$rotation_matrix/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
rotation_matrix/concatX
transform/ShapeShapeinputs*
T0*
_output_shapes
:2
transform/Shape�
transform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
transform/strided_slice/stack�
transform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2!
transform/strided_slice/stack_1�
transform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2!
transform/strided_slice/stack_2�
transform/strided_sliceStridedSlicetransform/Shape:output:0&transform/strided_slice/stack:output:0(transform/strided_slice/stack_1:output:0(transform/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2
transform/strided_sliceq
transform/fill_valueConst*
_output_shapes
: *
dtype0*
valueB
 *    2
transform/fill_value�
$transform/ImageProjectiveTransformV3ImageProjectiveTransformV3inputsrotation_matrix/concat:output:0 transform/strided_slice:output:0transform/fill_value:output:0*1
_output_shapes
:�����������*
dtype0*
	fill_mode	REFLECT*
interpolation
BILINEAR2&
$transform/ImageProjectiveTransformV3�
IdentityIdentity9transform/ImageProjectiveTransformV3:transformed_images:0^NoOp*
T0*1
_output_shapes
:�����������2

Identityp
NoOpNoOp ^stateful_uniform/RngReadAndSkip*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:�����������: 2B
stateful_uniform/RngReadAndSkipstateful_uniform/RngReadAndSkip:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
C__inference_dense_39_layer_call_and_return_conditional_losses_60707

inputs0
matmul_readvariableop_resource:@
-
biasadd_readvariableop_resource:

identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2	
BiasAdda
SoftmaxSoftmaxBiasAdd:output:0*
T0*'
_output_shapes
:���������
2	
Softmaxl
IdentityIdentitySoftmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
D__inference_conv2d_72_layer_call_and_return_conditional_losses_60613

inputs8
conv2d_readvariableop_resource: -
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp�
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: *
dtype02
Conv2D/ReadVariableOp�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� *
paddingVALID*
strides
2
Conv2D�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� 2	
BiasAddb
ReluReluBiasAdd:output:0*
T0*1
_output_shapes
:����������� 2
Reluw
IdentityIdentityRelu:activations:0^NoOp*
T0*1
_output_shapes
:����������� 2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
I
-__inference_sequential_12_layer_call_fn_61865

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_12_layer_call_and_return_conditional_losses_602162
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
C__inference_dense_39_layer_call_and_return_conditional_losses_62520

inputs0
matmul_readvariableop_resource:@
-
biasadd_readvariableop_resource:

identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@
*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2	
BiasAdda
SoftmaxSoftmaxBiasAdd:output:0*
T0*'
_output_shapes
:���������
2	
Softmaxl
IdentityIdentitySoftmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
��
�
H__inference_sequential_23_layer_call_and_return_conditional_losses_61470

inputs[
Msequential_12_random_flip_1_stateful_uniform_full_int_rngreadandskip_resource:	V
Hsequential_12_random_rotation_1_stateful_uniform_rngreadandskip_resource:	B
(conv2d_72_conv2d_readvariableop_resource: 7
)conv2d_72_biasadd_readvariableop_resource: B
(conv2d_73_conv2d_readvariableop_resource: @7
)conv2d_73_biasadd_readvariableop_resource:@B
(conv2d_74_conv2d_readvariableop_resource:@@7
)conv2d_74_biasadd_readvariableop_resource:@;
'dense_38_matmul_readvariableop_resource:
��@6
(dense_38_biasadd_readvariableop_resource:@9
'dense_39_matmul_readvariableop_resource:@
6
(dense_39_biasadd_readvariableop_resource:

identity�� conv2d_72/BiasAdd/ReadVariableOp�conv2d_72/Conv2D/ReadVariableOp� conv2d_73/BiasAdd/ReadVariableOp�conv2d_73/Conv2D/ReadVariableOp� conv2d_74/BiasAdd/ReadVariableOp�conv2d_74/Conv2D/ReadVariableOp�dense_38/BiasAdd/ReadVariableOp�dense_38/MatMul/ReadVariableOp�dense_39/BiasAdd/ReadVariableOp�dense_39/MatMul/ReadVariableOp�Dsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip�Fsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip�ksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�rsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�hsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�osequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip�
$sequential_11/resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2&
$sequential_11/resizing_1/resize/size�
.sequential_11/resizing_1/resize/ResizeBilinearResizeBilinearinputs-sequential_11/resizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(20
.sequential_11/resizing_1/resize/ResizeBilinear�
 sequential_11/rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2"
 sequential_11/rescaling_1/Cast/x�
"sequential_11/rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2$
"sequential_11/rescaling_1/Cast_1/x�
sequential_11/rescaling_1/mulMul?sequential_11/resizing_1/resize/ResizeBilinear:resized_images:0)sequential_11/rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2
sequential_11/rescaling_1/mul�
sequential_11/rescaling_1/addAddV2!sequential_11/rescaling_1/mul:z:0+sequential_11/rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
sequential_11/rescaling_1/add�
;sequential_12/random_flip_1/stateful_uniform_full_int/shapeConst*
_output_shapes
:*
dtype0*
valueB:2=
;sequential_12/random_flip_1/stateful_uniform_full_int/shape�
;sequential_12/random_flip_1/stateful_uniform_full_int/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2=
;sequential_12/random_flip_1/stateful_uniform_full_int/Const�
:sequential_12/random_flip_1/stateful_uniform_full_int/ProdProdDsequential_12/random_flip_1/stateful_uniform_full_int/shape:output:0Dsequential_12/random_flip_1/stateful_uniform_full_int/Const:output:0*
T0*
_output_shapes
: 2<
:sequential_12/random_flip_1/stateful_uniform_full_int/Prod�
<sequential_12/random_flip_1/stateful_uniform_full_int/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2>
<sequential_12/random_flip_1/stateful_uniform_full_int/Cast/x�
<sequential_12/random_flip_1/stateful_uniform_full_int/Cast_1CastCsequential_12/random_flip_1/stateful_uniform_full_int/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2>
<sequential_12/random_flip_1/stateful_uniform_full_int/Cast_1�
Dsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkipRngReadAndSkipMsequential_12_random_flip_1_stateful_uniform_full_int_rngreadandskip_resourceEsequential_12/random_flip_1/stateful_uniform_full_int/Cast/x:output:0@sequential_12/random_flip_1/stateful_uniform_full_int/Cast_1:y:0*
_output_shapes
:2F
Dsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip�
Isequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2K
Isequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack�
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2M
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_1�
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2M
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_2�
Csequential_12/random_flip_1/stateful_uniform_full_int/strided_sliceStridedSliceLsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip:value:0Rsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack:output:0Tsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_1:output:0Tsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2E
Csequential_12/random_flip_1/stateful_uniform_full_int/strided_slice�
=sequential_12/random_flip_1/stateful_uniform_full_int/BitcastBitcastLsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice:output:0*
T0	*
_output_shapes
:*

type02?
=sequential_12/random_flip_1/stateful_uniform_full_int/Bitcast�
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2M
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack�
Msequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1�
Msequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2�
Esequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1StridedSliceLsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip:value:0Tsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack:output:0Vsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1:output:0Vsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2G
Esequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1�
?sequential_12/random_flip_1/stateful_uniform_full_int/Bitcast_1BitcastNsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02A
?sequential_12/random_flip_1/stateful_uniform_full_int/Bitcast_1�
9sequential_12/random_flip_1/stateful_uniform_full_int/algConst*
_output_shapes
: *
dtype0*
value	B :2;
9sequential_12/random_flip_1/stateful_uniform_full_int/alg�
5sequential_12/random_flip_1/stateful_uniform_full_intStatelessRandomUniformFullIntV2Dsequential_12/random_flip_1/stateful_uniform_full_int/shape:output:0Hsequential_12/random_flip_1/stateful_uniform_full_int/Bitcast_1:output:0Fsequential_12/random_flip_1/stateful_uniform_full_int/Bitcast:output:0Bsequential_12/random_flip_1/stateful_uniform_full_int/alg:output:0*
_output_shapes
:*
dtype0	27
5sequential_12/random_flip_1/stateful_uniform_full_int�
&sequential_12/random_flip_1/zeros_likeConst*
_output_shapes
:*
dtype0	*
valueB	R 2(
&sequential_12/random_flip_1/zeros_like�
!sequential_12/random_flip_1/stackPack>sequential_12/random_flip_1/stateful_uniform_full_int:output:0/sequential_12/random_flip_1/zeros_like:output:0*
N*
T0	*
_output_shapes

:2#
!sequential_12/random_flip_1/stack�
/sequential_12/random_flip_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        21
/sequential_12/random_flip_1/strided_slice/stack�
1sequential_12/random_flip_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       23
1sequential_12/random_flip_1/strided_slice/stack_1�
1sequential_12/random_flip_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      23
1sequential_12/random_flip_1/strided_slice/stack_2�
)sequential_12/random_flip_1/strided_sliceStridedSlice*sequential_12/random_flip_1/stack:output:08sequential_12/random_flip_1/strided_slice/stack:output:0:sequential_12/random_flip_1/strided_slice/stack_1:output:0:sequential_12/random_flip_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2+
)sequential_12/random_flip_1/strided_slice�
Osequential_12/random_flip_1/stateless_random_flip_left_right/control_dependencyIdentity!sequential_11/rescaling_1/add:z:0*
T0*0
_class&
$"loc:@sequential_11/rescaling_1/add*1
_output_shapes
:�����������2Q
Osequential_12/random_flip_1/stateless_random_flip_left_right/control_dependency�
Bsequential_12/random_flip_1/stateless_random_flip_left_right/ShapeShapeXsequential_12/random_flip_1/stateless_random_flip_left_right/control_dependency:output:0*
T0*
_output_shapes
:2D
Bsequential_12/random_flip_1/stateless_random_flip_left_right/Shape�
Psequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2R
Psequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack�
Rsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2T
Rsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_1�
Rsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2T
Rsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_2�
Jsequential_12/random_flip_1/stateless_random_flip_left_right/strided_sliceStridedSliceKsequential_12/random_flip_1/stateless_random_flip_left_right/Shape:output:0Ysequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack:output:0[sequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_1:output:0[sequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2L
Jsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice�
[sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shapePackSsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice:output:0*
N*
T0*
_output_shapes
:2]
[sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shape�
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2[
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min�
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2[
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/max�
rsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounter2sequential_12/random_flip_1/strided_slice:output:0* 
_output_shapes
::2t
rsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�
ksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgs^sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2m
ksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�
nsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2dsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shape:output:0xsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0|sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0qsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2p
nsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2�
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/subSubbsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/max:output:0bsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2[
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/sub�
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mulMulwsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2:output:0]sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2[
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mul�
Usequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniformAddV2]sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mul:z:0bsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2W
Usequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform�
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2N
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/1�
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2N
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/2�
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2N
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/3�
Jsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shapePackSsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice:output:0Usequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/1:output:0Usequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/2:output:0Usequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2L
Jsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape�
Dsequential_12/random_flip_1/stateless_random_flip_left_right/ReshapeReshapeYsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform:z:0Ssequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2F
Dsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape�
Bsequential_12/random_flip_1/stateless_random_flip_left_right/RoundRoundMsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape:output:0*
T0*/
_output_shapes
:���������2D
Bsequential_12/random_flip_1/stateless_random_flip_left_right/Round�
Ksequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2M
Ksequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2/axis�
Fsequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2	ReverseV2Xsequential_12/random_flip_1/stateless_random_flip_left_right/control_dependency:output:0Tsequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2H
Fsequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2�
@sequential_12/random_flip_1/stateless_random_flip_left_right/mulMulFsequential_12/random_flip_1/stateless_random_flip_left_right/Round:y:0Osequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2:output:0*
T0*1
_output_shapes
:�����������2B
@sequential_12/random_flip_1/stateless_random_flip_left_right/mul�
Bsequential_12/random_flip_1/stateless_random_flip_left_right/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2D
Bsequential_12/random_flip_1/stateless_random_flip_left_right/sub/x�
@sequential_12/random_flip_1/stateless_random_flip_left_right/subSubKsequential_12/random_flip_1/stateless_random_flip_left_right/sub/x:output:0Fsequential_12/random_flip_1/stateless_random_flip_left_right/Round:y:0*
T0*/
_output_shapes
:���������2B
@sequential_12/random_flip_1/stateless_random_flip_left_right/sub�
Bsequential_12/random_flip_1/stateless_random_flip_left_right/mul_1MulDsequential_12/random_flip_1/stateless_random_flip_left_right/sub:z:0Xsequential_12/random_flip_1/stateless_random_flip_left_right/control_dependency:output:0*
T0*1
_output_shapes
:�����������2D
Bsequential_12/random_flip_1/stateless_random_flip_left_right/mul_1�
@sequential_12/random_flip_1/stateless_random_flip_left_right/addAddV2Dsequential_12/random_flip_1/stateless_random_flip_left_right/mul:z:0Fsequential_12/random_flip_1/stateless_random_flip_left_right/mul_1:z:0*
T0*1
_output_shapes
:�����������2B
@sequential_12/random_flip_1/stateless_random_flip_left_right/add�
=sequential_12/random_flip_1/stateful_uniform_full_int_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:2?
=sequential_12/random_flip_1/stateful_uniform_full_int_1/shape�
=sequential_12/random_flip_1/stateful_uniform_full_int_1/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2?
=sequential_12/random_flip_1/stateful_uniform_full_int_1/Const�
<sequential_12/random_flip_1/stateful_uniform_full_int_1/ProdProdFsequential_12/random_flip_1/stateful_uniform_full_int_1/shape:output:0Fsequential_12/random_flip_1/stateful_uniform_full_int_1/Const:output:0*
T0*
_output_shapes
: 2>
<sequential_12/random_flip_1/stateful_uniform_full_int_1/Prod�
>sequential_12/random_flip_1/stateful_uniform_full_int_1/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2@
>sequential_12/random_flip_1/stateful_uniform_full_int_1/Cast/x�
>sequential_12/random_flip_1/stateful_uniform_full_int_1/Cast_1CastEsequential_12/random_flip_1/stateful_uniform_full_int_1/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2@
>sequential_12/random_flip_1/stateful_uniform_full_int_1/Cast_1�
Fsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkipRngReadAndSkipMsequential_12_random_flip_1_stateful_uniform_full_int_rngreadandskip_resourceGsequential_12/random_flip_1/stateful_uniform_full_int_1/Cast/x:output:0Bsequential_12/random_flip_1/stateful_uniform_full_int_1/Cast_1:y:0E^sequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip*
_output_shapes
:2H
Fsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip�
Ksequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2M
Ksequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack�
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1�
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2�
Esequential_12/random_flip_1/stateful_uniform_full_int_1/strided_sliceStridedSliceNsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip:value:0Tsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack:output:0Vsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1:output:0Vsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2G
Esequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice�
?sequential_12/random_flip_1/stateful_uniform_full_int_1/BitcastBitcastNsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice:output:0*
T0	*
_output_shapes
:*

type02A
?sequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast�
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack�
Osequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2Q
Osequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1�
Osequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2Q
Osequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2�
Gsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1StridedSliceNsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip:value:0Vsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack:output:0Xsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1:output:0Xsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2I
Gsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1�
Asequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast_1BitcastPsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02C
Asequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast_1�
;sequential_12/random_flip_1/stateful_uniform_full_int_1/algConst*
_output_shapes
: *
dtype0*
value	B :2=
;sequential_12/random_flip_1/stateful_uniform_full_int_1/alg�
7sequential_12/random_flip_1/stateful_uniform_full_int_1StatelessRandomUniformFullIntV2Fsequential_12/random_flip_1/stateful_uniform_full_int_1/shape:output:0Jsequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast_1:output:0Hsequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast:output:0Dsequential_12/random_flip_1/stateful_uniform_full_int_1/alg:output:0*
_output_shapes
:*
dtype0	29
7sequential_12/random_flip_1/stateful_uniform_full_int_1�
(sequential_12/random_flip_1/zeros_like_1Const*
_output_shapes
:*
dtype0	*
valueB	R 2*
(sequential_12/random_flip_1/zeros_like_1�
#sequential_12/random_flip_1/stack_1Pack@sequential_12/random_flip_1/stateful_uniform_full_int_1:output:01sequential_12/random_flip_1/zeros_like_1:output:0*
N*
T0	*
_output_shapes

:2%
#sequential_12/random_flip_1/stack_1�
1sequential_12/random_flip_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        23
1sequential_12/random_flip_1/strided_slice_1/stack�
3sequential_12/random_flip_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       25
3sequential_12/random_flip_1/strided_slice_1/stack_1�
3sequential_12/random_flip_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      25
3sequential_12/random_flip_1/strided_slice_1/stack_2�
+sequential_12/random_flip_1/strided_slice_1StridedSlice,sequential_12/random_flip_1/stack_1:output:0:sequential_12/random_flip_1/strided_slice_1/stack:output:0<sequential_12/random_flip_1/strided_slice_1/stack_1:output:0<sequential_12/random_flip_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2-
+sequential_12/random_flip_1/strided_slice_1�
Lsequential_12/random_flip_1/stateless_random_flip_up_down/control_dependencyIdentityDsequential_12/random_flip_1/stateless_random_flip_left_right/add:z:0*
T0*S
_classI
GEloc:@sequential_12/random_flip_1/stateless_random_flip_left_right/add*1
_output_shapes
:�����������2N
Lsequential_12/random_flip_1/stateless_random_flip_up_down/control_dependency�
?sequential_12/random_flip_1/stateless_random_flip_up_down/ShapeShapeUsequential_12/random_flip_1/stateless_random_flip_up_down/control_dependency:output:0*
T0*
_output_shapes
:2A
?sequential_12/random_flip_1/stateless_random_flip_up_down/Shape�
Msequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2O
Msequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack�
Osequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2Q
Osequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_1�
Osequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2Q
Osequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_2�
Gsequential_12/random_flip_1/stateless_random_flip_up_down/strided_sliceStridedSliceHsequential_12/random_flip_1/stateless_random_flip_up_down/Shape:output:0Vsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack:output:0Xsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_1:output:0Xsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2I
Gsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice�
Xsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shapePackPsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice:output:0*
N*
T0*
_output_shapes
:2Z
Xsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shape�
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2X
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min�
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2X
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/max�
osequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounter4sequential_12/random_flip_1/strided_slice_1:output:0l^sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg* 
_output_shapes
::2q
osequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�
hsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgp^sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2j
hsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�
ksequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2asequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shape:output:0usequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0ysequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0nsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2m
ksequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2�
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/subSub_sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/max:output:0_sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2X
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/sub�
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mulMultsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2:output:0Zsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2X
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mul�
Rsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniformAddV2Zsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mul:z:0_sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2T
Rsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform�
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2K
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/1�
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2K
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/2�
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2K
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/3�
Gsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shapePackPsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice:output:0Rsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/1:output:0Rsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/2:output:0Rsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2I
Gsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape�
Asequential_12/random_flip_1/stateless_random_flip_up_down/ReshapeReshapeVsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform:z:0Psequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2C
Asequential_12/random_flip_1/stateless_random_flip_up_down/Reshape�
?sequential_12/random_flip_1/stateless_random_flip_up_down/RoundRoundJsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape:output:0*
T0*/
_output_shapes
:���������2A
?sequential_12/random_flip_1/stateless_random_flip_up_down/Round�
Hsequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2J
Hsequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2/axis�
Csequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2	ReverseV2Usequential_12/random_flip_1/stateless_random_flip_up_down/control_dependency:output:0Qsequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2E
Csequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2�
=sequential_12/random_flip_1/stateless_random_flip_up_down/mulMulCsequential_12/random_flip_1/stateless_random_flip_up_down/Round:y:0Lsequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2:output:0*
T0*1
_output_shapes
:�����������2?
=sequential_12/random_flip_1/stateless_random_flip_up_down/mul�
?sequential_12/random_flip_1/stateless_random_flip_up_down/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2A
?sequential_12/random_flip_1/stateless_random_flip_up_down/sub/x�
=sequential_12/random_flip_1/stateless_random_flip_up_down/subSubHsequential_12/random_flip_1/stateless_random_flip_up_down/sub/x:output:0Csequential_12/random_flip_1/stateless_random_flip_up_down/Round:y:0*
T0*/
_output_shapes
:���������2?
=sequential_12/random_flip_1/stateless_random_flip_up_down/sub�
?sequential_12/random_flip_1/stateless_random_flip_up_down/mul_1MulAsequential_12/random_flip_1/stateless_random_flip_up_down/sub:z:0Usequential_12/random_flip_1/stateless_random_flip_up_down/control_dependency:output:0*
T0*1
_output_shapes
:�����������2A
?sequential_12/random_flip_1/stateless_random_flip_up_down/mul_1�
=sequential_12/random_flip_1/stateless_random_flip_up_down/addAddV2Asequential_12/random_flip_1/stateless_random_flip_up_down/mul:z:0Csequential_12/random_flip_1/stateless_random_flip_up_down/mul_1:z:0*
T0*1
_output_shapes
:�����������2?
=sequential_12/random_flip_1/stateless_random_flip_up_down/add�
%sequential_12/random_rotation_1/ShapeShapeAsequential_12/random_flip_1/stateless_random_flip_up_down/add:z:0*
T0*
_output_shapes
:2'
%sequential_12/random_rotation_1/Shape�
3sequential_12/random_rotation_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 25
3sequential_12/random_rotation_1/strided_slice/stack�
5sequential_12/random_rotation_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:27
5sequential_12/random_rotation_1/strided_slice/stack_1�
5sequential_12/random_rotation_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:27
5sequential_12/random_rotation_1/strided_slice/stack_2�
-sequential_12/random_rotation_1/strided_sliceStridedSlice.sequential_12/random_rotation_1/Shape:output:0<sequential_12/random_rotation_1/strided_slice/stack:output:0>sequential_12/random_rotation_1/strided_slice/stack_1:output:0>sequential_12/random_rotation_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2/
-sequential_12/random_rotation_1/strided_slice�
5sequential_12/random_rotation_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������27
5sequential_12/random_rotation_1/strided_slice_1/stack�
7sequential_12/random_rotation_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������29
7sequential_12/random_rotation_1/strided_slice_1/stack_1�
7sequential_12/random_rotation_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:29
7sequential_12/random_rotation_1/strided_slice_1/stack_2�
/sequential_12/random_rotation_1/strided_slice_1StridedSlice.sequential_12/random_rotation_1/Shape:output:0>sequential_12/random_rotation_1/strided_slice_1/stack:output:0@sequential_12/random_rotation_1/strided_slice_1/stack_1:output:0@sequential_12/random_rotation_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask21
/sequential_12/random_rotation_1/strided_slice_1�
$sequential_12/random_rotation_1/CastCast8sequential_12/random_rotation_1/strided_slice_1:output:0*

DstT0*

SrcT0*
_output_shapes
: 2&
$sequential_12/random_rotation_1/Cast�
5sequential_12/random_rotation_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������27
5sequential_12/random_rotation_1/strided_slice_2/stack�
7sequential_12/random_rotation_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������29
7sequential_12/random_rotation_1/strided_slice_2/stack_1�
7sequential_12/random_rotation_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:29
7sequential_12/random_rotation_1/strided_slice_2/stack_2�
/sequential_12/random_rotation_1/strided_slice_2StridedSlice.sequential_12/random_rotation_1/Shape:output:0>sequential_12/random_rotation_1/strided_slice_2/stack:output:0@sequential_12/random_rotation_1/strided_slice_2/stack_1:output:0@sequential_12/random_rotation_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask21
/sequential_12/random_rotation_1/strided_slice_2�
&sequential_12/random_rotation_1/Cast_1Cast8sequential_12/random_rotation_1/strided_slice_2:output:0*

DstT0*

SrcT0*
_output_shapes
: 2(
&sequential_12/random_rotation_1/Cast_1�
6sequential_12/random_rotation_1/stateful_uniform/shapePack6sequential_12/random_rotation_1/strided_slice:output:0*
N*
T0*
_output_shapes
:28
6sequential_12/random_rotation_1/stateful_uniform/shape�
4sequential_12/random_rotation_1/stateful_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *|٠�26
4sequential_12/random_rotation_1/stateful_uniform/min�
4sequential_12/random_rotation_1/stateful_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *|٠?26
4sequential_12/random_rotation_1/stateful_uniform/max�
6sequential_12/random_rotation_1/stateful_uniform/ConstConst*
_output_shapes
:*
dtype0*
valueB: 28
6sequential_12/random_rotation_1/stateful_uniform/Const�
5sequential_12/random_rotation_1/stateful_uniform/ProdProd?sequential_12/random_rotation_1/stateful_uniform/shape:output:0?sequential_12/random_rotation_1/stateful_uniform/Const:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/stateful_uniform/Prod�
7sequential_12/random_rotation_1/stateful_uniform/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :29
7sequential_12/random_rotation_1/stateful_uniform/Cast/x�
7sequential_12/random_rotation_1/stateful_uniform/Cast_1Cast>sequential_12/random_rotation_1/stateful_uniform/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 29
7sequential_12/random_rotation_1/stateful_uniform/Cast_1�
?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkipRngReadAndSkipHsequential_12_random_rotation_1_stateful_uniform_rngreadandskip_resource@sequential_12/random_rotation_1/stateful_uniform/Cast/x:output:0;sequential_12/random_rotation_1/stateful_uniform/Cast_1:y:0*
_output_shapes
:2A
?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip�
Dsequential_12/random_rotation_1/stateful_uniform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2F
Dsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack�
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2H
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_1�
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2H
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_2�
>sequential_12/random_rotation_1/stateful_uniform/strided_sliceStridedSliceGsequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip:value:0Msequential_12/random_rotation_1/stateful_uniform/strided_slice/stack:output:0Osequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_1:output:0Osequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2@
>sequential_12/random_rotation_1/stateful_uniform/strided_slice�
8sequential_12/random_rotation_1/stateful_uniform/BitcastBitcastGsequential_12/random_rotation_1/stateful_uniform/strided_slice:output:0*
T0	*
_output_shapes
:*

type02:
8sequential_12/random_rotation_1/stateful_uniform/Bitcast�
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2H
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack�
Hsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2J
Hsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_1�
Hsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2J
Hsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_2�
@sequential_12/random_rotation_1/stateful_uniform/strided_slice_1StridedSliceGsequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip:value:0Osequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack:output:0Qsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_1:output:0Qsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2B
@sequential_12/random_rotation_1/stateful_uniform/strided_slice_1�
:sequential_12/random_rotation_1/stateful_uniform/Bitcast_1BitcastIsequential_12/random_rotation_1/stateful_uniform/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02<
:sequential_12/random_rotation_1/stateful_uniform/Bitcast_1�
Msequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2/algConst*
_output_shapes
: *
dtype0*
value	B :2O
Msequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2/alg�
Isequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2StatelessRandomUniformV2?sequential_12/random_rotation_1/stateful_uniform/shape:output:0Csequential_12/random_rotation_1/stateful_uniform/Bitcast_1:output:0Asequential_12/random_rotation_1/stateful_uniform/Bitcast:output:0Vsequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2/alg:output:0*#
_output_shapes
:���������2K
Isequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2�
4sequential_12/random_rotation_1/stateful_uniform/subSub=sequential_12/random_rotation_1/stateful_uniform/max:output:0=sequential_12/random_rotation_1/stateful_uniform/min:output:0*
T0*
_output_shapes
: 26
4sequential_12/random_rotation_1/stateful_uniform/sub�
4sequential_12/random_rotation_1/stateful_uniform/mulMulRsequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2:output:08sequential_12/random_rotation_1/stateful_uniform/sub:z:0*
T0*#
_output_shapes
:���������26
4sequential_12/random_rotation_1/stateful_uniform/mul�
0sequential_12/random_rotation_1/stateful_uniformAddV28sequential_12/random_rotation_1/stateful_uniform/mul:z:0=sequential_12/random_rotation_1/stateful_uniform/min:output:0*
T0*#
_output_shapes
:���������22
0sequential_12/random_rotation_1/stateful_uniform�
5sequential_12/random_rotation_1/rotation_matrix/sub/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?27
5sequential_12/random_rotation_1/rotation_matrix/sub/y�
3sequential_12/random_rotation_1/rotation_matrix/subSub*sequential_12/random_rotation_1/Cast_1:y:0>sequential_12/random_rotation_1/rotation_matrix/sub/y:output:0*
T0*
_output_shapes
: 25
3sequential_12/random_rotation_1/rotation_matrix/sub�
3sequential_12/random_rotation_1/rotation_matrix/CosCos4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/Cos�
7sequential_12/random_rotation_1/rotation_matrix/sub_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_1/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_1Sub*sequential_12/random_rotation_1/Cast_1:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_1/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_1�
3sequential_12/random_rotation_1/rotation_matrix/mulMul7sequential_12/random_rotation_1/rotation_matrix/Cos:y:09sequential_12/random_rotation_1/rotation_matrix/sub_1:z:0*
T0*#
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/mul�
3sequential_12/random_rotation_1/rotation_matrix/SinSin4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/Sin�
7sequential_12/random_rotation_1/rotation_matrix/sub_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_2/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_2Sub(sequential_12/random_rotation_1/Cast:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_2/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_2�
5sequential_12/random_rotation_1/rotation_matrix/mul_1Mul7sequential_12/random_rotation_1/rotation_matrix/Sin:y:09sequential_12/random_rotation_1/rotation_matrix/sub_2:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/mul_1�
5sequential_12/random_rotation_1/rotation_matrix/sub_3Sub7sequential_12/random_rotation_1/rotation_matrix/mul:z:09sequential_12/random_rotation_1/rotation_matrix/mul_1:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/sub_3�
5sequential_12/random_rotation_1/rotation_matrix/sub_4Sub7sequential_12/random_rotation_1/rotation_matrix/sub:z:09sequential_12/random_rotation_1/rotation_matrix/sub_3:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/sub_4�
9sequential_12/random_rotation_1/rotation_matrix/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2;
9sequential_12/random_rotation_1/rotation_matrix/truediv/y�
7sequential_12/random_rotation_1/rotation_matrix/truedivRealDiv9sequential_12/random_rotation_1/rotation_matrix/sub_4:z:0Bsequential_12/random_rotation_1/rotation_matrix/truediv/y:output:0*
T0*#
_output_shapes
:���������29
7sequential_12/random_rotation_1/rotation_matrix/truediv�
7sequential_12/random_rotation_1/rotation_matrix/sub_5/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_5/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_5Sub(sequential_12/random_rotation_1/Cast:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_5/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_5�
5sequential_12/random_rotation_1/rotation_matrix/Sin_1Sin4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Sin_1�
7sequential_12/random_rotation_1/rotation_matrix/sub_6/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_6/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_6Sub*sequential_12/random_rotation_1/Cast_1:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_6/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_6�
5sequential_12/random_rotation_1/rotation_matrix/mul_2Mul9sequential_12/random_rotation_1/rotation_matrix/Sin_1:y:09sequential_12/random_rotation_1/rotation_matrix/sub_6:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/mul_2�
5sequential_12/random_rotation_1/rotation_matrix/Cos_1Cos4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Cos_1�
7sequential_12/random_rotation_1/rotation_matrix/sub_7/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_7/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_7Sub(sequential_12/random_rotation_1/Cast:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_7/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_7�
5sequential_12/random_rotation_1/rotation_matrix/mul_3Mul9sequential_12/random_rotation_1/rotation_matrix/Cos_1:y:09sequential_12/random_rotation_1/rotation_matrix/sub_7:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/mul_3�
3sequential_12/random_rotation_1/rotation_matrix/addAddV29sequential_12/random_rotation_1/rotation_matrix/mul_2:z:09sequential_12/random_rotation_1/rotation_matrix/mul_3:z:0*
T0*#
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/add�
5sequential_12/random_rotation_1/rotation_matrix/sub_8Sub9sequential_12/random_rotation_1/rotation_matrix/sub_5:z:07sequential_12/random_rotation_1/rotation_matrix/add:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/sub_8�
;sequential_12/random_rotation_1/rotation_matrix/truediv_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2=
;sequential_12/random_rotation_1/rotation_matrix/truediv_1/y�
9sequential_12/random_rotation_1/rotation_matrix/truediv_1RealDiv9sequential_12/random_rotation_1/rotation_matrix/sub_8:z:0Dsequential_12/random_rotation_1/rotation_matrix/truediv_1/y:output:0*
T0*#
_output_shapes
:���������2;
9sequential_12/random_rotation_1/rotation_matrix/truediv_1�
5sequential_12/random_rotation_1/rotation_matrix/ShapeShape4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*
_output_shapes
:27
5sequential_12/random_rotation_1/rotation_matrix/Shape�
Csequential_12/random_rotation_1/rotation_matrix/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2E
Csequential_12/random_rotation_1/rotation_matrix/strided_slice/stack�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_1�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_2�
=sequential_12/random_rotation_1/rotation_matrix/strided_sliceStridedSlice>sequential_12/random_rotation_1/rotation_matrix/Shape:output:0Lsequential_12/random_rotation_1/rotation_matrix/strided_slice/stack:output:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_1:output:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2?
=sequential_12/random_rotation_1/rotation_matrix/strided_slice�
5sequential_12/random_rotation_1/rotation_matrix/Cos_2Cos4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Cos_2�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_1StridedSlice9sequential_12/random_rotation_1/rotation_matrix/Cos_2:y:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_1�
5sequential_12/random_rotation_1/rotation_matrix/Sin_2Sin4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Sin_2�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_2StridedSlice9sequential_12/random_rotation_1/rotation_matrix/Sin_2:y:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_2�
3sequential_12/random_rotation_1/rotation_matrix/NegNegHsequential_12/random_rotation_1/rotation_matrix/strided_slice_2:output:0*
T0*'
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/Neg�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_3StridedSlice;sequential_12/random_rotation_1/rotation_matrix/truediv:z:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_3�
5sequential_12/random_rotation_1/rotation_matrix/Sin_3Sin4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Sin_3�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_4StridedSlice9sequential_12/random_rotation_1/rotation_matrix/Sin_3:y:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_4�
5sequential_12/random_rotation_1/rotation_matrix/Cos_3Cos4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Cos_3�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_5StridedSlice9sequential_12/random_rotation_1/rotation_matrix/Cos_3:y:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_5�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_6StridedSlice=sequential_12/random_rotation_1/rotation_matrix/truediv_1:z:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_6�
;sequential_12/random_rotation_1/rotation_matrix/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2=
;sequential_12/random_rotation_1/rotation_matrix/zeros/mul/y�
9sequential_12/random_rotation_1/rotation_matrix/zeros/mulMulFsequential_12/random_rotation_1/rotation_matrix/strided_slice:output:0Dsequential_12/random_rotation_1/rotation_matrix/zeros/mul/y:output:0*
T0*
_output_shapes
: 2;
9sequential_12/random_rotation_1/rotation_matrix/zeros/mul�
<sequential_12/random_rotation_1/rotation_matrix/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :�2>
<sequential_12/random_rotation_1/rotation_matrix/zeros/Less/y�
:sequential_12/random_rotation_1/rotation_matrix/zeros/LessLess=sequential_12/random_rotation_1/rotation_matrix/zeros/mul:z:0Esequential_12/random_rotation_1/rotation_matrix/zeros/Less/y:output:0*
T0*
_output_shapes
: 2<
:sequential_12/random_rotation_1/rotation_matrix/zeros/Less�
>sequential_12/random_rotation_1/rotation_matrix/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :2@
>sequential_12/random_rotation_1/rotation_matrix/zeros/packed/1�
<sequential_12/random_rotation_1/rotation_matrix/zeros/packedPackFsequential_12/random_rotation_1/rotation_matrix/strided_slice:output:0Gsequential_12/random_rotation_1/rotation_matrix/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2>
<sequential_12/random_rotation_1/rotation_matrix/zeros/packed�
;sequential_12/random_rotation_1/rotation_matrix/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2=
;sequential_12/random_rotation_1/rotation_matrix/zeros/Const�
5sequential_12/random_rotation_1/rotation_matrix/zerosFillEsequential_12/random_rotation_1/rotation_matrix/zeros/packed:output:0Dsequential_12/random_rotation_1/rotation_matrix/zeros/Const:output:0*
T0*'
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/zeros�
;sequential_12/random_rotation_1/rotation_matrix/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2=
;sequential_12/random_rotation_1/rotation_matrix/concat/axis�
6sequential_12/random_rotation_1/rotation_matrix/concatConcatV2Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_1:output:07sequential_12/random_rotation_1/rotation_matrix/Neg:y:0Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_3:output:0Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_4:output:0Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_5:output:0Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_6:output:0>sequential_12/random_rotation_1/rotation_matrix/zeros:output:0Dsequential_12/random_rotation_1/rotation_matrix/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������28
6sequential_12/random_rotation_1/rotation_matrix/concat�
/sequential_12/random_rotation_1/transform/ShapeShapeAsequential_12/random_flip_1/stateless_random_flip_up_down/add:z:0*
T0*
_output_shapes
:21
/sequential_12/random_rotation_1/transform/Shape�
=sequential_12/random_rotation_1/transform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2?
=sequential_12/random_rotation_1/transform/strided_slice/stack�
?sequential_12/random_rotation_1/transform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2A
?sequential_12/random_rotation_1/transform/strided_slice/stack_1�
?sequential_12/random_rotation_1/transform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2A
?sequential_12/random_rotation_1/transform/strided_slice/stack_2�
7sequential_12/random_rotation_1/transform/strided_sliceStridedSlice8sequential_12/random_rotation_1/transform/Shape:output:0Fsequential_12/random_rotation_1/transform/strided_slice/stack:output:0Hsequential_12/random_rotation_1/transform/strided_slice/stack_1:output:0Hsequential_12/random_rotation_1/transform/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:29
7sequential_12/random_rotation_1/transform/strided_slice�
4sequential_12/random_rotation_1/transform/fill_valueConst*
_output_shapes
: *
dtype0*
valueB
 *    26
4sequential_12/random_rotation_1/transform/fill_value�
Dsequential_12/random_rotation_1/transform/ImageProjectiveTransformV3ImageProjectiveTransformV3Asequential_12/random_flip_1/stateless_random_flip_up_down/add:z:0?sequential_12/random_rotation_1/rotation_matrix/concat:output:0@sequential_12/random_rotation_1/transform/strided_slice:output:0=sequential_12/random_rotation_1/transform/fill_value:output:0*1
_output_shapes
:�����������*
dtype0*
	fill_mode	REFLECT*
interpolation
BILINEAR2F
Dsequential_12/random_rotation_1/transform/ImageProjectiveTransformV3�
conv2d_72/Conv2D/ReadVariableOpReadVariableOp(conv2d_72_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype02!
conv2d_72/Conv2D/ReadVariableOp�
conv2d_72/Conv2DConv2DYsequential_12/random_rotation_1/transform/ImageProjectiveTransformV3:transformed_images:0'conv2d_72/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� *
paddingVALID*
strides
2
conv2d_72/Conv2D�
 conv2d_72/BiasAdd/ReadVariableOpReadVariableOp)conv2d_72_biasadd_readvariableop_resource*
_output_shapes
: *
dtype02"
 conv2d_72/BiasAdd/ReadVariableOp�
conv2d_72/BiasAddBiasAddconv2d_72/Conv2D:output:0(conv2d_72/BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� 2
conv2d_72/BiasAdd�
conv2d_72/ReluReluconv2d_72/BiasAdd:output:0*
T0*1
_output_shapes
:����������� 2
conv2d_72/Relu�
max_pooling2d_72/MaxPoolMaxPoolconv2d_72/Relu:activations:0*/
_output_shapes
:���������~~ *
ksize
*
paddingVALID*
strides
2
max_pooling2d_72/MaxPool�
conv2d_73/Conv2D/ReadVariableOpReadVariableOp(conv2d_73_conv2d_readvariableop_resource*&
_output_shapes
: @*
dtype02!
conv2d_73/Conv2D/ReadVariableOp�
conv2d_73/Conv2DConv2D!max_pooling2d_72/MaxPool:output:0'conv2d_73/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@*
paddingVALID*
strides
2
conv2d_73/Conv2D�
 conv2d_73/BiasAdd/ReadVariableOpReadVariableOp)conv2d_73_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02"
 conv2d_73/BiasAdd/ReadVariableOp�
conv2d_73/BiasAddBiasAddconv2d_73/Conv2D:output:0(conv2d_73/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@2
conv2d_73/BiasAdd~
conv2d_73/ReluReluconv2d_73/BiasAdd:output:0*
T0*/
_output_shapes
:���������||@2
conv2d_73/Relu�
max_pooling2d_73/MaxPoolMaxPoolconv2d_73/Relu:activations:0*/
_output_shapes
:���������>>@*
ksize
*
paddingVALID*
strides
2
max_pooling2d_73/MaxPool�
conv2d_74/Conv2D/ReadVariableOpReadVariableOp(conv2d_74_conv2d_readvariableop_resource*&
_output_shapes
:@@*
dtype02!
conv2d_74/Conv2D/ReadVariableOp�
conv2d_74/Conv2DConv2D!max_pooling2d_73/MaxPool:output:0'conv2d_74/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@*
paddingVALID*
strides
2
conv2d_74/Conv2D�
 conv2d_74/BiasAdd/ReadVariableOpReadVariableOp)conv2d_74_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02"
 conv2d_74/BiasAdd/ReadVariableOp�
conv2d_74/BiasAddBiasAddconv2d_74/Conv2D:output:0(conv2d_74/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@2
conv2d_74/BiasAdd~
conv2d_74/ReluReluconv2d_74/BiasAdd:output:0*
T0*/
_output_shapes
:���������<<@2
conv2d_74/Relu�
max_pooling2d_74/MaxPoolMaxPoolconv2d_74/Relu:activations:0*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
2
max_pooling2d_74/MaxPoolu
flatten_19/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� �  2
flatten_19/Const�
flatten_19/ReshapeReshape!max_pooling2d_74/MaxPool:output:0flatten_19/Const:output:0*
T0*)
_output_shapes
:�����������2
flatten_19/Reshape�
dense_38/MatMul/ReadVariableOpReadVariableOp'dense_38_matmul_readvariableop_resource* 
_output_shapes
:
��@*
dtype02 
dense_38/MatMul/ReadVariableOp�
dense_38/MatMulMatMulflatten_19/Reshape:output:0&dense_38/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_38/MatMul�
dense_38/BiasAdd/ReadVariableOpReadVariableOp(dense_38_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02!
dense_38/BiasAdd/ReadVariableOp�
dense_38/BiasAddBiasAdddense_38/MatMul:product:0'dense_38/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_38/BiasAdds
dense_38/ReluReludense_38/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
dense_38/Relu�
dense_39/MatMul/ReadVariableOpReadVariableOp'dense_39_matmul_readvariableop_resource*
_output_shapes

:@
*
dtype02 
dense_39/MatMul/ReadVariableOp�
dense_39/MatMulMatMuldense_38/Relu:activations:0&dense_39/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_39/MatMul�
dense_39/BiasAdd/ReadVariableOpReadVariableOp(dense_39_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_39/BiasAdd/ReadVariableOp�
dense_39/BiasAddBiasAdddense_39/MatMul:product:0'dense_39/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_39/BiasAdd|
dense_39/SoftmaxSoftmaxdense_39/BiasAdd:output:0*
T0*'
_output_shapes
:���������
2
dense_39/Softmaxu
IdentityIdentitydense_39/Softmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity�
NoOpNoOp!^conv2d_72/BiasAdd/ReadVariableOp ^conv2d_72/Conv2D/ReadVariableOp!^conv2d_73/BiasAdd/ReadVariableOp ^conv2d_73/Conv2D/ReadVariableOp!^conv2d_74/BiasAdd/ReadVariableOp ^conv2d_74/Conv2D/ReadVariableOp ^dense_38/BiasAdd/ReadVariableOp^dense_38/MatMul/ReadVariableOp ^dense_39/BiasAdd/ReadVariableOp^dense_39/MatMul/ReadVariableOpE^sequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkipG^sequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkipl^sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgs^sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounteri^sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgp^sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter@^sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:�����������: : : : : : : : : : : : 2D
 conv2d_72/BiasAdd/ReadVariableOp conv2d_72/BiasAdd/ReadVariableOp2B
conv2d_72/Conv2D/ReadVariableOpconv2d_72/Conv2D/ReadVariableOp2D
 conv2d_73/BiasAdd/ReadVariableOp conv2d_73/BiasAdd/ReadVariableOp2B
conv2d_73/Conv2D/ReadVariableOpconv2d_73/Conv2D/ReadVariableOp2D
 conv2d_74/BiasAdd/ReadVariableOp conv2d_74/BiasAdd/ReadVariableOp2B
conv2d_74/Conv2D/ReadVariableOpconv2d_74/Conv2D/ReadVariableOp2B
dense_38/BiasAdd/ReadVariableOpdense_38/BiasAdd/ReadVariableOp2@
dense_38/MatMul/ReadVariableOpdense_38/MatMul/ReadVariableOp2B
dense_39/BiasAdd/ReadVariableOpdense_39/BiasAdd/ReadVariableOp2@
dense_39/MatMul/ReadVariableOpdense_39/MatMul/ReadVariableOp2�
Dsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkipDsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip2�
Fsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkipFsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip2�
ksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg2�
rsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterrsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter2�
hsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlghsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg2�
osequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterosequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter2�
?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
-__inference_sequential_23_layer_call_fn_61062
sequential_11_input!
unknown: 
	unknown_0: #
	unknown_1: @
	unknown_2:@#
	unknown_3:@@
	unknown_4:@
	unknown_5:
��@
	unknown_6:@
	unknown_7:@

	unknown_8:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallsequential_11_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*,
_read_only_resource_inputs

	
*/
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_23_layer_call_and_return_conditional_losses_607142
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:�����������: : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
1
_output_shapes
:�����������
-
_user_specified_namesequential_11_input
�

�
#__inference_signature_wrapper_61037
sequential_11_input!
unknown: 
	unknown_0: #
	unknown_1: @
	unknown_2:@#
	unknown_3:@@
	unknown_4:@
	unknown_5:
��@
	unknown_6:@
	unknown_7:@

	unknown_8:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallsequential_11_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*,
_read_only_resource_inputs

	
*/
config_proto

CPU

GPU 2J 8@� *)
f$R"
 __inference__wrapped_model_601222
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:�����������: : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
1
_output_shapes
:�����������
-
_user_specified_namesequential_11_input
�
g
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_62424

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
MaxPool�
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
1__inference_random_rotation_1_layer_call_fn_62683

inputs
unknown:	
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *U
fPRN
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_603492
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*1
_output_shapes
:�����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:�����������: 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�	
d
H__inference_sequential_11_layer_call_and_return_conditional_losses_60176

inputs
identity�
resizing_1/PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *N
fIRG
E__inference_resizing_1_layer_call_and_return_conditional_losses_601352
resizing_1/PartitionedCall�
rescaling_1/PartitionedCallPartitionedCall#resizing_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *O
fJRH
F__inference_rescaling_1_layer_call_and_return_conditional_losses_601452
rescaling_1/PartitionedCall�
IdentityIdentity$rescaling_1/PartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
}
-__inference_random_flip_1_layer_call_fn_62556

inputs
unknown:	
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_random_flip_1_layer_call_and_return_conditional_losses_604732
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*1
_output_shapes
:�����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:�����������: 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
��
�
H__inference_random_flip_1_layer_call_and_return_conditional_losses_60473

inputs?
1stateful_uniform_full_int_rngreadandskip_resource:	
identity��(stateful_uniform_full_int/RngReadAndSkip�*stateful_uniform_full_int_1/RngReadAndSkip�Ostateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�Vstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�Lstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�Sstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�
stateful_uniform_full_int/shapeConst*
_output_shapes
:*
dtype0*
valueB:2!
stateful_uniform_full_int/shape�
stateful_uniform_full_int/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
stateful_uniform_full_int/Const�
stateful_uniform_full_int/ProdProd(stateful_uniform_full_int/shape:output:0(stateful_uniform_full_int/Const:output:0*
T0*
_output_shapes
: 2 
stateful_uniform_full_int/Prod�
 stateful_uniform_full_int/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2"
 stateful_uniform_full_int/Cast/x�
 stateful_uniform_full_int/Cast_1Cast'stateful_uniform_full_int/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2"
 stateful_uniform_full_int/Cast_1�
(stateful_uniform_full_int/RngReadAndSkipRngReadAndSkip1stateful_uniform_full_int_rngreadandskip_resource)stateful_uniform_full_int/Cast/x:output:0$stateful_uniform_full_int/Cast_1:y:0*
_output_shapes
:2*
(stateful_uniform_full_int/RngReadAndSkip�
-stateful_uniform_full_int/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2/
-stateful_uniform_full_int/strided_slice/stack�
/stateful_uniform_full_int/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/stateful_uniform_full_int/strided_slice/stack_1�
/stateful_uniform_full_int/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/stateful_uniform_full_int/strided_slice/stack_2�
'stateful_uniform_full_int/strided_sliceStridedSlice0stateful_uniform_full_int/RngReadAndSkip:value:06stateful_uniform_full_int/strided_slice/stack:output:08stateful_uniform_full_int/strided_slice/stack_1:output:08stateful_uniform_full_int/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2)
'stateful_uniform_full_int/strided_slice�
!stateful_uniform_full_int/BitcastBitcast0stateful_uniform_full_int/strided_slice:output:0*
T0	*
_output_shapes
:*

type02#
!stateful_uniform_full_int/Bitcast�
/stateful_uniform_full_int/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:21
/stateful_uniform_full_int/strided_slice_1/stack�
1stateful_uniform_full_int/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int/strided_slice_1/stack_1�
1stateful_uniform_full_int/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int/strided_slice_1/stack_2�
)stateful_uniform_full_int/strided_slice_1StridedSlice0stateful_uniform_full_int/RngReadAndSkip:value:08stateful_uniform_full_int/strided_slice_1/stack:output:0:stateful_uniform_full_int/strided_slice_1/stack_1:output:0:stateful_uniform_full_int/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2+
)stateful_uniform_full_int/strided_slice_1�
#stateful_uniform_full_int/Bitcast_1Bitcast2stateful_uniform_full_int/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02%
#stateful_uniform_full_int/Bitcast_1�
stateful_uniform_full_int/algConst*
_output_shapes
: *
dtype0*
value	B :2
stateful_uniform_full_int/alg�
stateful_uniform_full_intStatelessRandomUniformFullIntV2(stateful_uniform_full_int/shape:output:0,stateful_uniform_full_int/Bitcast_1:output:0*stateful_uniform_full_int/Bitcast:output:0&stateful_uniform_full_int/alg:output:0*
_output_shapes
:*
dtype0	2
stateful_uniform_full_intb

zeros_likeConst*
_output_shapes
:*
dtype0	*
valueB	R 2

zeros_like�
stackPack"stateful_uniform_full_int:output:0zeros_like:output:0*
N*
T0	*
_output_shapes

:2
stack{
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice/stack
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2
strided_slice/stack_1
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice/stack_2�
strided_sliceStridedSlicestack:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice�
3stateless_random_flip_left_right/control_dependencyIdentityinputs*
T0*
_class
loc:@inputs*1
_output_shapes
:�����������25
3stateless_random_flip_left_right/control_dependency�
&stateless_random_flip_left_right/ShapeShape<stateless_random_flip_left_right/control_dependency:output:0*
T0*
_output_shapes
:2(
&stateless_random_flip_left_right/Shape�
4stateless_random_flip_left_right/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 26
4stateless_random_flip_left_right/strided_slice/stack�
6stateless_random_flip_left_right/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:28
6stateless_random_flip_left_right/strided_slice/stack_1�
6stateless_random_flip_left_right/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:28
6stateless_random_flip_left_right/strided_slice/stack_2�
.stateless_random_flip_left_right/strided_sliceStridedSlice/stateless_random_flip_left_right/Shape:output:0=stateless_random_flip_left_right/strided_slice/stack:output:0?stateless_random_flip_left_right/strided_slice/stack_1:output:0?stateless_random_flip_left_right/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask20
.stateless_random_flip_left_right/strided_slice�
?stateless_random_flip_left_right/stateless_random_uniform/shapePack7stateless_random_flip_left_right/strided_slice:output:0*
N*
T0*
_output_shapes
:2A
?stateless_random_flip_left_right/stateless_random_uniform/shape�
=stateless_random_flip_left_right/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2?
=stateless_random_flip_left_right/stateless_random_uniform/min�
=stateless_random_flip_left_right/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2?
=stateless_random_flip_left_right/stateless_random_uniform/max�
Vstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounterstrided_slice:output:0* 
_output_shapes
::2X
Vstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�
Ostateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgW^stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2Q
Ostateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�
Rstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2Hstateless_random_flip_left_right/stateless_random_uniform/shape:output:0\stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0`stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0Ustateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2T
Rstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2�
=stateless_random_flip_left_right/stateless_random_uniform/subSubFstateless_random_flip_left_right/stateless_random_uniform/max:output:0Fstateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2?
=stateless_random_flip_left_right/stateless_random_uniform/sub�
=stateless_random_flip_left_right/stateless_random_uniform/mulMul[stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2:output:0Astateless_random_flip_left_right/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2?
=stateless_random_flip_left_right/stateless_random_uniform/mul�
9stateless_random_flip_left_right/stateless_random_uniformAddV2Astateless_random_flip_left_right/stateless_random_uniform/mul:z:0Fstateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2;
9stateless_random_flip_left_right/stateless_random_uniform�
0stateless_random_flip_left_right/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :22
0stateless_random_flip_left_right/Reshape/shape/1�
0stateless_random_flip_left_right/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :22
0stateless_random_flip_left_right/Reshape/shape/2�
0stateless_random_flip_left_right/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :22
0stateless_random_flip_left_right/Reshape/shape/3�
.stateless_random_flip_left_right/Reshape/shapePack7stateless_random_flip_left_right/strided_slice:output:09stateless_random_flip_left_right/Reshape/shape/1:output:09stateless_random_flip_left_right/Reshape/shape/2:output:09stateless_random_flip_left_right/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:20
.stateless_random_flip_left_right/Reshape/shape�
(stateless_random_flip_left_right/ReshapeReshape=stateless_random_flip_left_right/stateless_random_uniform:z:07stateless_random_flip_left_right/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2*
(stateless_random_flip_left_right/Reshape�
&stateless_random_flip_left_right/RoundRound1stateless_random_flip_left_right/Reshape:output:0*
T0*/
_output_shapes
:���������2(
&stateless_random_flip_left_right/Round�
/stateless_random_flip_left_right/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:21
/stateless_random_flip_left_right/ReverseV2/axis�
*stateless_random_flip_left_right/ReverseV2	ReverseV2<stateless_random_flip_left_right/control_dependency:output:08stateless_random_flip_left_right/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2,
*stateless_random_flip_left_right/ReverseV2�
$stateless_random_flip_left_right/mulMul*stateless_random_flip_left_right/Round:y:03stateless_random_flip_left_right/ReverseV2:output:0*
T0*1
_output_shapes
:�����������2&
$stateless_random_flip_left_right/mul�
&stateless_random_flip_left_right/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2(
&stateless_random_flip_left_right/sub/x�
$stateless_random_flip_left_right/subSub/stateless_random_flip_left_right/sub/x:output:0*stateless_random_flip_left_right/Round:y:0*
T0*/
_output_shapes
:���������2&
$stateless_random_flip_left_right/sub�
&stateless_random_flip_left_right/mul_1Mul(stateless_random_flip_left_right/sub:z:0<stateless_random_flip_left_right/control_dependency:output:0*
T0*1
_output_shapes
:�����������2(
&stateless_random_flip_left_right/mul_1�
$stateless_random_flip_left_right/addAddV2(stateless_random_flip_left_right/mul:z:0*stateless_random_flip_left_right/mul_1:z:0*
T0*1
_output_shapes
:�����������2&
$stateless_random_flip_left_right/add�
!stateful_uniform_full_int_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:2#
!stateful_uniform_full_int_1/shape�
!stateful_uniform_full_int_1/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2#
!stateful_uniform_full_int_1/Const�
 stateful_uniform_full_int_1/ProdProd*stateful_uniform_full_int_1/shape:output:0*stateful_uniform_full_int_1/Const:output:0*
T0*
_output_shapes
: 2"
 stateful_uniform_full_int_1/Prod�
"stateful_uniform_full_int_1/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2$
"stateful_uniform_full_int_1/Cast/x�
"stateful_uniform_full_int_1/Cast_1Cast)stateful_uniform_full_int_1/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2$
"stateful_uniform_full_int_1/Cast_1�
*stateful_uniform_full_int_1/RngReadAndSkipRngReadAndSkip1stateful_uniform_full_int_rngreadandskip_resource+stateful_uniform_full_int_1/Cast/x:output:0&stateful_uniform_full_int_1/Cast_1:y:0)^stateful_uniform_full_int/RngReadAndSkip*
_output_shapes
:2,
*stateful_uniform_full_int_1/RngReadAndSkip�
/stateful_uniform_full_int_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 21
/stateful_uniform_full_int_1/strided_slice/stack�
1stateful_uniform_full_int_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int_1/strided_slice/stack_1�
1stateful_uniform_full_int_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int_1/strided_slice/stack_2�
)stateful_uniform_full_int_1/strided_sliceStridedSlice2stateful_uniform_full_int_1/RngReadAndSkip:value:08stateful_uniform_full_int_1/strided_slice/stack:output:0:stateful_uniform_full_int_1/strided_slice/stack_1:output:0:stateful_uniform_full_int_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2+
)stateful_uniform_full_int_1/strided_slice�
#stateful_uniform_full_int_1/BitcastBitcast2stateful_uniform_full_int_1/strided_slice:output:0*
T0	*
_output_shapes
:*

type02%
#stateful_uniform_full_int_1/Bitcast�
1stateful_uniform_full_int_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int_1/strided_slice_1/stack�
3stateful_uniform_full_int_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:25
3stateful_uniform_full_int_1/strided_slice_1/stack_1�
3stateful_uniform_full_int_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:25
3stateful_uniform_full_int_1/strided_slice_1/stack_2�
+stateful_uniform_full_int_1/strided_slice_1StridedSlice2stateful_uniform_full_int_1/RngReadAndSkip:value:0:stateful_uniform_full_int_1/strided_slice_1/stack:output:0<stateful_uniform_full_int_1/strided_slice_1/stack_1:output:0<stateful_uniform_full_int_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2-
+stateful_uniform_full_int_1/strided_slice_1�
%stateful_uniform_full_int_1/Bitcast_1Bitcast4stateful_uniform_full_int_1/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02'
%stateful_uniform_full_int_1/Bitcast_1�
stateful_uniform_full_int_1/algConst*
_output_shapes
: *
dtype0*
value	B :2!
stateful_uniform_full_int_1/alg�
stateful_uniform_full_int_1StatelessRandomUniformFullIntV2*stateful_uniform_full_int_1/shape:output:0.stateful_uniform_full_int_1/Bitcast_1:output:0,stateful_uniform_full_int_1/Bitcast:output:0(stateful_uniform_full_int_1/alg:output:0*
_output_shapes
:*
dtype0	2
stateful_uniform_full_int_1f
zeros_like_1Const*
_output_shapes
:*
dtype0	*
valueB	R 2
zeros_like_1�
stack_1Pack$stateful_uniform_full_int_1:output:0zeros_like_1:output:0*
N*
T0	*
_output_shapes

:2	
stack_1
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice_1/stack�
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2
strided_slice_1/stack_1�
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_1/stack_2�
strided_slice_1StridedSlicestack_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_1�
0stateless_random_flip_up_down/control_dependencyIdentity(stateless_random_flip_left_right/add:z:0*
T0*7
_class-
+)loc:@stateless_random_flip_left_right/add*1
_output_shapes
:�����������22
0stateless_random_flip_up_down/control_dependency�
#stateless_random_flip_up_down/ShapeShape9stateless_random_flip_up_down/control_dependency:output:0*
T0*
_output_shapes
:2%
#stateless_random_flip_up_down/Shape�
1stateless_random_flip_up_down/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 23
1stateless_random_flip_up_down/strided_slice/stack�
3stateless_random_flip_up_down/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:25
3stateless_random_flip_up_down/strided_slice/stack_1�
3stateless_random_flip_up_down/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:25
3stateless_random_flip_up_down/strided_slice/stack_2�
+stateless_random_flip_up_down/strided_sliceStridedSlice,stateless_random_flip_up_down/Shape:output:0:stateless_random_flip_up_down/strided_slice/stack:output:0<stateless_random_flip_up_down/strided_slice/stack_1:output:0<stateless_random_flip_up_down/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2-
+stateless_random_flip_up_down/strided_slice�
<stateless_random_flip_up_down/stateless_random_uniform/shapePack4stateless_random_flip_up_down/strided_slice:output:0*
N*
T0*
_output_shapes
:2>
<stateless_random_flip_up_down/stateless_random_uniform/shape�
:stateless_random_flip_up_down/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2<
:stateless_random_flip_up_down/stateless_random_uniform/min�
:stateless_random_flip_up_down/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2<
:stateless_random_flip_up_down/stateless_random_uniform/max�
Sstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounterstrided_slice_1:output:0P^stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg* 
_output_shapes
::2U
Sstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�
Lstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgT^stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2N
Lstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�
Ostateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2Estateless_random_flip_up_down/stateless_random_uniform/shape:output:0Ystateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0]stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0Rstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2Q
Ostateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2�
:stateless_random_flip_up_down/stateless_random_uniform/subSubCstateless_random_flip_up_down/stateless_random_uniform/max:output:0Cstateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2<
:stateless_random_flip_up_down/stateless_random_uniform/sub�
:stateless_random_flip_up_down/stateless_random_uniform/mulMulXstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2:output:0>stateless_random_flip_up_down/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2<
:stateless_random_flip_up_down/stateless_random_uniform/mul�
6stateless_random_flip_up_down/stateless_random_uniformAddV2>stateless_random_flip_up_down/stateless_random_uniform/mul:z:0Cstateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������28
6stateless_random_flip_up_down/stateless_random_uniform�
-stateless_random_flip_up_down/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2/
-stateless_random_flip_up_down/Reshape/shape/1�
-stateless_random_flip_up_down/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2/
-stateless_random_flip_up_down/Reshape/shape/2�
-stateless_random_flip_up_down/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2/
-stateless_random_flip_up_down/Reshape/shape/3�
+stateless_random_flip_up_down/Reshape/shapePack4stateless_random_flip_up_down/strided_slice:output:06stateless_random_flip_up_down/Reshape/shape/1:output:06stateless_random_flip_up_down/Reshape/shape/2:output:06stateless_random_flip_up_down/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2-
+stateless_random_flip_up_down/Reshape/shape�
%stateless_random_flip_up_down/ReshapeReshape:stateless_random_flip_up_down/stateless_random_uniform:z:04stateless_random_flip_up_down/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2'
%stateless_random_flip_up_down/Reshape�
#stateless_random_flip_up_down/RoundRound.stateless_random_flip_up_down/Reshape:output:0*
T0*/
_output_shapes
:���������2%
#stateless_random_flip_up_down/Round�
,stateless_random_flip_up_down/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2.
,stateless_random_flip_up_down/ReverseV2/axis�
'stateless_random_flip_up_down/ReverseV2	ReverseV29stateless_random_flip_up_down/control_dependency:output:05stateless_random_flip_up_down/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2)
'stateless_random_flip_up_down/ReverseV2�
!stateless_random_flip_up_down/mulMul'stateless_random_flip_up_down/Round:y:00stateless_random_flip_up_down/ReverseV2:output:0*
T0*1
_output_shapes
:�����������2#
!stateless_random_flip_up_down/mul�
#stateless_random_flip_up_down/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2%
#stateless_random_flip_up_down/sub/x�
!stateless_random_flip_up_down/subSub,stateless_random_flip_up_down/sub/x:output:0'stateless_random_flip_up_down/Round:y:0*
T0*/
_output_shapes
:���������2#
!stateless_random_flip_up_down/sub�
#stateless_random_flip_up_down/mul_1Mul%stateless_random_flip_up_down/sub:z:09stateless_random_flip_up_down/control_dependency:output:0*
T0*1
_output_shapes
:�����������2%
#stateless_random_flip_up_down/mul_1�
!stateless_random_flip_up_down/addAddV2%stateless_random_flip_up_down/mul:z:0'stateless_random_flip_up_down/mul_1:z:0*
T0*1
_output_shapes
:�����������2#
!stateless_random_flip_up_down/add�
IdentityIdentity%stateless_random_flip_up_down/add:z:0^NoOp*
T0*1
_output_shapes
:�����������2

Identity�
NoOpNoOp)^stateful_uniform_full_int/RngReadAndSkip+^stateful_uniform_full_int_1/RngReadAndSkipP^stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgW^stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterM^stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgT^stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:�����������: 2T
(stateful_uniform_full_int/RngReadAndSkip(stateful_uniform_full_int/RngReadAndSkip2X
*stateful_uniform_full_int_1/RngReadAndSkip*stateful_uniform_full_int_1/RngReadAndSkip2�
Ostateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgOstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg2�
Vstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterVstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter2�
Lstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgLstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg2�
Sstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterSstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
S
-__inference_sequential_11_layer_call_fn_61815
resizing_1_input
identity�
PartitionedCallPartitionedCallresizing_1_input*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_11_layer_call_and_return_conditional_losses_601762
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:c _
1
_output_shapes
:�����������
*
_user_specified_nameresizing_1_input
��
�
H__inference_sequential_12_layer_call_and_return_conditional_losses_62349
random_flip_1_inputM
?random_flip_1_stateful_uniform_full_int_rngreadandskip_resource:	H
:random_rotation_1_stateful_uniform_rngreadandskip_resource:	
identity��6random_flip_1/stateful_uniform_full_int/RngReadAndSkip�8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip�]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�drandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�Zrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�arandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�1random_rotation_1/stateful_uniform/RngReadAndSkip�
-random_flip_1/stateful_uniform_full_int/shapeConst*
_output_shapes
:*
dtype0*
valueB:2/
-random_flip_1/stateful_uniform_full_int/shape�
-random_flip_1/stateful_uniform_full_int/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2/
-random_flip_1/stateful_uniform_full_int/Const�
,random_flip_1/stateful_uniform_full_int/ProdProd6random_flip_1/stateful_uniform_full_int/shape:output:06random_flip_1/stateful_uniform_full_int/Const:output:0*
T0*
_output_shapes
: 2.
,random_flip_1/stateful_uniform_full_int/Prod�
.random_flip_1/stateful_uniform_full_int/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :20
.random_flip_1/stateful_uniform_full_int/Cast/x�
.random_flip_1/stateful_uniform_full_int/Cast_1Cast5random_flip_1/stateful_uniform_full_int/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 20
.random_flip_1/stateful_uniform_full_int/Cast_1�
6random_flip_1/stateful_uniform_full_int/RngReadAndSkipRngReadAndSkip?random_flip_1_stateful_uniform_full_int_rngreadandskip_resource7random_flip_1/stateful_uniform_full_int/Cast/x:output:02random_flip_1/stateful_uniform_full_int/Cast_1:y:0*
_output_shapes
:28
6random_flip_1/stateful_uniform_full_int/RngReadAndSkip�
;random_flip_1/stateful_uniform_full_int/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2=
;random_flip_1/stateful_uniform_full_int/strided_slice/stack�
=random_flip_1/stateful_uniform_full_int/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2?
=random_flip_1/stateful_uniform_full_int/strided_slice/stack_1�
=random_flip_1/stateful_uniform_full_int/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2?
=random_flip_1/stateful_uniform_full_int/strided_slice/stack_2�
5random_flip_1/stateful_uniform_full_int/strided_sliceStridedSlice>random_flip_1/stateful_uniform_full_int/RngReadAndSkip:value:0Drandom_flip_1/stateful_uniform_full_int/strided_slice/stack:output:0Frandom_flip_1/stateful_uniform_full_int/strided_slice/stack_1:output:0Frandom_flip_1/stateful_uniform_full_int/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask27
5random_flip_1/stateful_uniform_full_int/strided_slice�
/random_flip_1/stateful_uniform_full_int/BitcastBitcast>random_flip_1/stateful_uniform_full_int/strided_slice:output:0*
T0	*
_output_shapes
:*

type021
/random_flip_1/stateful_uniform_full_int/Bitcast�
=random_flip_1/stateful_uniform_full_int/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2?
=random_flip_1/stateful_uniform_full_int/strided_slice_1/stack�
?random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1�
?random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2�
7random_flip_1/stateful_uniform_full_int/strided_slice_1StridedSlice>random_flip_1/stateful_uniform_full_int/RngReadAndSkip:value:0Frandom_flip_1/stateful_uniform_full_int/strided_slice_1/stack:output:0Hrandom_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1:output:0Hrandom_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:29
7random_flip_1/stateful_uniform_full_int/strided_slice_1�
1random_flip_1/stateful_uniform_full_int/Bitcast_1Bitcast@random_flip_1/stateful_uniform_full_int/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type023
1random_flip_1/stateful_uniform_full_int/Bitcast_1�
+random_flip_1/stateful_uniform_full_int/algConst*
_output_shapes
: *
dtype0*
value	B :2-
+random_flip_1/stateful_uniform_full_int/alg�
'random_flip_1/stateful_uniform_full_intStatelessRandomUniformFullIntV26random_flip_1/stateful_uniform_full_int/shape:output:0:random_flip_1/stateful_uniform_full_int/Bitcast_1:output:08random_flip_1/stateful_uniform_full_int/Bitcast:output:04random_flip_1/stateful_uniform_full_int/alg:output:0*
_output_shapes
:*
dtype0	2)
'random_flip_1/stateful_uniform_full_int~
random_flip_1/zeros_likeConst*
_output_shapes
:*
dtype0	*
valueB	R 2
random_flip_1/zeros_like�
random_flip_1/stackPack0random_flip_1/stateful_uniform_full_int:output:0!random_flip_1/zeros_like:output:0*
N*
T0	*
_output_shapes

:2
random_flip_1/stack�
!random_flip_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2#
!random_flip_1/strided_slice/stack�
#random_flip_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2%
#random_flip_1/strided_slice/stack_1�
#random_flip_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2%
#random_flip_1/strided_slice/stack_2�
random_flip_1/strided_sliceStridedSlicerandom_flip_1/stack:output:0*random_flip_1/strided_slice/stack:output:0,random_flip_1/strided_slice/stack_1:output:0,random_flip_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2
random_flip_1/strided_slice�
Arandom_flip_1/stateless_random_flip_left_right/control_dependencyIdentityrandom_flip_1_input*
T0*&
_class
loc:@random_flip_1_input*1
_output_shapes
:�����������2C
Arandom_flip_1/stateless_random_flip_left_right/control_dependency�
4random_flip_1/stateless_random_flip_left_right/ShapeShapeJrandom_flip_1/stateless_random_flip_left_right/control_dependency:output:0*
T0*
_output_shapes
:26
4random_flip_1/stateless_random_flip_left_right/Shape�
Brandom_flip_1/stateless_random_flip_left_right/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2D
Brandom_flip_1/stateless_random_flip_left_right/strided_slice/stack�
Drandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2F
Drandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_1�
Drandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2F
Drandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_2�
<random_flip_1/stateless_random_flip_left_right/strided_sliceStridedSlice=random_flip_1/stateless_random_flip_left_right/Shape:output:0Krandom_flip_1/stateless_random_flip_left_right/strided_slice/stack:output:0Mrandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_1:output:0Mrandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2>
<random_flip_1/stateless_random_flip_left_right/strided_slice�
Mrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shapePackErandom_flip_1/stateless_random_flip_left_right/strided_slice:output:0*
N*
T0*
_output_shapes
:2O
Mrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shape�
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2M
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min�
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2M
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/max�
drandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounter$random_flip_1/strided_slice:output:0* 
_output_shapes
::2f
drandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�
]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlge^random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2_
]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�
`random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2Vrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shape:output:0jrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0nrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0crandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2b
`random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2�
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/subSubTrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/max:output:0Trandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2M
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/sub�
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mulMulirandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2:output:0Orandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2M
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mul�
Grandom_flip_1/stateless_random_flip_left_right/stateless_random_uniformAddV2Orandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mul:z:0Trandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2I
Grandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform�
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2@
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/1�
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2@
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/2�
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2@
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/3�
<random_flip_1/stateless_random_flip_left_right/Reshape/shapePackErandom_flip_1/stateless_random_flip_left_right/strided_slice:output:0Grandom_flip_1/stateless_random_flip_left_right/Reshape/shape/1:output:0Grandom_flip_1/stateless_random_flip_left_right/Reshape/shape/2:output:0Grandom_flip_1/stateless_random_flip_left_right/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2>
<random_flip_1/stateless_random_flip_left_right/Reshape/shape�
6random_flip_1/stateless_random_flip_left_right/ReshapeReshapeKrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform:z:0Erandom_flip_1/stateless_random_flip_left_right/Reshape/shape:output:0*
T0*/
_output_shapes
:���������28
6random_flip_1/stateless_random_flip_left_right/Reshape�
4random_flip_1/stateless_random_flip_left_right/RoundRound?random_flip_1/stateless_random_flip_left_right/Reshape:output:0*
T0*/
_output_shapes
:���������26
4random_flip_1/stateless_random_flip_left_right/Round�
=random_flip_1/stateless_random_flip_left_right/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2?
=random_flip_1/stateless_random_flip_left_right/ReverseV2/axis�
8random_flip_1/stateless_random_flip_left_right/ReverseV2	ReverseV2Jrandom_flip_1/stateless_random_flip_left_right/control_dependency:output:0Frandom_flip_1/stateless_random_flip_left_right/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2:
8random_flip_1/stateless_random_flip_left_right/ReverseV2�
2random_flip_1/stateless_random_flip_left_right/mulMul8random_flip_1/stateless_random_flip_left_right/Round:y:0Arandom_flip_1/stateless_random_flip_left_right/ReverseV2:output:0*
T0*1
_output_shapes
:�����������24
2random_flip_1/stateless_random_flip_left_right/mul�
4random_flip_1/stateless_random_flip_left_right/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?26
4random_flip_1/stateless_random_flip_left_right/sub/x�
2random_flip_1/stateless_random_flip_left_right/subSub=random_flip_1/stateless_random_flip_left_right/sub/x:output:08random_flip_1/stateless_random_flip_left_right/Round:y:0*
T0*/
_output_shapes
:���������24
2random_flip_1/stateless_random_flip_left_right/sub�
4random_flip_1/stateless_random_flip_left_right/mul_1Mul6random_flip_1/stateless_random_flip_left_right/sub:z:0Jrandom_flip_1/stateless_random_flip_left_right/control_dependency:output:0*
T0*1
_output_shapes
:�����������26
4random_flip_1/stateless_random_flip_left_right/mul_1�
2random_flip_1/stateless_random_flip_left_right/addAddV26random_flip_1/stateless_random_flip_left_right/mul:z:08random_flip_1/stateless_random_flip_left_right/mul_1:z:0*
T0*1
_output_shapes
:�����������24
2random_flip_1/stateless_random_flip_left_right/add�
/random_flip_1/stateful_uniform_full_int_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:21
/random_flip_1/stateful_uniform_full_int_1/shape�
/random_flip_1/stateful_uniform_full_int_1/ConstConst*
_output_shapes
:*
dtype0*
valueB: 21
/random_flip_1/stateful_uniform_full_int_1/Const�
.random_flip_1/stateful_uniform_full_int_1/ProdProd8random_flip_1/stateful_uniform_full_int_1/shape:output:08random_flip_1/stateful_uniform_full_int_1/Const:output:0*
T0*
_output_shapes
: 20
.random_flip_1/stateful_uniform_full_int_1/Prod�
0random_flip_1/stateful_uniform_full_int_1/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :22
0random_flip_1/stateful_uniform_full_int_1/Cast/x�
0random_flip_1/stateful_uniform_full_int_1/Cast_1Cast7random_flip_1/stateful_uniform_full_int_1/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 22
0random_flip_1/stateful_uniform_full_int_1/Cast_1�
8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkipRngReadAndSkip?random_flip_1_stateful_uniform_full_int_rngreadandskip_resource9random_flip_1/stateful_uniform_full_int_1/Cast/x:output:04random_flip_1/stateful_uniform_full_int_1/Cast_1:y:07^random_flip_1/stateful_uniform_full_int/RngReadAndSkip*
_output_shapes
:2:
8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip�
=random_flip_1/stateful_uniform_full_int_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2?
=random_flip_1/stateful_uniform_full_int_1/strided_slice/stack�
?random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1�
?random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2�
7random_flip_1/stateful_uniform_full_int_1/strided_sliceStridedSlice@random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip:value:0Frandom_flip_1/stateful_uniform_full_int_1/strided_slice/stack:output:0Hrandom_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1:output:0Hrandom_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask29
7random_flip_1/stateful_uniform_full_int_1/strided_slice�
1random_flip_1/stateful_uniform_full_int_1/BitcastBitcast@random_flip_1/stateful_uniform_full_int_1/strided_slice:output:0*
T0	*
_output_shapes
:*

type023
1random_flip_1/stateful_uniform_full_int_1/Bitcast�
?random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack�
Arandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2C
Arandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1�
Arandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2C
Arandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2�
9random_flip_1/stateful_uniform_full_int_1/strided_slice_1StridedSlice@random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip:value:0Hrandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack:output:0Jrandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1:output:0Jrandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2;
9random_flip_1/stateful_uniform_full_int_1/strided_slice_1�
3random_flip_1/stateful_uniform_full_int_1/Bitcast_1BitcastBrandom_flip_1/stateful_uniform_full_int_1/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type025
3random_flip_1/stateful_uniform_full_int_1/Bitcast_1�
-random_flip_1/stateful_uniform_full_int_1/algConst*
_output_shapes
: *
dtype0*
value	B :2/
-random_flip_1/stateful_uniform_full_int_1/alg�
)random_flip_1/stateful_uniform_full_int_1StatelessRandomUniformFullIntV28random_flip_1/stateful_uniform_full_int_1/shape:output:0<random_flip_1/stateful_uniform_full_int_1/Bitcast_1:output:0:random_flip_1/stateful_uniform_full_int_1/Bitcast:output:06random_flip_1/stateful_uniform_full_int_1/alg:output:0*
_output_shapes
:*
dtype0	2+
)random_flip_1/stateful_uniform_full_int_1�
random_flip_1/zeros_like_1Const*
_output_shapes
:*
dtype0	*
valueB	R 2
random_flip_1/zeros_like_1�
random_flip_1/stack_1Pack2random_flip_1/stateful_uniform_full_int_1:output:0#random_flip_1/zeros_like_1:output:0*
N*
T0	*
_output_shapes

:2
random_flip_1/stack_1�
#random_flip_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        2%
#random_flip_1/strided_slice_1/stack�
%random_flip_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2'
%random_flip_1/strided_slice_1/stack_1�
%random_flip_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2'
%random_flip_1/strided_slice_1/stack_2�
random_flip_1/strided_slice_1StridedSlicerandom_flip_1/stack_1:output:0,random_flip_1/strided_slice_1/stack:output:0.random_flip_1/strided_slice_1/stack_1:output:0.random_flip_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2
random_flip_1/strided_slice_1�
>random_flip_1/stateless_random_flip_up_down/control_dependencyIdentity6random_flip_1/stateless_random_flip_left_right/add:z:0*
T0*E
_class;
97loc:@random_flip_1/stateless_random_flip_left_right/add*1
_output_shapes
:�����������2@
>random_flip_1/stateless_random_flip_up_down/control_dependency�
1random_flip_1/stateless_random_flip_up_down/ShapeShapeGrandom_flip_1/stateless_random_flip_up_down/control_dependency:output:0*
T0*
_output_shapes
:23
1random_flip_1/stateless_random_flip_up_down/Shape�
?random_flip_1/stateless_random_flip_up_down/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2A
?random_flip_1/stateless_random_flip_up_down/strided_slice/stack�
Arandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2C
Arandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_1�
Arandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2C
Arandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_2�
9random_flip_1/stateless_random_flip_up_down/strided_sliceStridedSlice:random_flip_1/stateless_random_flip_up_down/Shape:output:0Hrandom_flip_1/stateless_random_flip_up_down/strided_slice/stack:output:0Jrandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_1:output:0Jrandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2;
9random_flip_1/stateless_random_flip_up_down/strided_slice�
Jrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shapePackBrandom_flip_1/stateless_random_flip_up_down/strided_slice:output:0*
N*
T0*
_output_shapes
:2L
Jrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shape�
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2J
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min�
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2J
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/max�
arandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounter&random_flip_1/strided_slice_1:output:0^^random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg* 
_output_shapes
::2c
arandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�
Zrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgb^random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2\
Zrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�
]random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2Srandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shape:output:0grandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0krandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0`random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2_
]random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2�
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/subSubQrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/max:output:0Qrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2J
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/sub�
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mulMulfrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2:output:0Lrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2J
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mul�
Drandom_flip_1/stateless_random_flip_up_down/stateless_random_uniformAddV2Lrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mul:z:0Qrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2F
Drandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform�
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2=
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/1�
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2=
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/2�
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2=
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/3�
9random_flip_1/stateless_random_flip_up_down/Reshape/shapePackBrandom_flip_1/stateless_random_flip_up_down/strided_slice:output:0Drandom_flip_1/stateless_random_flip_up_down/Reshape/shape/1:output:0Drandom_flip_1/stateless_random_flip_up_down/Reshape/shape/2:output:0Drandom_flip_1/stateless_random_flip_up_down/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2;
9random_flip_1/stateless_random_flip_up_down/Reshape/shape�
3random_flip_1/stateless_random_flip_up_down/ReshapeReshapeHrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform:z:0Brandom_flip_1/stateless_random_flip_up_down/Reshape/shape:output:0*
T0*/
_output_shapes
:���������25
3random_flip_1/stateless_random_flip_up_down/Reshape�
1random_flip_1/stateless_random_flip_up_down/RoundRound<random_flip_1/stateless_random_flip_up_down/Reshape:output:0*
T0*/
_output_shapes
:���������23
1random_flip_1/stateless_random_flip_up_down/Round�
:random_flip_1/stateless_random_flip_up_down/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2<
:random_flip_1/stateless_random_flip_up_down/ReverseV2/axis�
5random_flip_1/stateless_random_flip_up_down/ReverseV2	ReverseV2Grandom_flip_1/stateless_random_flip_up_down/control_dependency:output:0Crandom_flip_1/stateless_random_flip_up_down/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������27
5random_flip_1/stateless_random_flip_up_down/ReverseV2�
/random_flip_1/stateless_random_flip_up_down/mulMul5random_flip_1/stateless_random_flip_up_down/Round:y:0>random_flip_1/stateless_random_flip_up_down/ReverseV2:output:0*
T0*1
_output_shapes
:�����������21
/random_flip_1/stateless_random_flip_up_down/mul�
1random_flip_1/stateless_random_flip_up_down/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?23
1random_flip_1/stateless_random_flip_up_down/sub/x�
/random_flip_1/stateless_random_flip_up_down/subSub:random_flip_1/stateless_random_flip_up_down/sub/x:output:05random_flip_1/stateless_random_flip_up_down/Round:y:0*
T0*/
_output_shapes
:���������21
/random_flip_1/stateless_random_flip_up_down/sub�
1random_flip_1/stateless_random_flip_up_down/mul_1Mul3random_flip_1/stateless_random_flip_up_down/sub:z:0Grandom_flip_1/stateless_random_flip_up_down/control_dependency:output:0*
T0*1
_output_shapes
:�����������23
1random_flip_1/stateless_random_flip_up_down/mul_1�
/random_flip_1/stateless_random_flip_up_down/addAddV23random_flip_1/stateless_random_flip_up_down/mul:z:05random_flip_1/stateless_random_flip_up_down/mul_1:z:0*
T0*1
_output_shapes
:�����������21
/random_flip_1/stateless_random_flip_up_down/add�
random_rotation_1/ShapeShape3random_flip_1/stateless_random_flip_up_down/add:z:0*
T0*
_output_shapes
:2
random_rotation_1/Shape�
%random_rotation_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2'
%random_rotation_1/strided_slice/stack�
'random_rotation_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2)
'random_rotation_1/strided_slice/stack_1�
'random_rotation_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2)
'random_rotation_1/strided_slice/stack_2�
random_rotation_1/strided_sliceStridedSlice random_rotation_1/Shape:output:0.random_rotation_1/strided_slice/stack:output:00random_rotation_1/strided_slice/stack_1:output:00random_rotation_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2!
random_rotation_1/strided_slice�
'random_rotation_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������2)
'random_rotation_1/strided_slice_1/stack�
)random_rotation_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������2+
)random_rotation_1/strided_slice_1/stack_1�
)random_rotation_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2+
)random_rotation_1/strided_slice_1/stack_2�
!random_rotation_1/strided_slice_1StridedSlice random_rotation_1/Shape:output:00random_rotation_1/strided_slice_1/stack:output:02random_rotation_1/strided_slice_1/stack_1:output:02random_rotation_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2#
!random_rotation_1/strided_slice_1�
random_rotation_1/CastCast*random_rotation_1/strided_slice_1:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
random_rotation_1/Cast�
'random_rotation_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������2)
'random_rotation_1/strided_slice_2/stack�
)random_rotation_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������2+
)random_rotation_1/strided_slice_2/stack_1�
)random_rotation_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2+
)random_rotation_1/strided_slice_2/stack_2�
!random_rotation_1/strided_slice_2StridedSlice random_rotation_1/Shape:output:00random_rotation_1/strided_slice_2/stack:output:02random_rotation_1/strided_slice_2/stack_1:output:02random_rotation_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2#
!random_rotation_1/strided_slice_2�
random_rotation_1/Cast_1Cast*random_rotation_1/strided_slice_2:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
random_rotation_1/Cast_1�
(random_rotation_1/stateful_uniform/shapePack(random_rotation_1/strided_slice:output:0*
N*
T0*
_output_shapes
:2*
(random_rotation_1/stateful_uniform/shape�
&random_rotation_1/stateful_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *|٠�2(
&random_rotation_1/stateful_uniform/min�
&random_rotation_1/stateful_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *|٠?2(
&random_rotation_1/stateful_uniform/max�
(random_rotation_1/stateful_uniform/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2*
(random_rotation_1/stateful_uniform/Const�
'random_rotation_1/stateful_uniform/ProdProd1random_rotation_1/stateful_uniform/shape:output:01random_rotation_1/stateful_uniform/Const:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/stateful_uniform/Prod�
)random_rotation_1/stateful_uniform/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2+
)random_rotation_1/stateful_uniform/Cast/x�
)random_rotation_1/stateful_uniform/Cast_1Cast0random_rotation_1/stateful_uniform/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2+
)random_rotation_1/stateful_uniform/Cast_1�
1random_rotation_1/stateful_uniform/RngReadAndSkipRngReadAndSkip:random_rotation_1_stateful_uniform_rngreadandskip_resource2random_rotation_1/stateful_uniform/Cast/x:output:0-random_rotation_1/stateful_uniform/Cast_1:y:0*
_output_shapes
:23
1random_rotation_1/stateful_uniform/RngReadAndSkip�
6random_rotation_1/stateful_uniform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 28
6random_rotation_1/stateful_uniform/strided_slice/stack�
8random_rotation_1/stateful_uniform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8random_rotation_1/stateful_uniform/strided_slice/stack_1�
8random_rotation_1/stateful_uniform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8random_rotation_1/stateful_uniform/strided_slice/stack_2�
0random_rotation_1/stateful_uniform/strided_sliceStridedSlice9random_rotation_1/stateful_uniform/RngReadAndSkip:value:0?random_rotation_1/stateful_uniform/strided_slice/stack:output:0Arandom_rotation_1/stateful_uniform/strided_slice/stack_1:output:0Arandom_rotation_1/stateful_uniform/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask22
0random_rotation_1/stateful_uniform/strided_slice�
*random_rotation_1/stateful_uniform/BitcastBitcast9random_rotation_1/stateful_uniform/strided_slice:output:0*
T0	*
_output_shapes
:*

type02,
*random_rotation_1/stateful_uniform/Bitcast�
8random_rotation_1/stateful_uniform/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2:
8random_rotation_1/stateful_uniform/strided_slice_1/stack�
:random_rotation_1/stateful_uniform/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2<
:random_rotation_1/stateful_uniform/strided_slice_1/stack_1�
:random_rotation_1/stateful_uniform/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2<
:random_rotation_1/stateful_uniform/strided_slice_1/stack_2�
2random_rotation_1/stateful_uniform/strided_slice_1StridedSlice9random_rotation_1/stateful_uniform/RngReadAndSkip:value:0Arandom_rotation_1/stateful_uniform/strided_slice_1/stack:output:0Crandom_rotation_1/stateful_uniform/strided_slice_1/stack_1:output:0Crandom_rotation_1/stateful_uniform/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:24
2random_rotation_1/stateful_uniform/strided_slice_1�
,random_rotation_1/stateful_uniform/Bitcast_1Bitcast;random_rotation_1/stateful_uniform/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02.
,random_rotation_1/stateful_uniform/Bitcast_1�
?random_rotation_1/stateful_uniform/StatelessRandomUniformV2/algConst*
_output_shapes
: *
dtype0*
value	B :2A
?random_rotation_1/stateful_uniform/StatelessRandomUniformV2/alg�
;random_rotation_1/stateful_uniform/StatelessRandomUniformV2StatelessRandomUniformV21random_rotation_1/stateful_uniform/shape:output:05random_rotation_1/stateful_uniform/Bitcast_1:output:03random_rotation_1/stateful_uniform/Bitcast:output:0Hrandom_rotation_1/stateful_uniform/StatelessRandomUniformV2/alg:output:0*#
_output_shapes
:���������2=
;random_rotation_1/stateful_uniform/StatelessRandomUniformV2�
&random_rotation_1/stateful_uniform/subSub/random_rotation_1/stateful_uniform/max:output:0/random_rotation_1/stateful_uniform/min:output:0*
T0*
_output_shapes
: 2(
&random_rotation_1/stateful_uniform/sub�
&random_rotation_1/stateful_uniform/mulMulDrandom_rotation_1/stateful_uniform/StatelessRandomUniformV2:output:0*random_rotation_1/stateful_uniform/sub:z:0*
T0*#
_output_shapes
:���������2(
&random_rotation_1/stateful_uniform/mul�
"random_rotation_1/stateful_uniformAddV2*random_rotation_1/stateful_uniform/mul:z:0/random_rotation_1/stateful_uniform/min:output:0*
T0*#
_output_shapes
:���������2$
"random_rotation_1/stateful_uniform�
'random_rotation_1/rotation_matrix/sub/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2)
'random_rotation_1/rotation_matrix/sub/y�
%random_rotation_1/rotation_matrix/subSubrandom_rotation_1/Cast_1:y:00random_rotation_1/rotation_matrix/sub/y:output:0*
T0*
_output_shapes
: 2'
%random_rotation_1/rotation_matrix/sub�
%random_rotation_1/rotation_matrix/CosCos&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/Cos�
)random_rotation_1/rotation_matrix/sub_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_1/y�
'random_rotation_1/rotation_matrix/sub_1Subrandom_rotation_1/Cast_1:y:02random_rotation_1/rotation_matrix/sub_1/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_1�
%random_rotation_1/rotation_matrix/mulMul)random_rotation_1/rotation_matrix/Cos:y:0+random_rotation_1/rotation_matrix/sub_1:z:0*
T0*#
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/mul�
%random_rotation_1/rotation_matrix/SinSin&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/Sin�
)random_rotation_1/rotation_matrix/sub_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_2/y�
'random_rotation_1/rotation_matrix/sub_2Subrandom_rotation_1/Cast:y:02random_rotation_1/rotation_matrix/sub_2/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_2�
'random_rotation_1/rotation_matrix/mul_1Mul)random_rotation_1/rotation_matrix/Sin:y:0+random_rotation_1/rotation_matrix/sub_2:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/mul_1�
'random_rotation_1/rotation_matrix/sub_3Sub)random_rotation_1/rotation_matrix/mul:z:0+random_rotation_1/rotation_matrix/mul_1:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/sub_3�
'random_rotation_1/rotation_matrix/sub_4Sub)random_rotation_1/rotation_matrix/sub:z:0+random_rotation_1/rotation_matrix/sub_3:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/sub_4�
+random_rotation_1/rotation_matrix/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2-
+random_rotation_1/rotation_matrix/truediv/y�
)random_rotation_1/rotation_matrix/truedivRealDiv+random_rotation_1/rotation_matrix/sub_4:z:04random_rotation_1/rotation_matrix/truediv/y:output:0*
T0*#
_output_shapes
:���������2+
)random_rotation_1/rotation_matrix/truediv�
)random_rotation_1/rotation_matrix/sub_5/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_5/y�
'random_rotation_1/rotation_matrix/sub_5Subrandom_rotation_1/Cast:y:02random_rotation_1/rotation_matrix/sub_5/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_5�
'random_rotation_1/rotation_matrix/Sin_1Sin&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Sin_1�
)random_rotation_1/rotation_matrix/sub_6/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_6/y�
'random_rotation_1/rotation_matrix/sub_6Subrandom_rotation_1/Cast_1:y:02random_rotation_1/rotation_matrix/sub_6/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_6�
'random_rotation_1/rotation_matrix/mul_2Mul+random_rotation_1/rotation_matrix/Sin_1:y:0+random_rotation_1/rotation_matrix/sub_6:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/mul_2�
'random_rotation_1/rotation_matrix/Cos_1Cos&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Cos_1�
)random_rotation_1/rotation_matrix/sub_7/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_7/y�
'random_rotation_1/rotation_matrix/sub_7Subrandom_rotation_1/Cast:y:02random_rotation_1/rotation_matrix/sub_7/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_7�
'random_rotation_1/rotation_matrix/mul_3Mul+random_rotation_1/rotation_matrix/Cos_1:y:0+random_rotation_1/rotation_matrix/sub_7:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/mul_3�
%random_rotation_1/rotation_matrix/addAddV2+random_rotation_1/rotation_matrix/mul_2:z:0+random_rotation_1/rotation_matrix/mul_3:z:0*
T0*#
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/add�
'random_rotation_1/rotation_matrix/sub_8Sub+random_rotation_1/rotation_matrix/sub_5:z:0)random_rotation_1/rotation_matrix/add:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/sub_8�
-random_rotation_1/rotation_matrix/truediv_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2/
-random_rotation_1/rotation_matrix/truediv_1/y�
+random_rotation_1/rotation_matrix/truediv_1RealDiv+random_rotation_1/rotation_matrix/sub_8:z:06random_rotation_1/rotation_matrix/truediv_1/y:output:0*
T0*#
_output_shapes
:���������2-
+random_rotation_1/rotation_matrix/truediv_1�
'random_rotation_1/rotation_matrix/ShapeShape&random_rotation_1/stateful_uniform:z:0*
T0*
_output_shapes
:2)
'random_rotation_1/rotation_matrix/Shape�
5random_rotation_1/rotation_matrix/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 27
5random_rotation_1/rotation_matrix/strided_slice/stack�
7random_rotation_1/rotation_matrix/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:29
7random_rotation_1/rotation_matrix/strided_slice/stack_1�
7random_rotation_1/rotation_matrix/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:29
7random_rotation_1/rotation_matrix/strided_slice/stack_2�
/random_rotation_1/rotation_matrix/strided_sliceStridedSlice0random_rotation_1/rotation_matrix/Shape:output:0>random_rotation_1/rotation_matrix/strided_slice/stack:output:0@random_rotation_1/rotation_matrix/strided_slice/stack_1:output:0@random_rotation_1/rotation_matrix/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask21
/random_rotation_1/rotation_matrix/strided_slice�
'random_rotation_1/rotation_matrix/Cos_2Cos&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Cos_2�
7random_rotation_1/rotation_matrix/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_1/stack�
9random_rotation_1/rotation_matrix/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_1/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_1/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_1StridedSlice+random_rotation_1/rotation_matrix/Cos_2:y:0@random_rotation_1/rotation_matrix/strided_slice_1/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_1/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_1/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_1�
'random_rotation_1/rotation_matrix/Sin_2Sin&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Sin_2�
7random_rotation_1/rotation_matrix/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_2/stack�
9random_rotation_1/rotation_matrix/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_2/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_2/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_2StridedSlice+random_rotation_1/rotation_matrix/Sin_2:y:0@random_rotation_1/rotation_matrix/strided_slice_2/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_2/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_2�
%random_rotation_1/rotation_matrix/NegNeg:random_rotation_1/rotation_matrix/strided_slice_2:output:0*
T0*'
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/Neg�
7random_rotation_1/rotation_matrix/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_3/stack�
9random_rotation_1/rotation_matrix/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_3/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_3/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_3StridedSlice-random_rotation_1/rotation_matrix/truediv:z:0@random_rotation_1/rotation_matrix/strided_slice_3/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_3/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_3�
'random_rotation_1/rotation_matrix/Sin_3Sin&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Sin_3�
7random_rotation_1/rotation_matrix/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_4/stack�
9random_rotation_1/rotation_matrix/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_4/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_4/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_4StridedSlice+random_rotation_1/rotation_matrix/Sin_3:y:0@random_rotation_1/rotation_matrix/strided_slice_4/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_4/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_4/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_4�
'random_rotation_1/rotation_matrix/Cos_3Cos&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Cos_3�
7random_rotation_1/rotation_matrix/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_5/stack�
9random_rotation_1/rotation_matrix/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_5/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_5/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_5StridedSlice+random_rotation_1/rotation_matrix/Cos_3:y:0@random_rotation_1/rotation_matrix/strided_slice_5/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_5/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_5/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_5�
7random_rotation_1/rotation_matrix/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_6/stack�
9random_rotation_1/rotation_matrix/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_6/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_6/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_6StridedSlice/random_rotation_1/rotation_matrix/truediv_1:z:0@random_rotation_1/rotation_matrix/strided_slice_6/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_6/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_6/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_6�
-random_rotation_1/rotation_matrix/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2/
-random_rotation_1/rotation_matrix/zeros/mul/y�
+random_rotation_1/rotation_matrix/zeros/mulMul8random_rotation_1/rotation_matrix/strided_slice:output:06random_rotation_1/rotation_matrix/zeros/mul/y:output:0*
T0*
_output_shapes
: 2-
+random_rotation_1/rotation_matrix/zeros/mul�
.random_rotation_1/rotation_matrix/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :�20
.random_rotation_1/rotation_matrix/zeros/Less/y�
,random_rotation_1/rotation_matrix/zeros/LessLess/random_rotation_1/rotation_matrix/zeros/mul:z:07random_rotation_1/rotation_matrix/zeros/Less/y:output:0*
T0*
_output_shapes
: 2.
,random_rotation_1/rotation_matrix/zeros/Less�
0random_rotation_1/rotation_matrix/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :22
0random_rotation_1/rotation_matrix/zeros/packed/1�
.random_rotation_1/rotation_matrix/zeros/packedPack8random_rotation_1/rotation_matrix/strided_slice:output:09random_rotation_1/rotation_matrix/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:20
.random_rotation_1/rotation_matrix/zeros/packed�
-random_rotation_1/rotation_matrix/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2/
-random_rotation_1/rotation_matrix/zeros/Const�
'random_rotation_1/rotation_matrix/zerosFill7random_rotation_1/rotation_matrix/zeros/packed:output:06random_rotation_1/rotation_matrix/zeros/Const:output:0*
T0*'
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/zeros�
-random_rotation_1/rotation_matrix/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2/
-random_rotation_1/rotation_matrix/concat/axis�
(random_rotation_1/rotation_matrix/concatConcatV2:random_rotation_1/rotation_matrix/strided_slice_1:output:0)random_rotation_1/rotation_matrix/Neg:y:0:random_rotation_1/rotation_matrix/strided_slice_3:output:0:random_rotation_1/rotation_matrix/strided_slice_4:output:0:random_rotation_1/rotation_matrix/strided_slice_5:output:0:random_rotation_1/rotation_matrix/strided_slice_6:output:00random_rotation_1/rotation_matrix/zeros:output:06random_rotation_1/rotation_matrix/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2*
(random_rotation_1/rotation_matrix/concat�
!random_rotation_1/transform/ShapeShape3random_flip_1/stateless_random_flip_up_down/add:z:0*
T0*
_output_shapes
:2#
!random_rotation_1/transform/Shape�
/random_rotation_1/transform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:21
/random_rotation_1/transform/strided_slice/stack�
1random_rotation_1/transform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:23
1random_rotation_1/transform/strided_slice/stack_1�
1random_rotation_1/transform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:23
1random_rotation_1/transform/strided_slice/stack_2�
)random_rotation_1/transform/strided_sliceStridedSlice*random_rotation_1/transform/Shape:output:08random_rotation_1/transform/strided_slice/stack:output:0:random_rotation_1/transform/strided_slice/stack_1:output:0:random_rotation_1/transform/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2+
)random_rotation_1/transform/strided_slice�
&random_rotation_1/transform/fill_valueConst*
_output_shapes
: *
dtype0*
valueB
 *    2(
&random_rotation_1/transform/fill_value�
6random_rotation_1/transform/ImageProjectiveTransformV3ImageProjectiveTransformV33random_flip_1/stateless_random_flip_up_down/add:z:01random_rotation_1/rotation_matrix/concat:output:02random_rotation_1/transform/strided_slice:output:0/random_rotation_1/transform/fill_value:output:0*1
_output_shapes
:�����������*
dtype0*
	fill_mode	REFLECT*
interpolation
BILINEAR28
6random_rotation_1/transform/ImageProjectiveTransformV3�
IdentityIdentityKrandom_rotation_1/transform/ImageProjectiveTransformV3:transformed_images:0^NoOp*
T0*1
_output_shapes
:�����������2

Identity�
NoOpNoOp7^random_flip_1/stateful_uniform_full_int/RngReadAndSkip9^random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip^^random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlge^random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter[^random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgb^random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter2^random_rotation_1/stateful_uniform/RngReadAndSkip*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 2p
6random_flip_1/stateful_uniform_full_int/RngReadAndSkip6random_flip_1/stateful_uniform_full_int/RngReadAndSkip2t
8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip2�
]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg2�
drandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterdrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter2�
Zrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgZrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg2�
arandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterarandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter2f
1random_rotation_1/stateful_uniform/RngReadAndSkip1random_rotation_1/stateful_uniform/RngReadAndSkip:f b
1
_output_shapes
:�����������
-
_user_specified_namerandom_flip_1_input
�
L
0__inference_max_pooling2d_74_layer_call_fn_62459

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_606692
PartitionedCallt
IdentityIdentityPartitionedCall:output:0*
T0*/
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������<<@:W S
/
_output_shapes
:���������<<@
 
_user_specified_nameinputs
�W
�

 __inference__wrapped_model_60122
sequential_11_inputP
6sequential_23_conv2d_72_conv2d_readvariableop_resource: E
7sequential_23_conv2d_72_biasadd_readvariableop_resource: P
6sequential_23_conv2d_73_conv2d_readvariableop_resource: @E
7sequential_23_conv2d_73_biasadd_readvariableop_resource:@P
6sequential_23_conv2d_74_conv2d_readvariableop_resource:@@E
7sequential_23_conv2d_74_biasadd_readvariableop_resource:@I
5sequential_23_dense_38_matmul_readvariableop_resource:
��@D
6sequential_23_dense_38_biasadd_readvariableop_resource:@G
5sequential_23_dense_39_matmul_readvariableop_resource:@
D
6sequential_23_dense_39_biasadd_readvariableop_resource:

identity��.sequential_23/conv2d_72/BiasAdd/ReadVariableOp�-sequential_23/conv2d_72/Conv2D/ReadVariableOp�.sequential_23/conv2d_73/BiasAdd/ReadVariableOp�-sequential_23/conv2d_73/Conv2D/ReadVariableOp�.sequential_23/conv2d_74/BiasAdd/ReadVariableOp�-sequential_23/conv2d_74/Conv2D/ReadVariableOp�-sequential_23/dense_38/BiasAdd/ReadVariableOp�,sequential_23/dense_38/MatMul/ReadVariableOp�-sequential_23/dense_39/BiasAdd/ReadVariableOp�,sequential_23/dense_39/MatMul/ReadVariableOp�
2sequential_23/sequential_11/resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      24
2sequential_23/sequential_11/resizing_1/resize/size�
<sequential_23/sequential_11/resizing_1/resize/ResizeBilinearResizeBilinearsequential_11_input;sequential_23/sequential_11/resizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(2>
<sequential_23/sequential_11/resizing_1/resize/ResizeBilinear�
.sequential_23/sequential_11/rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;20
.sequential_23/sequential_11/rescaling_1/Cast/x�
0sequential_23/sequential_11/rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    22
0sequential_23/sequential_11/rescaling_1/Cast_1/x�
+sequential_23/sequential_11/rescaling_1/mulMulMsequential_23/sequential_11/resizing_1/resize/ResizeBilinear:resized_images:07sequential_23/sequential_11/rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2-
+sequential_23/sequential_11/rescaling_1/mul�
+sequential_23/sequential_11/rescaling_1/addAddV2/sequential_23/sequential_11/rescaling_1/mul:z:09sequential_23/sequential_11/rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2-
+sequential_23/sequential_11/rescaling_1/add�
-sequential_23/conv2d_72/Conv2D/ReadVariableOpReadVariableOp6sequential_23_conv2d_72_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype02/
-sequential_23/conv2d_72/Conv2D/ReadVariableOp�
sequential_23/conv2d_72/Conv2DConv2D/sequential_23/sequential_11/rescaling_1/add:z:05sequential_23/conv2d_72/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� *
paddingVALID*
strides
2 
sequential_23/conv2d_72/Conv2D�
.sequential_23/conv2d_72/BiasAdd/ReadVariableOpReadVariableOp7sequential_23_conv2d_72_biasadd_readvariableop_resource*
_output_shapes
: *
dtype020
.sequential_23/conv2d_72/BiasAdd/ReadVariableOp�
sequential_23/conv2d_72/BiasAddBiasAdd'sequential_23/conv2d_72/Conv2D:output:06sequential_23/conv2d_72/BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� 2!
sequential_23/conv2d_72/BiasAdd�
sequential_23/conv2d_72/ReluRelu(sequential_23/conv2d_72/BiasAdd:output:0*
T0*1
_output_shapes
:����������� 2
sequential_23/conv2d_72/Relu�
&sequential_23/max_pooling2d_72/MaxPoolMaxPool*sequential_23/conv2d_72/Relu:activations:0*/
_output_shapes
:���������~~ *
ksize
*
paddingVALID*
strides
2(
&sequential_23/max_pooling2d_72/MaxPool�
-sequential_23/conv2d_73/Conv2D/ReadVariableOpReadVariableOp6sequential_23_conv2d_73_conv2d_readvariableop_resource*&
_output_shapes
: @*
dtype02/
-sequential_23/conv2d_73/Conv2D/ReadVariableOp�
sequential_23/conv2d_73/Conv2DConv2D/sequential_23/max_pooling2d_72/MaxPool:output:05sequential_23/conv2d_73/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@*
paddingVALID*
strides
2 
sequential_23/conv2d_73/Conv2D�
.sequential_23/conv2d_73/BiasAdd/ReadVariableOpReadVariableOp7sequential_23_conv2d_73_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype020
.sequential_23/conv2d_73/BiasAdd/ReadVariableOp�
sequential_23/conv2d_73/BiasAddBiasAdd'sequential_23/conv2d_73/Conv2D:output:06sequential_23/conv2d_73/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@2!
sequential_23/conv2d_73/BiasAdd�
sequential_23/conv2d_73/ReluRelu(sequential_23/conv2d_73/BiasAdd:output:0*
T0*/
_output_shapes
:���������||@2
sequential_23/conv2d_73/Relu�
&sequential_23/max_pooling2d_73/MaxPoolMaxPool*sequential_23/conv2d_73/Relu:activations:0*/
_output_shapes
:���������>>@*
ksize
*
paddingVALID*
strides
2(
&sequential_23/max_pooling2d_73/MaxPool�
-sequential_23/conv2d_74/Conv2D/ReadVariableOpReadVariableOp6sequential_23_conv2d_74_conv2d_readvariableop_resource*&
_output_shapes
:@@*
dtype02/
-sequential_23/conv2d_74/Conv2D/ReadVariableOp�
sequential_23/conv2d_74/Conv2DConv2D/sequential_23/max_pooling2d_73/MaxPool:output:05sequential_23/conv2d_74/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@*
paddingVALID*
strides
2 
sequential_23/conv2d_74/Conv2D�
.sequential_23/conv2d_74/BiasAdd/ReadVariableOpReadVariableOp7sequential_23_conv2d_74_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype020
.sequential_23/conv2d_74/BiasAdd/ReadVariableOp�
sequential_23/conv2d_74/BiasAddBiasAdd'sequential_23/conv2d_74/Conv2D:output:06sequential_23/conv2d_74/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@2!
sequential_23/conv2d_74/BiasAdd�
sequential_23/conv2d_74/ReluRelu(sequential_23/conv2d_74/BiasAdd:output:0*
T0*/
_output_shapes
:���������<<@2
sequential_23/conv2d_74/Relu�
&sequential_23/max_pooling2d_74/MaxPoolMaxPool*sequential_23/conv2d_74/Relu:activations:0*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
2(
&sequential_23/max_pooling2d_74/MaxPool�
sequential_23/flatten_19/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� �  2 
sequential_23/flatten_19/Const�
 sequential_23/flatten_19/ReshapeReshape/sequential_23/max_pooling2d_74/MaxPool:output:0'sequential_23/flatten_19/Const:output:0*
T0*)
_output_shapes
:�����������2"
 sequential_23/flatten_19/Reshape�
,sequential_23/dense_38/MatMul/ReadVariableOpReadVariableOp5sequential_23_dense_38_matmul_readvariableop_resource* 
_output_shapes
:
��@*
dtype02.
,sequential_23/dense_38/MatMul/ReadVariableOp�
sequential_23/dense_38/MatMulMatMul)sequential_23/flatten_19/Reshape:output:04sequential_23/dense_38/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
sequential_23/dense_38/MatMul�
-sequential_23/dense_38/BiasAdd/ReadVariableOpReadVariableOp6sequential_23_dense_38_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02/
-sequential_23/dense_38/BiasAdd/ReadVariableOp�
sequential_23/dense_38/BiasAddBiasAdd'sequential_23/dense_38/MatMul:product:05sequential_23/dense_38/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2 
sequential_23/dense_38/BiasAdd�
sequential_23/dense_38/ReluRelu'sequential_23/dense_38/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
sequential_23/dense_38/Relu�
,sequential_23/dense_39/MatMul/ReadVariableOpReadVariableOp5sequential_23_dense_39_matmul_readvariableop_resource*
_output_shapes

:@
*
dtype02.
,sequential_23/dense_39/MatMul/ReadVariableOp�
sequential_23/dense_39/MatMulMatMul)sequential_23/dense_38/Relu:activations:04sequential_23/dense_39/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
sequential_23/dense_39/MatMul�
-sequential_23/dense_39/BiasAdd/ReadVariableOpReadVariableOp6sequential_23_dense_39_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02/
-sequential_23/dense_39/BiasAdd/ReadVariableOp�
sequential_23/dense_39/BiasAddBiasAdd'sequential_23/dense_39/MatMul:product:05sequential_23/dense_39/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2 
sequential_23/dense_39/BiasAdd�
sequential_23/dense_39/SoftmaxSoftmax'sequential_23/dense_39/BiasAdd:output:0*
T0*'
_output_shapes
:���������
2 
sequential_23/dense_39/Softmax�
IdentityIdentity(sequential_23/dense_39/Softmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity�
NoOpNoOp/^sequential_23/conv2d_72/BiasAdd/ReadVariableOp.^sequential_23/conv2d_72/Conv2D/ReadVariableOp/^sequential_23/conv2d_73/BiasAdd/ReadVariableOp.^sequential_23/conv2d_73/Conv2D/ReadVariableOp/^sequential_23/conv2d_74/BiasAdd/ReadVariableOp.^sequential_23/conv2d_74/Conv2D/ReadVariableOp.^sequential_23/dense_38/BiasAdd/ReadVariableOp-^sequential_23/dense_38/MatMul/ReadVariableOp.^sequential_23/dense_39/BiasAdd/ReadVariableOp-^sequential_23/dense_39/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:�����������: : : : : : : : : : 2`
.sequential_23/conv2d_72/BiasAdd/ReadVariableOp.sequential_23/conv2d_72/BiasAdd/ReadVariableOp2^
-sequential_23/conv2d_72/Conv2D/ReadVariableOp-sequential_23/conv2d_72/Conv2D/ReadVariableOp2`
.sequential_23/conv2d_73/BiasAdd/ReadVariableOp.sequential_23/conv2d_73/BiasAdd/ReadVariableOp2^
-sequential_23/conv2d_73/Conv2D/ReadVariableOp-sequential_23/conv2d_73/Conv2D/ReadVariableOp2`
.sequential_23/conv2d_74/BiasAdd/ReadVariableOp.sequential_23/conv2d_74/BiasAdd/ReadVariableOp2^
-sequential_23/conv2d_74/Conv2D/ReadVariableOp-sequential_23/conv2d_74/Conv2D/ReadVariableOp2^
-sequential_23/dense_38/BiasAdd/ReadVariableOp-sequential_23/dense_38/BiasAdd/ReadVariableOp2\
,sequential_23/dense_38/MatMul/ReadVariableOp,sequential_23/dense_38/MatMul/ReadVariableOp2^
-sequential_23/dense_39/BiasAdd/ReadVariableOp-sequential_23/dense_39/BiasAdd/ReadVariableOp2\
,sequential_23/dense_39/MatMul/ReadVariableOp,sequential_23/dense_39/MatMul/ReadVariableOp:f b
1
_output_shapes
:�����������
-
_user_specified_namesequential_11_input
�
b
F__inference_rescaling_1_layer_call_and_return_conditional_losses_60145

inputs
identityU
Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2
Cast/xY
Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2

Cast_1/xf
mulMulinputsCast/x:output:0*
T0*1
_output_shapes
:�����������2
mulk
addAddV2mul:z:0Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
adde
IdentityIdentityadd:z:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�E
�
H__inference_sequential_23_layer_call_and_return_conditional_losses_61520
sequential_11_inputB
(conv2d_72_conv2d_readvariableop_resource: 7
)conv2d_72_biasadd_readvariableop_resource: B
(conv2d_73_conv2d_readvariableop_resource: @7
)conv2d_73_biasadd_readvariableop_resource:@B
(conv2d_74_conv2d_readvariableop_resource:@@7
)conv2d_74_biasadd_readvariableop_resource:@;
'dense_38_matmul_readvariableop_resource:
��@6
(dense_38_biasadd_readvariableop_resource:@9
'dense_39_matmul_readvariableop_resource:@
6
(dense_39_biasadd_readvariableop_resource:

identity�� conv2d_72/BiasAdd/ReadVariableOp�conv2d_72/Conv2D/ReadVariableOp� conv2d_73/BiasAdd/ReadVariableOp�conv2d_73/Conv2D/ReadVariableOp� conv2d_74/BiasAdd/ReadVariableOp�conv2d_74/Conv2D/ReadVariableOp�dense_38/BiasAdd/ReadVariableOp�dense_38/MatMul/ReadVariableOp�dense_39/BiasAdd/ReadVariableOp�dense_39/MatMul/ReadVariableOp�
$sequential_11/resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2&
$sequential_11/resizing_1/resize/size�
.sequential_11/resizing_1/resize/ResizeBilinearResizeBilinearsequential_11_input-sequential_11/resizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(20
.sequential_11/resizing_1/resize/ResizeBilinear�
 sequential_11/rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2"
 sequential_11/rescaling_1/Cast/x�
"sequential_11/rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2$
"sequential_11/rescaling_1/Cast_1/x�
sequential_11/rescaling_1/mulMul?sequential_11/resizing_1/resize/ResizeBilinear:resized_images:0)sequential_11/rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2
sequential_11/rescaling_1/mul�
sequential_11/rescaling_1/addAddV2!sequential_11/rescaling_1/mul:z:0+sequential_11/rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
sequential_11/rescaling_1/add�
conv2d_72/Conv2D/ReadVariableOpReadVariableOp(conv2d_72_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype02!
conv2d_72/Conv2D/ReadVariableOp�
conv2d_72/Conv2DConv2D!sequential_11/rescaling_1/add:z:0'conv2d_72/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� *
paddingVALID*
strides
2
conv2d_72/Conv2D�
 conv2d_72/BiasAdd/ReadVariableOpReadVariableOp)conv2d_72_biasadd_readvariableop_resource*
_output_shapes
: *
dtype02"
 conv2d_72/BiasAdd/ReadVariableOp�
conv2d_72/BiasAddBiasAddconv2d_72/Conv2D:output:0(conv2d_72/BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� 2
conv2d_72/BiasAdd�
conv2d_72/ReluReluconv2d_72/BiasAdd:output:0*
T0*1
_output_shapes
:����������� 2
conv2d_72/Relu�
max_pooling2d_72/MaxPoolMaxPoolconv2d_72/Relu:activations:0*/
_output_shapes
:���������~~ *
ksize
*
paddingVALID*
strides
2
max_pooling2d_72/MaxPool�
conv2d_73/Conv2D/ReadVariableOpReadVariableOp(conv2d_73_conv2d_readvariableop_resource*&
_output_shapes
: @*
dtype02!
conv2d_73/Conv2D/ReadVariableOp�
conv2d_73/Conv2DConv2D!max_pooling2d_72/MaxPool:output:0'conv2d_73/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@*
paddingVALID*
strides
2
conv2d_73/Conv2D�
 conv2d_73/BiasAdd/ReadVariableOpReadVariableOp)conv2d_73_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02"
 conv2d_73/BiasAdd/ReadVariableOp�
conv2d_73/BiasAddBiasAddconv2d_73/Conv2D:output:0(conv2d_73/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@2
conv2d_73/BiasAdd~
conv2d_73/ReluReluconv2d_73/BiasAdd:output:0*
T0*/
_output_shapes
:���������||@2
conv2d_73/Relu�
max_pooling2d_73/MaxPoolMaxPoolconv2d_73/Relu:activations:0*/
_output_shapes
:���������>>@*
ksize
*
paddingVALID*
strides
2
max_pooling2d_73/MaxPool�
conv2d_74/Conv2D/ReadVariableOpReadVariableOp(conv2d_74_conv2d_readvariableop_resource*&
_output_shapes
:@@*
dtype02!
conv2d_74/Conv2D/ReadVariableOp�
conv2d_74/Conv2DConv2D!max_pooling2d_73/MaxPool:output:0'conv2d_74/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@*
paddingVALID*
strides
2
conv2d_74/Conv2D�
 conv2d_74/BiasAdd/ReadVariableOpReadVariableOp)conv2d_74_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02"
 conv2d_74/BiasAdd/ReadVariableOp�
conv2d_74/BiasAddBiasAddconv2d_74/Conv2D:output:0(conv2d_74/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@2
conv2d_74/BiasAdd~
conv2d_74/ReluReluconv2d_74/BiasAdd:output:0*
T0*/
_output_shapes
:���������<<@2
conv2d_74/Relu�
max_pooling2d_74/MaxPoolMaxPoolconv2d_74/Relu:activations:0*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
2
max_pooling2d_74/MaxPoolu
flatten_19/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� �  2
flatten_19/Const�
flatten_19/ReshapeReshape!max_pooling2d_74/MaxPool:output:0flatten_19/Const:output:0*
T0*)
_output_shapes
:�����������2
flatten_19/Reshape�
dense_38/MatMul/ReadVariableOpReadVariableOp'dense_38_matmul_readvariableop_resource* 
_output_shapes
:
��@*
dtype02 
dense_38/MatMul/ReadVariableOp�
dense_38/MatMulMatMulflatten_19/Reshape:output:0&dense_38/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_38/MatMul�
dense_38/BiasAdd/ReadVariableOpReadVariableOp(dense_38_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02!
dense_38/BiasAdd/ReadVariableOp�
dense_38/BiasAddBiasAdddense_38/MatMul:product:0'dense_38/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_38/BiasAdds
dense_38/ReluReludense_38/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
dense_38/Relu�
dense_39/MatMul/ReadVariableOpReadVariableOp'dense_39_matmul_readvariableop_resource*
_output_shapes

:@
*
dtype02 
dense_39/MatMul/ReadVariableOp�
dense_39/MatMulMatMuldense_38/Relu:activations:0&dense_39/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_39/MatMul�
dense_39/BiasAdd/ReadVariableOpReadVariableOp(dense_39_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_39/BiasAdd/ReadVariableOp�
dense_39/BiasAddBiasAdddense_39/MatMul:product:0'dense_39/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_39/BiasAdd|
dense_39/SoftmaxSoftmaxdense_39/BiasAdd:output:0*
T0*'
_output_shapes
:���������
2
dense_39/Softmaxu
IdentityIdentitydense_39/Softmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity�
NoOpNoOp!^conv2d_72/BiasAdd/ReadVariableOp ^conv2d_72/Conv2D/ReadVariableOp!^conv2d_73/BiasAdd/ReadVariableOp ^conv2d_73/Conv2D/ReadVariableOp!^conv2d_74/BiasAdd/ReadVariableOp ^conv2d_74/Conv2D/ReadVariableOp ^dense_38/BiasAdd/ReadVariableOp^dense_38/MatMul/ReadVariableOp ^dense_39/BiasAdd/ReadVariableOp^dense_39/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:�����������: : : : : : : : : : 2D
 conv2d_72/BiasAdd/ReadVariableOp conv2d_72/BiasAdd/ReadVariableOp2B
conv2d_72/Conv2D/ReadVariableOpconv2d_72/Conv2D/ReadVariableOp2D
 conv2d_73/BiasAdd/ReadVariableOp conv2d_73/BiasAdd/ReadVariableOp2B
conv2d_73/Conv2D/ReadVariableOpconv2d_73/Conv2D/ReadVariableOp2D
 conv2d_74/BiasAdd/ReadVariableOp conv2d_74/BiasAdd/ReadVariableOp2B
conv2d_74/Conv2D/ReadVariableOpconv2d_74/Conv2D/ReadVariableOp2B
dense_38/BiasAdd/ReadVariableOpdense_38/BiasAdd/ReadVariableOp2@
dense_38/MatMul/ReadVariableOpdense_38/MatMul/ReadVariableOp2B
dense_39/BiasAdd/ReadVariableOpdense_39/BiasAdd/ReadVariableOp2@
dense_39/MatMul/ReadVariableOpdense_39/MatMul/ReadVariableOp:f b
1
_output_shapes
:�����������
-
_user_specified_namesequential_11_input
�
�
-__inference_sequential_23_layer_call_fn_61145
sequential_11_input
unknown:	
	unknown_0:	#
	unknown_1: 
	unknown_2: #
	unknown_3: @
	unknown_4:@#
	unknown_5:@@
	unknown_6:@
	unknown_7:
��@
	unknown_8:@
	unknown_9:@


unknown_10:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallsequential_11_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*,
_read_only_resource_inputs

	
*/
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_23_layer_call_and_return_conditional_losses_608742
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:�����������: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
1
_output_shapes
:�����������
-
_user_specified_namesequential_11_input
�
a
E__inference_flatten_19_layer_call_and_return_conditional_losses_62480

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"���� �  2
Consti
ReshapeReshapeinputsConst:output:0*
T0*)
_output_shapes
:�����������2	
Reshapef
IdentityIdentityReshape:output:0*
T0*)
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������@:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
��
�
H__inference_sequential_23_layer_call_and_return_conditional_losses_61795
sequential_11_input[
Msequential_12_random_flip_1_stateful_uniform_full_int_rngreadandskip_resource:	V
Hsequential_12_random_rotation_1_stateful_uniform_rngreadandskip_resource:	B
(conv2d_72_conv2d_readvariableop_resource: 7
)conv2d_72_biasadd_readvariableop_resource: B
(conv2d_73_conv2d_readvariableop_resource: @7
)conv2d_73_biasadd_readvariableop_resource:@B
(conv2d_74_conv2d_readvariableop_resource:@@7
)conv2d_74_biasadd_readvariableop_resource:@;
'dense_38_matmul_readvariableop_resource:
��@6
(dense_38_biasadd_readvariableop_resource:@9
'dense_39_matmul_readvariableop_resource:@
6
(dense_39_biasadd_readvariableop_resource:

identity�� conv2d_72/BiasAdd/ReadVariableOp�conv2d_72/Conv2D/ReadVariableOp� conv2d_73/BiasAdd/ReadVariableOp�conv2d_73/Conv2D/ReadVariableOp� conv2d_74/BiasAdd/ReadVariableOp�conv2d_74/Conv2D/ReadVariableOp�dense_38/BiasAdd/ReadVariableOp�dense_38/MatMul/ReadVariableOp�dense_39/BiasAdd/ReadVariableOp�dense_39/MatMul/ReadVariableOp�Dsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip�Fsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip�ksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�rsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�hsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�osequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip�
$sequential_11/resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2&
$sequential_11/resizing_1/resize/size�
.sequential_11/resizing_1/resize/ResizeBilinearResizeBilinearsequential_11_input-sequential_11/resizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(20
.sequential_11/resizing_1/resize/ResizeBilinear�
 sequential_11/rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2"
 sequential_11/rescaling_1/Cast/x�
"sequential_11/rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2$
"sequential_11/rescaling_1/Cast_1/x�
sequential_11/rescaling_1/mulMul?sequential_11/resizing_1/resize/ResizeBilinear:resized_images:0)sequential_11/rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2
sequential_11/rescaling_1/mul�
sequential_11/rescaling_1/addAddV2!sequential_11/rescaling_1/mul:z:0+sequential_11/rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
sequential_11/rescaling_1/add�
;sequential_12/random_flip_1/stateful_uniform_full_int/shapeConst*
_output_shapes
:*
dtype0*
valueB:2=
;sequential_12/random_flip_1/stateful_uniform_full_int/shape�
;sequential_12/random_flip_1/stateful_uniform_full_int/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2=
;sequential_12/random_flip_1/stateful_uniform_full_int/Const�
:sequential_12/random_flip_1/stateful_uniform_full_int/ProdProdDsequential_12/random_flip_1/stateful_uniform_full_int/shape:output:0Dsequential_12/random_flip_1/stateful_uniform_full_int/Const:output:0*
T0*
_output_shapes
: 2<
:sequential_12/random_flip_1/stateful_uniform_full_int/Prod�
<sequential_12/random_flip_1/stateful_uniform_full_int/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2>
<sequential_12/random_flip_1/stateful_uniform_full_int/Cast/x�
<sequential_12/random_flip_1/stateful_uniform_full_int/Cast_1CastCsequential_12/random_flip_1/stateful_uniform_full_int/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2>
<sequential_12/random_flip_1/stateful_uniform_full_int/Cast_1�
Dsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkipRngReadAndSkipMsequential_12_random_flip_1_stateful_uniform_full_int_rngreadandskip_resourceEsequential_12/random_flip_1/stateful_uniform_full_int/Cast/x:output:0@sequential_12/random_flip_1/stateful_uniform_full_int/Cast_1:y:0*
_output_shapes
:2F
Dsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip�
Isequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2K
Isequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack�
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2M
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_1�
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2M
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_2�
Csequential_12/random_flip_1/stateful_uniform_full_int/strided_sliceStridedSliceLsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip:value:0Rsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack:output:0Tsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_1:output:0Tsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2E
Csequential_12/random_flip_1/stateful_uniform_full_int/strided_slice�
=sequential_12/random_flip_1/stateful_uniform_full_int/BitcastBitcastLsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice:output:0*
T0	*
_output_shapes
:*

type02?
=sequential_12/random_flip_1/stateful_uniform_full_int/Bitcast�
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2M
Ksequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack�
Msequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1�
Msequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2�
Esequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1StridedSliceLsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip:value:0Tsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack:output:0Vsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1:output:0Vsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2G
Esequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1�
?sequential_12/random_flip_1/stateful_uniform_full_int/Bitcast_1BitcastNsequential_12/random_flip_1/stateful_uniform_full_int/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02A
?sequential_12/random_flip_1/stateful_uniform_full_int/Bitcast_1�
9sequential_12/random_flip_1/stateful_uniform_full_int/algConst*
_output_shapes
: *
dtype0*
value	B :2;
9sequential_12/random_flip_1/stateful_uniform_full_int/alg�
5sequential_12/random_flip_1/stateful_uniform_full_intStatelessRandomUniformFullIntV2Dsequential_12/random_flip_1/stateful_uniform_full_int/shape:output:0Hsequential_12/random_flip_1/stateful_uniform_full_int/Bitcast_1:output:0Fsequential_12/random_flip_1/stateful_uniform_full_int/Bitcast:output:0Bsequential_12/random_flip_1/stateful_uniform_full_int/alg:output:0*
_output_shapes
:*
dtype0	27
5sequential_12/random_flip_1/stateful_uniform_full_int�
&sequential_12/random_flip_1/zeros_likeConst*
_output_shapes
:*
dtype0	*
valueB	R 2(
&sequential_12/random_flip_1/zeros_like�
!sequential_12/random_flip_1/stackPack>sequential_12/random_flip_1/stateful_uniform_full_int:output:0/sequential_12/random_flip_1/zeros_like:output:0*
N*
T0	*
_output_shapes

:2#
!sequential_12/random_flip_1/stack�
/sequential_12/random_flip_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        21
/sequential_12/random_flip_1/strided_slice/stack�
1sequential_12/random_flip_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       23
1sequential_12/random_flip_1/strided_slice/stack_1�
1sequential_12/random_flip_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      23
1sequential_12/random_flip_1/strided_slice/stack_2�
)sequential_12/random_flip_1/strided_sliceStridedSlice*sequential_12/random_flip_1/stack:output:08sequential_12/random_flip_1/strided_slice/stack:output:0:sequential_12/random_flip_1/strided_slice/stack_1:output:0:sequential_12/random_flip_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2+
)sequential_12/random_flip_1/strided_slice�
Osequential_12/random_flip_1/stateless_random_flip_left_right/control_dependencyIdentity!sequential_11/rescaling_1/add:z:0*
T0*0
_class&
$"loc:@sequential_11/rescaling_1/add*1
_output_shapes
:�����������2Q
Osequential_12/random_flip_1/stateless_random_flip_left_right/control_dependency�
Bsequential_12/random_flip_1/stateless_random_flip_left_right/ShapeShapeXsequential_12/random_flip_1/stateless_random_flip_left_right/control_dependency:output:0*
T0*
_output_shapes
:2D
Bsequential_12/random_flip_1/stateless_random_flip_left_right/Shape�
Psequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2R
Psequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack�
Rsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2T
Rsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_1�
Rsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2T
Rsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_2�
Jsequential_12/random_flip_1/stateless_random_flip_left_right/strided_sliceStridedSliceKsequential_12/random_flip_1/stateless_random_flip_left_right/Shape:output:0Ysequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack:output:0[sequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_1:output:0[sequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2L
Jsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice�
[sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shapePackSsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice:output:0*
N*
T0*
_output_shapes
:2]
[sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shape�
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2[
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min�
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2[
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/max�
rsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounter2sequential_12/random_flip_1/strided_slice:output:0* 
_output_shapes
::2t
rsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�
ksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgs^sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2m
ksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�
nsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2dsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shape:output:0xsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0|sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0qsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2p
nsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2�
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/subSubbsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/max:output:0bsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2[
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/sub�
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mulMulwsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2:output:0]sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2[
Ysequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mul�
Usequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniformAddV2]sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mul:z:0bsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2W
Usequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform�
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2N
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/1�
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2N
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/2�
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2N
Lsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/3�
Jsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shapePackSsequential_12/random_flip_1/stateless_random_flip_left_right/strided_slice:output:0Usequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/1:output:0Usequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/2:output:0Usequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2L
Jsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape�
Dsequential_12/random_flip_1/stateless_random_flip_left_right/ReshapeReshapeYsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform:z:0Ssequential_12/random_flip_1/stateless_random_flip_left_right/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2F
Dsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape�
Bsequential_12/random_flip_1/stateless_random_flip_left_right/RoundRoundMsequential_12/random_flip_1/stateless_random_flip_left_right/Reshape:output:0*
T0*/
_output_shapes
:���������2D
Bsequential_12/random_flip_1/stateless_random_flip_left_right/Round�
Ksequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2M
Ksequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2/axis�
Fsequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2	ReverseV2Xsequential_12/random_flip_1/stateless_random_flip_left_right/control_dependency:output:0Tsequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2H
Fsequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2�
@sequential_12/random_flip_1/stateless_random_flip_left_right/mulMulFsequential_12/random_flip_1/stateless_random_flip_left_right/Round:y:0Osequential_12/random_flip_1/stateless_random_flip_left_right/ReverseV2:output:0*
T0*1
_output_shapes
:�����������2B
@sequential_12/random_flip_1/stateless_random_flip_left_right/mul�
Bsequential_12/random_flip_1/stateless_random_flip_left_right/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2D
Bsequential_12/random_flip_1/stateless_random_flip_left_right/sub/x�
@sequential_12/random_flip_1/stateless_random_flip_left_right/subSubKsequential_12/random_flip_1/stateless_random_flip_left_right/sub/x:output:0Fsequential_12/random_flip_1/stateless_random_flip_left_right/Round:y:0*
T0*/
_output_shapes
:���������2B
@sequential_12/random_flip_1/stateless_random_flip_left_right/sub�
Bsequential_12/random_flip_1/stateless_random_flip_left_right/mul_1MulDsequential_12/random_flip_1/stateless_random_flip_left_right/sub:z:0Xsequential_12/random_flip_1/stateless_random_flip_left_right/control_dependency:output:0*
T0*1
_output_shapes
:�����������2D
Bsequential_12/random_flip_1/stateless_random_flip_left_right/mul_1�
@sequential_12/random_flip_1/stateless_random_flip_left_right/addAddV2Dsequential_12/random_flip_1/stateless_random_flip_left_right/mul:z:0Fsequential_12/random_flip_1/stateless_random_flip_left_right/mul_1:z:0*
T0*1
_output_shapes
:�����������2B
@sequential_12/random_flip_1/stateless_random_flip_left_right/add�
=sequential_12/random_flip_1/stateful_uniform_full_int_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:2?
=sequential_12/random_flip_1/stateful_uniform_full_int_1/shape�
=sequential_12/random_flip_1/stateful_uniform_full_int_1/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2?
=sequential_12/random_flip_1/stateful_uniform_full_int_1/Const�
<sequential_12/random_flip_1/stateful_uniform_full_int_1/ProdProdFsequential_12/random_flip_1/stateful_uniform_full_int_1/shape:output:0Fsequential_12/random_flip_1/stateful_uniform_full_int_1/Const:output:0*
T0*
_output_shapes
: 2>
<sequential_12/random_flip_1/stateful_uniform_full_int_1/Prod�
>sequential_12/random_flip_1/stateful_uniform_full_int_1/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2@
>sequential_12/random_flip_1/stateful_uniform_full_int_1/Cast/x�
>sequential_12/random_flip_1/stateful_uniform_full_int_1/Cast_1CastEsequential_12/random_flip_1/stateful_uniform_full_int_1/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2@
>sequential_12/random_flip_1/stateful_uniform_full_int_1/Cast_1�
Fsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkipRngReadAndSkipMsequential_12_random_flip_1_stateful_uniform_full_int_rngreadandskip_resourceGsequential_12/random_flip_1/stateful_uniform_full_int_1/Cast/x:output:0Bsequential_12/random_flip_1/stateful_uniform_full_int_1/Cast_1:y:0E^sequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip*
_output_shapes
:2H
Fsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip�
Ksequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2M
Ksequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack�
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1�
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2�
Esequential_12/random_flip_1/stateful_uniform_full_int_1/strided_sliceStridedSliceNsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip:value:0Tsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack:output:0Vsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1:output:0Vsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2G
Esequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice�
?sequential_12/random_flip_1/stateful_uniform_full_int_1/BitcastBitcastNsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice:output:0*
T0	*
_output_shapes
:*

type02A
?sequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast�
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2O
Msequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack�
Osequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2Q
Osequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1�
Osequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2Q
Osequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2�
Gsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1StridedSliceNsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip:value:0Vsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack:output:0Xsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1:output:0Xsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2I
Gsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1�
Asequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast_1BitcastPsequential_12/random_flip_1/stateful_uniform_full_int_1/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02C
Asequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast_1�
;sequential_12/random_flip_1/stateful_uniform_full_int_1/algConst*
_output_shapes
: *
dtype0*
value	B :2=
;sequential_12/random_flip_1/stateful_uniform_full_int_1/alg�
7sequential_12/random_flip_1/stateful_uniform_full_int_1StatelessRandomUniformFullIntV2Fsequential_12/random_flip_1/stateful_uniform_full_int_1/shape:output:0Jsequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast_1:output:0Hsequential_12/random_flip_1/stateful_uniform_full_int_1/Bitcast:output:0Dsequential_12/random_flip_1/stateful_uniform_full_int_1/alg:output:0*
_output_shapes
:*
dtype0	29
7sequential_12/random_flip_1/stateful_uniform_full_int_1�
(sequential_12/random_flip_1/zeros_like_1Const*
_output_shapes
:*
dtype0	*
valueB	R 2*
(sequential_12/random_flip_1/zeros_like_1�
#sequential_12/random_flip_1/stack_1Pack@sequential_12/random_flip_1/stateful_uniform_full_int_1:output:01sequential_12/random_flip_1/zeros_like_1:output:0*
N*
T0	*
_output_shapes

:2%
#sequential_12/random_flip_1/stack_1�
1sequential_12/random_flip_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        23
1sequential_12/random_flip_1/strided_slice_1/stack�
3sequential_12/random_flip_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       25
3sequential_12/random_flip_1/strided_slice_1/stack_1�
3sequential_12/random_flip_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      25
3sequential_12/random_flip_1/strided_slice_1/stack_2�
+sequential_12/random_flip_1/strided_slice_1StridedSlice,sequential_12/random_flip_1/stack_1:output:0:sequential_12/random_flip_1/strided_slice_1/stack:output:0<sequential_12/random_flip_1/strided_slice_1/stack_1:output:0<sequential_12/random_flip_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2-
+sequential_12/random_flip_1/strided_slice_1�
Lsequential_12/random_flip_1/stateless_random_flip_up_down/control_dependencyIdentityDsequential_12/random_flip_1/stateless_random_flip_left_right/add:z:0*
T0*S
_classI
GEloc:@sequential_12/random_flip_1/stateless_random_flip_left_right/add*1
_output_shapes
:�����������2N
Lsequential_12/random_flip_1/stateless_random_flip_up_down/control_dependency�
?sequential_12/random_flip_1/stateless_random_flip_up_down/ShapeShapeUsequential_12/random_flip_1/stateless_random_flip_up_down/control_dependency:output:0*
T0*
_output_shapes
:2A
?sequential_12/random_flip_1/stateless_random_flip_up_down/Shape�
Msequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2O
Msequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack�
Osequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2Q
Osequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_1�
Osequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2Q
Osequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_2�
Gsequential_12/random_flip_1/stateless_random_flip_up_down/strided_sliceStridedSliceHsequential_12/random_flip_1/stateless_random_flip_up_down/Shape:output:0Vsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack:output:0Xsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_1:output:0Xsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2I
Gsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice�
Xsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shapePackPsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice:output:0*
N*
T0*
_output_shapes
:2Z
Xsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shape�
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2X
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min�
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2X
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/max�
osequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounter4sequential_12/random_flip_1/strided_slice_1:output:0l^sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg* 
_output_shapes
::2q
osequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�
hsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgp^sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2j
hsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�
ksequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2asequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shape:output:0usequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0ysequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0nsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2m
ksequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2�
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/subSub_sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/max:output:0_sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2X
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/sub�
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mulMultsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2:output:0Zsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2X
Vsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mul�
Rsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniformAddV2Zsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mul:z:0_sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2T
Rsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform�
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2K
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/1�
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2K
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/2�
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2K
Isequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/3�
Gsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shapePackPsequential_12/random_flip_1/stateless_random_flip_up_down/strided_slice:output:0Rsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/1:output:0Rsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/2:output:0Rsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2I
Gsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape�
Asequential_12/random_flip_1/stateless_random_flip_up_down/ReshapeReshapeVsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform:z:0Psequential_12/random_flip_1/stateless_random_flip_up_down/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2C
Asequential_12/random_flip_1/stateless_random_flip_up_down/Reshape�
?sequential_12/random_flip_1/stateless_random_flip_up_down/RoundRoundJsequential_12/random_flip_1/stateless_random_flip_up_down/Reshape:output:0*
T0*/
_output_shapes
:���������2A
?sequential_12/random_flip_1/stateless_random_flip_up_down/Round�
Hsequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2J
Hsequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2/axis�
Csequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2	ReverseV2Usequential_12/random_flip_1/stateless_random_flip_up_down/control_dependency:output:0Qsequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2E
Csequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2�
=sequential_12/random_flip_1/stateless_random_flip_up_down/mulMulCsequential_12/random_flip_1/stateless_random_flip_up_down/Round:y:0Lsequential_12/random_flip_1/stateless_random_flip_up_down/ReverseV2:output:0*
T0*1
_output_shapes
:�����������2?
=sequential_12/random_flip_1/stateless_random_flip_up_down/mul�
?sequential_12/random_flip_1/stateless_random_flip_up_down/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2A
?sequential_12/random_flip_1/stateless_random_flip_up_down/sub/x�
=sequential_12/random_flip_1/stateless_random_flip_up_down/subSubHsequential_12/random_flip_1/stateless_random_flip_up_down/sub/x:output:0Csequential_12/random_flip_1/stateless_random_flip_up_down/Round:y:0*
T0*/
_output_shapes
:���������2?
=sequential_12/random_flip_1/stateless_random_flip_up_down/sub�
?sequential_12/random_flip_1/stateless_random_flip_up_down/mul_1MulAsequential_12/random_flip_1/stateless_random_flip_up_down/sub:z:0Usequential_12/random_flip_1/stateless_random_flip_up_down/control_dependency:output:0*
T0*1
_output_shapes
:�����������2A
?sequential_12/random_flip_1/stateless_random_flip_up_down/mul_1�
=sequential_12/random_flip_1/stateless_random_flip_up_down/addAddV2Asequential_12/random_flip_1/stateless_random_flip_up_down/mul:z:0Csequential_12/random_flip_1/stateless_random_flip_up_down/mul_1:z:0*
T0*1
_output_shapes
:�����������2?
=sequential_12/random_flip_1/stateless_random_flip_up_down/add�
%sequential_12/random_rotation_1/ShapeShapeAsequential_12/random_flip_1/stateless_random_flip_up_down/add:z:0*
T0*
_output_shapes
:2'
%sequential_12/random_rotation_1/Shape�
3sequential_12/random_rotation_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 25
3sequential_12/random_rotation_1/strided_slice/stack�
5sequential_12/random_rotation_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:27
5sequential_12/random_rotation_1/strided_slice/stack_1�
5sequential_12/random_rotation_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:27
5sequential_12/random_rotation_1/strided_slice/stack_2�
-sequential_12/random_rotation_1/strided_sliceStridedSlice.sequential_12/random_rotation_1/Shape:output:0<sequential_12/random_rotation_1/strided_slice/stack:output:0>sequential_12/random_rotation_1/strided_slice/stack_1:output:0>sequential_12/random_rotation_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2/
-sequential_12/random_rotation_1/strided_slice�
5sequential_12/random_rotation_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������27
5sequential_12/random_rotation_1/strided_slice_1/stack�
7sequential_12/random_rotation_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������29
7sequential_12/random_rotation_1/strided_slice_1/stack_1�
7sequential_12/random_rotation_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:29
7sequential_12/random_rotation_1/strided_slice_1/stack_2�
/sequential_12/random_rotation_1/strided_slice_1StridedSlice.sequential_12/random_rotation_1/Shape:output:0>sequential_12/random_rotation_1/strided_slice_1/stack:output:0@sequential_12/random_rotation_1/strided_slice_1/stack_1:output:0@sequential_12/random_rotation_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask21
/sequential_12/random_rotation_1/strided_slice_1�
$sequential_12/random_rotation_1/CastCast8sequential_12/random_rotation_1/strided_slice_1:output:0*

DstT0*

SrcT0*
_output_shapes
: 2&
$sequential_12/random_rotation_1/Cast�
5sequential_12/random_rotation_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������27
5sequential_12/random_rotation_1/strided_slice_2/stack�
7sequential_12/random_rotation_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������29
7sequential_12/random_rotation_1/strided_slice_2/stack_1�
7sequential_12/random_rotation_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:29
7sequential_12/random_rotation_1/strided_slice_2/stack_2�
/sequential_12/random_rotation_1/strided_slice_2StridedSlice.sequential_12/random_rotation_1/Shape:output:0>sequential_12/random_rotation_1/strided_slice_2/stack:output:0@sequential_12/random_rotation_1/strided_slice_2/stack_1:output:0@sequential_12/random_rotation_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask21
/sequential_12/random_rotation_1/strided_slice_2�
&sequential_12/random_rotation_1/Cast_1Cast8sequential_12/random_rotation_1/strided_slice_2:output:0*

DstT0*

SrcT0*
_output_shapes
: 2(
&sequential_12/random_rotation_1/Cast_1�
6sequential_12/random_rotation_1/stateful_uniform/shapePack6sequential_12/random_rotation_1/strided_slice:output:0*
N*
T0*
_output_shapes
:28
6sequential_12/random_rotation_1/stateful_uniform/shape�
4sequential_12/random_rotation_1/stateful_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *|٠�26
4sequential_12/random_rotation_1/stateful_uniform/min�
4sequential_12/random_rotation_1/stateful_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *|٠?26
4sequential_12/random_rotation_1/stateful_uniform/max�
6sequential_12/random_rotation_1/stateful_uniform/ConstConst*
_output_shapes
:*
dtype0*
valueB: 28
6sequential_12/random_rotation_1/stateful_uniform/Const�
5sequential_12/random_rotation_1/stateful_uniform/ProdProd?sequential_12/random_rotation_1/stateful_uniform/shape:output:0?sequential_12/random_rotation_1/stateful_uniform/Const:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/stateful_uniform/Prod�
7sequential_12/random_rotation_1/stateful_uniform/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :29
7sequential_12/random_rotation_1/stateful_uniform/Cast/x�
7sequential_12/random_rotation_1/stateful_uniform/Cast_1Cast>sequential_12/random_rotation_1/stateful_uniform/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 29
7sequential_12/random_rotation_1/stateful_uniform/Cast_1�
?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkipRngReadAndSkipHsequential_12_random_rotation_1_stateful_uniform_rngreadandskip_resource@sequential_12/random_rotation_1/stateful_uniform/Cast/x:output:0;sequential_12/random_rotation_1/stateful_uniform/Cast_1:y:0*
_output_shapes
:2A
?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip�
Dsequential_12/random_rotation_1/stateful_uniform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2F
Dsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack�
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2H
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_1�
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2H
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_2�
>sequential_12/random_rotation_1/stateful_uniform/strided_sliceStridedSliceGsequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip:value:0Msequential_12/random_rotation_1/stateful_uniform/strided_slice/stack:output:0Osequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_1:output:0Osequential_12/random_rotation_1/stateful_uniform/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2@
>sequential_12/random_rotation_1/stateful_uniform/strided_slice�
8sequential_12/random_rotation_1/stateful_uniform/BitcastBitcastGsequential_12/random_rotation_1/stateful_uniform/strided_slice:output:0*
T0	*
_output_shapes
:*

type02:
8sequential_12/random_rotation_1/stateful_uniform/Bitcast�
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2H
Fsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack�
Hsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2J
Hsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_1�
Hsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2J
Hsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_2�
@sequential_12/random_rotation_1/stateful_uniform/strided_slice_1StridedSliceGsequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip:value:0Osequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack:output:0Qsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_1:output:0Qsequential_12/random_rotation_1/stateful_uniform/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2B
@sequential_12/random_rotation_1/stateful_uniform/strided_slice_1�
:sequential_12/random_rotation_1/stateful_uniform/Bitcast_1BitcastIsequential_12/random_rotation_1/stateful_uniform/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02<
:sequential_12/random_rotation_1/stateful_uniform/Bitcast_1�
Msequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2/algConst*
_output_shapes
: *
dtype0*
value	B :2O
Msequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2/alg�
Isequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2StatelessRandomUniformV2?sequential_12/random_rotation_1/stateful_uniform/shape:output:0Csequential_12/random_rotation_1/stateful_uniform/Bitcast_1:output:0Asequential_12/random_rotation_1/stateful_uniform/Bitcast:output:0Vsequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2/alg:output:0*#
_output_shapes
:���������2K
Isequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2�
4sequential_12/random_rotation_1/stateful_uniform/subSub=sequential_12/random_rotation_1/stateful_uniform/max:output:0=sequential_12/random_rotation_1/stateful_uniform/min:output:0*
T0*
_output_shapes
: 26
4sequential_12/random_rotation_1/stateful_uniform/sub�
4sequential_12/random_rotation_1/stateful_uniform/mulMulRsequential_12/random_rotation_1/stateful_uniform/StatelessRandomUniformV2:output:08sequential_12/random_rotation_1/stateful_uniform/sub:z:0*
T0*#
_output_shapes
:���������26
4sequential_12/random_rotation_1/stateful_uniform/mul�
0sequential_12/random_rotation_1/stateful_uniformAddV28sequential_12/random_rotation_1/stateful_uniform/mul:z:0=sequential_12/random_rotation_1/stateful_uniform/min:output:0*
T0*#
_output_shapes
:���������22
0sequential_12/random_rotation_1/stateful_uniform�
5sequential_12/random_rotation_1/rotation_matrix/sub/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?27
5sequential_12/random_rotation_1/rotation_matrix/sub/y�
3sequential_12/random_rotation_1/rotation_matrix/subSub*sequential_12/random_rotation_1/Cast_1:y:0>sequential_12/random_rotation_1/rotation_matrix/sub/y:output:0*
T0*
_output_shapes
: 25
3sequential_12/random_rotation_1/rotation_matrix/sub�
3sequential_12/random_rotation_1/rotation_matrix/CosCos4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/Cos�
7sequential_12/random_rotation_1/rotation_matrix/sub_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_1/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_1Sub*sequential_12/random_rotation_1/Cast_1:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_1/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_1�
3sequential_12/random_rotation_1/rotation_matrix/mulMul7sequential_12/random_rotation_1/rotation_matrix/Cos:y:09sequential_12/random_rotation_1/rotation_matrix/sub_1:z:0*
T0*#
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/mul�
3sequential_12/random_rotation_1/rotation_matrix/SinSin4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/Sin�
7sequential_12/random_rotation_1/rotation_matrix/sub_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_2/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_2Sub(sequential_12/random_rotation_1/Cast:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_2/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_2�
5sequential_12/random_rotation_1/rotation_matrix/mul_1Mul7sequential_12/random_rotation_1/rotation_matrix/Sin:y:09sequential_12/random_rotation_1/rotation_matrix/sub_2:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/mul_1�
5sequential_12/random_rotation_1/rotation_matrix/sub_3Sub7sequential_12/random_rotation_1/rotation_matrix/mul:z:09sequential_12/random_rotation_1/rotation_matrix/mul_1:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/sub_3�
5sequential_12/random_rotation_1/rotation_matrix/sub_4Sub7sequential_12/random_rotation_1/rotation_matrix/sub:z:09sequential_12/random_rotation_1/rotation_matrix/sub_3:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/sub_4�
9sequential_12/random_rotation_1/rotation_matrix/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2;
9sequential_12/random_rotation_1/rotation_matrix/truediv/y�
7sequential_12/random_rotation_1/rotation_matrix/truedivRealDiv9sequential_12/random_rotation_1/rotation_matrix/sub_4:z:0Bsequential_12/random_rotation_1/rotation_matrix/truediv/y:output:0*
T0*#
_output_shapes
:���������29
7sequential_12/random_rotation_1/rotation_matrix/truediv�
7sequential_12/random_rotation_1/rotation_matrix/sub_5/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_5/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_5Sub(sequential_12/random_rotation_1/Cast:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_5/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_5�
5sequential_12/random_rotation_1/rotation_matrix/Sin_1Sin4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Sin_1�
7sequential_12/random_rotation_1/rotation_matrix/sub_6/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_6/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_6Sub*sequential_12/random_rotation_1/Cast_1:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_6/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_6�
5sequential_12/random_rotation_1/rotation_matrix/mul_2Mul9sequential_12/random_rotation_1/rotation_matrix/Sin_1:y:09sequential_12/random_rotation_1/rotation_matrix/sub_6:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/mul_2�
5sequential_12/random_rotation_1/rotation_matrix/Cos_1Cos4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Cos_1�
7sequential_12/random_rotation_1/rotation_matrix/sub_7/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7sequential_12/random_rotation_1/rotation_matrix/sub_7/y�
5sequential_12/random_rotation_1/rotation_matrix/sub_7Sub(sequential_12/random_rotation_1/Cast:y:0@sequential_12/random_rotation_1/rotation_matrix/sub_7/y:output:0*
T0*
_output_shapes
: 27
5sequential_12/random_rotation_1/rotation_matrix/sub_7�
5sequential_12/random_rotation_1/rotation_matrix/mul_3Mul9sequential_12/random_rotation_1/rotation_matrix/Cos_1:y:09sequential_12/random_rotation_1/rotation_matrix/sub_7:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/mul_3�
3sequential_12/random_rotation_1/rotation_matrix/addAddV29sequential_12/random_rotation_1/rotation_matrix/mul_2:z:09sequential_12/random_rotation_1/rotation_matrix/mul_3:z:0*
T0*#
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/add�
5sequential_12/random_rotation_1/rotation_matrix/sub_8Sub9sequential_12/random_rotation_1/rotation_matrix/sub_5:z:07sequential_12/random_rotation_1/rotation_matrix/add:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/sub_8�
;sequential_12/random_rotation_1/rotation_matrix/truediv_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2=
;sequential_12/random_rotation_1/rotation_matrix/truediv_1/y�
9sequential_12/random_rotation_1/rotation_matrix/truediv_1RealDiv9sequential_12/random_rotation_1/rotation_matrix/sub_8:z:0Dsequential_12/random_rotation_1/rotation_matrix/truediv_1/y:output:0*
T0*#
_output_shapes
:���������2;
9sequential_12/random_rotation_1/rotation_matrix/truediv_1�
5sequential_12/random_rotation_1/rotation_matrix/ShapeShape4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*
_output_shapes
:27
5sequential_12/random_rotation_1/rotation_matrix/Shape�
Csequential_12/random_rotation_1/rotation_matrix/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2E
Csequential_12/random_rotation_1/rotation_matrix/strided_slice/stack�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_1�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_2�
=sequential_12/random_rotation_1/rotation_matrix/strided_sliceStridedSlice>sequential_12/random_rotation_1/rotation_matrix/Shape:output:0Lsequential_12/random_rotation_1/rotation_matrix/strided_slice/stack:output:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_1:output:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2?
=sequential_12/random_rotation_1/rotation_matrix/strided_slice�
5sequential_12/random_rotation_1/rotation_matrix/Cos_2Cos4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Cos_2�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_1StridedSlice9sequential_12/random_rotation_1/rotation_matrix/Cos_2:y:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_1/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_1�
5sequential_12/random_rotation_1/rotation_matrix/Sin_2Sin4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Sin_2�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_2StridedSlice9sequential_12/random_rotation_1/rotation_matrix/Sin_2:y:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_2�
3sequential_12/random_rotation_1/rotation_matrix/NegNegHsequential_12/random_rotation_1/rotation_matrix/strided_slice_2:output:0*
T0*'
_output_shapes
:���������25
3sequential_12/random_rotation_1/rotation_matrix/Neg�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_3StridedSlice;sequential_12/random_rotation_1/rotation_matrix/truediv:z:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_3�
5sequential_12/random_rotation_1/rotation_matrix/Sin_3Sin4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Sin_3�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_4StridedSlice9sequential_12/random_rotation_1/rotation_matrix/Sin_3:y:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_4/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_4�
5sequential_12/random_rotation_1/rotation_matrix/Cos_3Cos4sequential_12/random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/Cos_3�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_5StridedSlice9sequential_12/random_rotation_1/rotation_matrix/Cos_3:y:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_5/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_5�
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB"        2G
Esequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_1�
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2I
Gsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_2�
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_6StridedSlice=sequential_12/random_rotation_1/rotation_matrix/truediv_1:z:0Nsequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_1:output:0Psequential_12/random_rotation_1/rotation_matrix/strided_slice_6/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2A
?sequential_12/random_rotation_1/rotation_matrix/strided_slice_6�
;sequential_12/random_rotation_1/rotation_matrix/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2=
;sequential_12/random_rotation_1/rotation_matrix/zeros/mul/y�
9sequential_12/random_rotation_1/rotation_matrix/zeros/mulMulFsequential_12/random_rotation_1/rotation_matrix/strided_slice:output:0Dsequential_12/random_rotation_1/rotation_matrix/zeros/mul/y:output:0*
T0*
_output_shapes
: 2;
9sequential_12/random_rotation_1/rotation_matrix/zeros/mul�
<sequential_12/random_rotation_1/rotation_matrix/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :�2>
<sequential_12/random_rotation_1/rotation_matrix/zeros/Less/y�
:sequential_12/random_rotation_1/rotation_matrix/zeros/LessLess=sequential_12/random_rotation_1/rotation_matrix/zeros/mul:z:0Esequential_12/random_rotation_1/rotation_matrix/zeros/Less/y:output:0*
T0*
_output_shapes
: 2<
:sequential_12/random_rotation_1/rotation_matrix/zeros/Less�
>sequential_12/random_rotation_1/rotation_matrix/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :2@
>sequential_12/random_rotation_1/rotation_matrix/zeros/packed/1�
<sequential_12/random_rotation_1/rotation_matrix/zeros/packedPackFsequential_12/random_rotation_1/rotation_matrix/strided_slice:output:0Gsequential_12/random_rotation_1/rotation_matrix/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2>
<sequential_12/random_rotation_1/rotation_matrix/zeros/packed�
;sequential_12/random_rotation_1/rotation_matrix/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2=
;sequential_12/random_rotation_1/rotation_matrix/zeros/Const�
5sequential_12/random_rotation_1/rotation_matrix/zerosFillEsequential_12/random_rotation_1/rotation_matrix/zeros/packed:output:0Dsequential_12/random_rotation_1/rotation_matrix/zeros/Const:output:0*
T0*'
_output_shapes
:���������27
5sequential_12/random_rotation_1/rotation_matrix/zeros�
;sequential_12/random_rotation_1/rotation_matrix/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2=
;sequential_12/random_rotation_1/rotation_matrix/concat/axis�
6sequential_12/random_rotation_1/rotation_matrix/concatConcatV2Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_1:output:07sequential_12/random_rotation_1/rotation_matrix/Neg:y:0Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_3:output:0Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_4:output:0Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_5:output:0Hsequential_12/random_rotation_1/rotation_matrix/strided_slice_6:output:0>sequential_12/random_rotation_1/rotation_matrix/zeros:output:0Dsequential_12/random_rotation_1/rotation_matrix/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������28
6sequential_12/random_rotation_1/rotation_matrix/concat�
/sequential_12/random_rotation_1/transform/ShapeShapeAsequential_12/random_flip_1/stateless_random_flip_up_down/add:z:0*
T0*
_output_shapes
:21
/sequential_12/random_rotation_1/transform/Shape�
=sequential_12/random_rotation_1/transform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2?
=sequential_12/random_rotation_1/transform/strided_slice/stack�
?sequential_12/random_rotation_1/transform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2A
?sequential_12/random_rotation_1/transform/strided_slice/stack_1�
?sequential_12/random_rotation_1/transform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2A
?sequential_12/random_rotation_1/transform/strided_slice/stack_2�
7sequential_12/random_rotation_1/transform/strided_sliceStridedSlice8sequential_12/random_rotation_1/transform/Shape:output:0Fsequential_12/random_rotation_1/transform/strided_slice/stack:output:0Hsequential_12/random_rotation_1/transform/strided_slice/stack_1:output:0Hsequential_12/random_rotation_1/transform/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:29
7sequential_12/random_rotation_1/transform/strided_slice�
4sequential_12/random_rotation_1/transform/fill_valueConst*
_output_shapes
: *
dtype0*
valueB
 *    26
4sequential_12/random_rotation_1/transform/fill_value�
Dsequential_12/random_rotation_1/transform/ImageProjectiveTransformV3ImageProjectiveTransformV3Asequential_12/random_flip_1/stateless_random_flip_up_down/add:z:0?sequential_12/random_rotation_1/rotation_matrix/concat:output:0@sequential_12/random_rotation_1/transform/strided_slice:output:0=sequential_12/random_rotation_1/transform/fill_value:output:0*1
_output_shapes
:�����������*
dtype0*
	fill_mode	REFLECT*
interpolation
BILINEAR2F
Dsequential_12/random_rotation_1/transform/ImageProjectiveTransformV3�
conv2d_72/Conv2D/ReadVariableOpReadVariableOp(conv2d_72_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype02!
conv2d_72/Conv2D/ReadVariableOp�
conv2d_72/Conv2DConv2DYsequential_12/random_rotation_1/transform/ImageProjectiveTransformV3:transformed_images:0'conv2d_72/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� *
paddingVALID*
strides
2
conv2d_72/Conv2D�
 conv2d_72/BiasAdd/ReadVariableOpReadVariableOp)conv2d_72_biasadd_readvariableop_resource*
_output_shapes
: *
dtype02"
 conv2d_72/BiasAdd/ReadVariableOp�
conv2d_72/BiasAddBiasAddconv2d_72/Conv2D:output:0(conv2d_72/BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� 2
conv2d_72/BiasAdd�
conv2d_72/ReluReluconv2d_72/BiasAdd:output:0*
T0*1
_output_shapes
:����������� 2
conv2d_72/Relu�
max_pooling2d_72/MaxPoolMaxPoolconv2d_72/Relu:activations:0*/
_output_shapes
:���������~~ *
ksize
*
paddingVALID*
strides
2
max_pooling2d_72/MaxPool�
conv2d_73/Conv2D/ReadVariableOpReadVariableOp(conv2d_73_conv2d_readvariableop_resource*&
_output_shapes
: @*
dtype02!
conv2d_73/Conv2D/ReadVariableOp�
conv2d_73/Conv2DConv2D!max_pooling2d_72/MaxPool:output:0'conv2d_73/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@*
paddingVALID*
strides
2
conv2d_73/Conv2D�
 conv2d_73/BiasAdd/ReadVariableOpReadVariableOp)conv2d_73_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02"
 conv2d_73/BiasAdd/ReadVariableOp�
conv2d_73/BiasAddBiasAddconv2d_73/Conv2D:output:0(conv2d_73/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@2
conv2d_73/BiasAdd~
conv2d_73/ReluReluconv2d_73/BiasAdd:output:0*
T0*/
_output_shapes
:���������||@2
conv2d_73/Relu�
max_pooling2d_73/MaxPoolMaxPoolconv2d_73/Relu:activations:0*/
_output_shapes
:���������>>@*
ksize
*
paddingVALID*
strides
2
max_pooling2d_73/MaxPool�
conv2d_74/Conv2D/ReadVariableOpReadVariableOp(conv2d_74_conv2d_readvariableop_resource*&
_output_shapes
:@@*
dtype02!
conv2d_74/Conv2D/ReadVariableOp�
conv2d_74/Conv2DConv2D!max_pooling2d_73/MaxPool:output:0'conv2d_74/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@*
paddingVALID*
strides
2
conv2d_74/Conv2D�
 conv2d_74/BiasAdd/ReadVariableOpReadVariableOp)conv2d_74_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02"
 conv2d_74/BiasAdd/ReadVariableOp�
conv2d_74/BiasAddBiasAddconv2d_74/Conv2D:output:0(conv2d_74/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@2
conv2d_74/BiasAdd~
conv2d_74/ReluReluconv2d_74/BiasAdd:output:0*
T0*/
_output_shapes
:���������<<@2
conv2d_74/Relu�
max_pooling2d_74/MaxPoolMaxPoolconv2d_74/Relu:activations:0*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
2
max_pooling2d_74/MaxPoolu
flatten_19/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� �  2
flatten_19/Const�
flatten_19/ReshapeReshape!max_pooling2d_74/MaxPool:output:0flatten_19/Const:output:0*
T0*)
_output_shapes
:�����������2
flatten_19/Reshape�
dense_38/MatMul/ReadVariableOpReadVariableOp'dense_38_matmul_readvariableop_resource* 
_output_shapes
:
��@*
dtype02 
dense_38/MatMul/ReadVariableOp�
dense_38/MatMulMatMulflatten_19/Reshape:output:0&dense_38/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_38/MatMul�
dense_38/BiasAdd/ReadVariableOpReadVariableOp(dense_38_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02!
dense_38/BiasAdd/ReadVariableOp�
dense_38/BiasAddBiasAdddense_38/MatMul:product:0'dense_38/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_38/BiasAdds
dense_38/ReluReludense_38/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
dense_38/Relu�
dense_39/MatMul/ReadVariableOpReadVariableOp'dense_39_matmul_readvariableop_resource*
_output_shapes

:@
*
dtype02 
dense_39/MatMul/ReadVariableOp�
dense_39/MatMulMatMuldense_38/Relu:activations:0&dense_39/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_39/MatMul�
dense_39/BiasAdd/ReadVariableOpReadVariableOp(dense_39_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_39/BiasAdd/ReadVariableOp�
dense_39/BiasAddBiasAdddense_39/MatMul:product:0'dense_39/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_39/BiasAdd|
dense_39/SoftmaxSoftmaxdense_39/BiasAdd:output:0*
T0*'
_output_shapes
:���������
2
dense_39/Softmaxu
IdentityIdentitydense_39/Softmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity�
NoOpNoOp!^conv2d_72/BiasAdd/ReadVariableOp ^conv2d_72/Conv2D/ReadVariableOp!^conv2d_73/BiasAdd/ReadVariableOp ^conv2d_73/Conv2D/ReadVariableOp!^conv2d_74/BiasAdd/ReadVariableOp ^conv2d_74/Conv2D/ReadVariableOp ^dense_38/BiasAdd/ReadVariableOp^dense_38/MatMul/ReadVariableOp ^dense_39/BiasAdd/ReadVariableOp^dense_39/MatMul/ReadVariableOpE^sequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkipG^sequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkipl^sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgs^sequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounteri^sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgp^sequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter@^sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:�����������: : : : : : : : : : : : 2D
 conv2d_72/BiasAdd/ReadVariableOp conv2d_72/BiasAdd/ReadVariableOp2B
conv2d_72/Conv2D/ReadVariableOpconv2d_72/Conv2D/ReadVariableOp2D
 conv2d_73/BiasAdd/ReadVariableOp conv2d_73/BiasAdd/ReadVariableOp2B
conv2d_73/Conv2D/ReadVariableOpconv2d_73/Conv2D/ReadVariableOp2D
 conv2d_74/BiasAdd/ReadVariableOp conv2d_74/BiasAdd/ReadVariableOp2B
conv2d_74/Conv2D/ReadVariableOpconv2d_74/Conv2D/ReadVariableOp2B
dense_38/BiasAdd/ReadVariableOpdense_38/BiasAdd/ReadVariableOp2@
dense_38/MatMul/ReadVariableOpdense_38/MatMul/ReadVariableOp2B
dense_39/BiasAdd/ReadVariableOpdense_39/BiasAdd/ReadVariableOp2@
dense_39/MatMul/ReadVariableOpdense_39/MatMul/ReadVariableOp2�
Dsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkipDsequential_12/random_flip_1/stateful_uniform_full_int/RngReadAndSkip2�
Fsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkipFsequential_12/random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip2�
ksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgksequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg2�
rsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterrsequential_12/random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter2�
hsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlghsequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg2�
osequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterosequential_12/random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter2�
?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip?sequential_12/random_rotation_1/stateful_uniform/RngReadAndSkip:f b
1
_output_shapes
:�����������
-
_user_specified_namesequential_11_input
�
g
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_60623

inputs
identity�
MaxPoolMaxPoolinputs*/
_output_shapes
:���������~~ *
ksize
*
paddingVALID*
strides
2	
MaxPooll
IdentityIdentityMaxPool:output:0*
T0*/
_output_shapes
:���������~~ 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:����������� :Y U
1
_output_shapes
:����������� 
 
_user_specified_nameinputs
�
I
-__inference_random_flip_1_layer_call_fn_62549

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_random_flip_1_layer_call_and_return_conditional_losses_602072
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
I
-__inference_sequential_11_layer_call_fn_61805

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_11_layer_call_and_return_conditional_losses_601482
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�	
d
H__inference_sequential_12_layer_call_and_return_conditional_losses_60216

inputs
identity�
random_flip_1/PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_random_flip_1_layer_call_and_return_conditional_losses_602072
random_flip_1/PartitionedCall�
!random_rotation_1/PartitionedCallPartitionedCall&random_flip_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *U
fPRN
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_602132#
!random_rotation_1/PartitionedCall�
IdentityIdentity*random_rotation_1/PartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_62389

inputs
identity�
MaxPoolMaxPoolinputs*/
_output_shapes
:���������~~ *
ksize
*
paddingVALID*
strides
2	
MaxPooll
IdentityIdentityMaxPool:output:0*
T0*/
_output_shapes
:���������~~ 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:����������� :Y U
1
_output_shapes
:����������� 
 
_user_specified_nameinputs
�
q
H__inference_sequential_12_layer_call_and_return_conditional_losses_62120
random_flip_1_input
identityq
IdentityIdentityrandom_flip_1_input*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:f b
1
_output_shapes
:�����������
-
_user_specified_namerandom_flip_1_input
�
�
D__inference_conv2d_73_layer_call_and_return_conditional_losses_60636

inputs8
conv2d_readvariableop_resource: @-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp�
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: @*
dtype02
Conv2D/ReadVariableOp�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@*
paddingVALID*
strides
2
Conv2D�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@2	
BiasAdd`
ReluReluBiasAdd:output:0*
T0*/
_output_shapes
:���������||@2
Reluu
IdentityIdentityRelu:activations:0^NoOp*
T0*/
_output_shapes
:���������||@2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������~~ : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:���������~~ 
 
_user_specified_nameinputs
�D
�
H__inference_sequential_23_layer_call_and_return_conditional_losses_61195

inputsB
(conv2d_72_conv2d_readvariableop_resource: 7
)conv2d_72_biasadd_readvariableop_resource: B
(conv2d_73_conv2d_readvariableop_resource: @7
)conv2d_73_biasadd_readvariableop_resource:@B
(conv2d_74_conv2d_readvariableop_resource:@@7
)conv2d_74_biasadd_readvariableop_resource:@;
'dense_38_matmul_readvariableop_resource:
��@6
(dense_38_biasadd_readvariableop_resource:@9
'dense_39_matmul_readvariableop_resource:@
6
(dense_39_biasadd_readvariableop_resource:

identity�� conv2d_72/BiasAdd/ReadVariableOp�conv2d_72/Conv2D/ReadVariableOp� conv2d_73/BiasAdd/ReadVariableOp�conv2d_73/Conv2D/ReadVariableOp� conv2d_74/BiasAdd/ReadVariableOp�conv2d_74/Conv2D/ReadVariableOp�dense_38/BiasAdd/ReadVariableOp�dense_38/MatMul/ReadVariableOp�dense_39/BiasAdd/ReadVariableOp�dense_39/MatMul/ReadVariableOp�
$sequential_11/resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2&
$sequential_11/resizing_1/resize/size�
.sequential_11/resizing_1/resize/ResizeBilinearResizeBilinearinputs-sequential_11/resizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(20
.sequential_11/resizing_1/resize/ResizeBilinear�
 sequential_11/rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2"
 sequential_11/rescaling_1/Cast/x�
"sequential_11/rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2$
"sequential_11/rescaling_1/Cast_1/x�
sequential_11/rescaling_1/mulMul?sequential_11/resizing_1/resize/ResizeBilinear:resized_images:0)sequential_11/rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2
sequential_11/rescaling_1/mul�
sequential_11/rescaling_1/addAddV2!sequential_11/rescaling_1/mul:z:0+sequential_11/rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
sequential_11/rescaling_1/add�
conv2d_72/Conv2D/ReadVariableOpReadVariableOp(conv2d_72_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype02!
conv2d_72/Conv2D/ReadVariableOp�
conv2d_72/Conv2DConv2D!sequential_11/rescaling_1/add:z:0'conv2d_72/Conv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� *
paddingVALID*
strides
2
conv2d_72/Conv2D�
 conv2d_72/BiasAdd/ReadVariableOpReadVariableOp)conv2d_72_biasadd_readvariableop_resource*
_output_shapes
: *
dtype02"
 conv2d_72/BiasAdd/ReadVariableOp�
conv2d_72/BiasAddBiasAddconv2d_72/Conv2D:output:0(conv2d_72/BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� 2
conv2d_72/BiasAdd�
conv2d_72/ReluReluconv2d_72/BiasAdd:output:0*
T0*1
_output_shapes
:����������� 2
conv2d_72/Relu�
max_pooling2d_72/MaxPoolMaxPoolconv2d_72/Relu:activations:0*/
_output_shapes
:���������~~ *
ksize
*
paddingVALID*
strides
2
max_pooling2d_72/MaxPool�
conv2d_73/Conv2D/ReadVariableOpReadVariableOp(conv2d_73_conv2d_readvariableop_resource*&
_output_shapes
: @*
dtype02!
conv2d_73/Conv2D/ReadVariableOp�
conv2d_73/Conv2DConv2D!max_pooling2d_72/MaxPool:output:0'conv2d_73/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@*
paddingVALID*
strides
2
conv2d_73/Conv2D�
 conv2d_73/BiasAdd/ReadVariableOpReadVariableOp)conv2d_73_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02"
 conv2d_73/BiasAdd/ReadVariableOp�
conv2d_73/BiasAddBiasAddconv2d_73/Conv2D:output:0(conv2d_73/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@2
conv2d_73/BiasAdd~
conv2d_73/ReluReluconv2d_73/BiasAdd:output:0*
T0*/
_output_shapes
:���������||@2
conv2d_73/Relu�
max_pooling2d_73/MaxPoolMaxPoolconv2d_73/Relu:activations:0*/
_output_shapes
:���������>>@*
ksize
*
paddingVALID*
strides
2
max_pooling2d_73/MaxPool�
conv2d_74/Conv2D/ReadVariableOpReadVariableOp(conv2d_74_conv2d_readvariableop_resource*&
_output_shapes
:@@*
dtype02!
conv2d_74/Conv2D/ReadVariableOp�
conv2d_74/Conv2DConv2D!max_pooling2d_73/MaxPool:output:0'conv2d_74/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@*
paddingVALID*
strides
2
conv2d_74/Conv2D�
 conv2d_74/BiasAdd/ReadVariableOpReadVariableOp)conv2d_74_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02"
 conv2d_74/BiasAdd/ReadVariableOp�
conv2d_74/BiasAddBiasAddconv2d_74/Conv2D:output:0(conv2d_74/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@2
conv2d_74/BiasAdd~
conv2d_74/ReluReluconv2d_74/BiasAdd:output:0*
T0*/
_output_shapes
:���������<<@2
conv2d_74/Relu�
max_pooling2d_74/MaxPoolMaxPoolconv2d_74/Relu:activations:0*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
2
max_pooling2d_74/MaxPoolu
flatten_19/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� �  2
flatten_19/Const�
flatten_19/ReshapeReshape!max_pooling2d_74/MaxPool:output:0flatten_19/Const:output:0*
T0*)
_output_shapes
:�����������2
flatten_19/Reshape�
dense_38/MatMul/ReadVariableOpReadVariableOp'dense_38_matmul_readvariableop_resource* 
_output_shapes
:
��@*
dtype02 
dense_38/MatMul/ReadVariableOp�
dense_38/MatMulMatMulflatten_19/Reshape:output:0&dense_38/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_38/MatMul�
dense_38/BiasAdd/ReadVariableOpReadVariableOp(dense_38_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02!
dense_38/BiasAdd/ReadVariableOp�
dense_38/BiasAddBiasAdddense_38/MatMul:product:0'dense_38/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_38/BiasAdds
dense_38/ReluReludense_38/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
dense_38/Relu�
dense_39/MatMul/ReadVariableOpReadVariableOp'dense_39_matmul_readvariableop_resource*
_output_shapes

:@
*
dtype02 
dense_39/MatMul/ReadVariableOp�
dense_39/MatMulMatMuldense_38/Relu:activations:0&dense_39/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_39/MatMul�
dense_39/BiasAdd/ReadVariableOpReadVariableOp(dense_39_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype02!
dense_39/BiasAdd/ReadVariableOp�
dense_39/BiasAddBiasAdddense_39/MatMul:product:0'dense_39/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense_39/BiasAdd|
dense_39/SoftmaxSoftmaxdense_39/BiasAdd:output:0*
T0*'
_output_shapes
:���������
2
dense_39/Softmaxu
IdentityIdentitydense_39/Softmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity�
NoOpNoOp!^conv2d_72/BiasAdd/ReadVariableOp ^conv2d_72/Conv2D/ReadVariableOp!^conv2d_73/BiasAdd/ReadVariableOp ^conv2d_73/Conv2D/ReadVariableOp!^conv2d_74/BiasAdd/ReadVariableOp ^conv2d_74/Conv2D/ReadVariableOp ^dense_38/BiasAdd/ReadVariableOp^dense_38/MatMul/ReadVariableOp ^dense_39/BiasAdd/ReadVariableOp^dense_39/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:�����������: : : : : : : : : : 2D
 conv2d_72/BiasAdd/ReadVariableOp conv2d_72/BiasAdd/ReadVariableOp2B
conv2d_72/Conv2D/ReadVariableOpconv2d_72/Conv2D/ReadVariableOp2D
 conv2d_73/BiasAdd/ReadVariableOp conv2d_73/BiasAdd/ReadVariableOp2B
conv2d_73/Conv2D/ReadVariableOpconv2d_73/Conv2D/ReadVariableOp2D
 conv2d_74/BiasAdd/ReadVariableOp conv2d_74/BiasAdd/ReadVariableOp2B
conv2d_74/Conv2D/ReadVariableOpconv2d_74/Conv2D/ReadVariableOp2B
dense_38/BiasAdd/ReadVariableOpdense_38/BiasAdd/ReadVariableOp2@
dense_38/MatMul/ReadVariableOpdense_38/MatMul/ReadVariableOp2B
dense_39/BiasAdd/ReadVariableOpdense_39/BiasAdd/ReadVariableOp2@
dense_39/MatMul/ReadVariableOpdense_39/MatMul/ReadVariableOp:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
V
-__inference_sequential_12_layer_call_fn_61860
random_flip_1_input
identity�
PartitionedCallPartitionedCallrandom_flip_1_input*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_12_layer_call_and_return_conditional_losses_602162
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:f b
1
_output_shapes
:�����������
-
_user_specified_namerandom_flip_1_input
�
�
)__inference_conv2d_73_layer_call_fn_62398

inputs!
unknown: @
	unknown_0:@
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������||@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_73_layer_call_and_return_conditional_losses_606362
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������||@2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������~~ : : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:���������~~ 
 
_user_specified_nameinputs
�

�
-__inference_sequential_23_layer_call_fn_61087

inputs!
unknown: 
	unknown_0: #
	unknown_1: @
	unknown_2:@#
	unknown_3:@@
	unknown_4:@
	unknown_5:
��@
	unknown_6:@
	unknown_7:@

	unknown_8:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*,
_read_only_resource_inputs

	
*/
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_23_layer_call_and_return_conditional_losses_607142
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:�����������: : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
a
E__inference_resizing_1_layer_call_and_return_conditional_losses_62531

inputs
identityk
resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2
resize/size�
resize/ResizeBilinearResizeBilinearinputsresize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(2
resize/ResizeBilinear�
IdentityIdentity&resize/ResizeBilinear:resized_images:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
F
*__inference_flatten_19_layer_call_fn_62474

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *)
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *N
fIRG
E__inference_flatten_19_layer_call_and_return_conditional_losses_606772
PartitionedCalln
IdentityIdentityPartitionedCall:output:0*
T0*)
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������@:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
�
d
H__inference_random_flip_1_layer_call_and_return_conditional_losses_62560

inputs
identityd
IdentityIdentityinputs*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
(__inference_dense_39_layer_call_fn_62509

inputs
unknown:@

	unknown_0:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *L
fGRE
C__inference_dense_39_layer_call_and_return_conditional_losses_607072
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
L
0__inference_max_pooling2d_72_layer_call_fn_62379

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������~~ * 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_606232
PartitionedCallt
IdentityIdentityPartitionedCall:output:0*
T0*/
_output_shapes
:���������~~ 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:����������� :Y U
1
_output_shapes
:����������� 
 
_user_specified_nameinputs
ϰ
�
!__inference__traced_restore_63098
file_prefix;
!assignvariableop_conv2d_72_kernel: /
!assignvariableop_1_conv2d_72_bias: =
#assignvariableop_2_conv2d_73_kernel: @/
!assignvariableop_3_conv2d_73_bias:@=
#assignvariableop_4_conv2d_74_kernel:@@/
!assignvariableop_5_conv2d_74_bias:@6
"assignvariableop_6_dense_38_kernel:
��@.
 assignvariableop_7_dense_38_bias:@4
"assignvariableop_8_dense_39_kernel:@
.
 assignvariableop_9_dense_39_bias:
'
assignvariableop_10_adam_iter:	 )
assignvariableop_11_adam_beta_1: )
assignvariableop_12_adam_beta_2: (
assignvariableop_13_adam_decay: 0
&assignvariableop_14_adam_learning_rate: *
assignvariableop_15_variable:	,
assignvariableop_16_variable_1:	#
assignvariableop_17_total: #
assignvariableop_18_count: %
assignvariableop_19_total_1: %
assignvariableop_20_count_1: E
+assignvariableop_21_adam_conv2d_72_kernel_m: 7
)assignvariableop_22_adam_conv2d_72_bias_m: E
+assignvariableop_23_adam_conv2d_73_kernel_m: @7
)assignvariableop_24_adam_conv2d_73_bias_m:@E
+assignvariableop_25_adam_conv2d_74_kernel_m:@@7
)assignvariableop_26_adam_conv2d_74_bias_m:@>
*assignvariableop_27_adam_dense_38_kernel_m:
��@6
(assignvariableop_28_adam_dense_38_bias_m:@<
*assignvariableop_29_adam_dense_39_kernel_m:@
6
(assignvariableop_30_adam_dense_39_bias_m:
E
+assignvariableop_31_adam_conv2d_72_kernel_v: 7
)assignvariableop_32_adam_conv2d_72_bias_v: E
+assignvariableop_33_adam_conv2d_73_kernel_v: @7
)assignvariableop_34_adam_conv2d_73_bias_v:@E
+assignvariableop_35_adam_conv2d_74_kernel_v:@@7
)assignvariableop_36_adam_conv2d_74_bias_v:@>
*assignvariableop_37_adam_dense_38_kernel_v:
��@6
(assignvariableop_38_adam_dense_38_bias_v:@<
*assignvariableop_39_adam_dense_39_kernel_v:@
6
(assignvariableop_40_adam_dense_39_bias_v:

identity_42��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:**
dtype0*�
value�B�*B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB:layer-1/layer-0/_rng/_state_var/.ATTRIBUTES/VARIABLE_VALUEB:layer-1/layer-1/_rng/_state_var/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:**
dtype0*g
value^B\*B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�::::::::::::::::::::::::::::::::::::::::::*8
dtypes.
,2*			2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp!assignvariableop_conv2d_72_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp!assignvariableop_1_conv2d_72_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp#assignvariableop_2_conv2d_73_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp!assignvariableop_3_conv2d_73_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp#assignvariableop_4_conv2d_74_kernelIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp!assignvariableop_5_conv2d_74_biasIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp"assignvariableop_6_dense_38_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp assignvariableop_7_dense_38_biasIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp"assignvariableop_8_dense_39_kernelIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp assignvariableop_9_dense_39_biasIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOpassignvariableop_10_adam_iterIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOpassignvariableop_11_adam_beta_1Identity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOpassignvariableop_12_adam_beta_2Identity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOpassignvariableop_13_adam_decayIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp&assignvariableop_14_adam_learning_rateIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOpassignvariableop_15_variableIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOpassignvariableop_16_variable_1Identity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOpassignvariableop_17_totalIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOpassignvariableop_18_countIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOpassignvariableop_19_total_1Identity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOpassignvariableop_20_count_1Identity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp+assignvariableop_21_adam_conv2d_72_kernel_mIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp)assignvariableop_22_adam_conv2d_72_bias_mIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp+assignvariableop_23_adam_conv2d_73_kernel_mIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp)assignvariableop_24_adam_conv2d_73_bias_mIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOp+assignvariableop_25_adam_conv2d_74_kernel_mIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOp)assignvariableop_26_adam_conv2d_74_bias_mIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp*assignvariableop_27_adam_dense_38_kernel_mIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp(assignvariableop_28_adam_dense_38_bias_mIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp*assignvariableop_29_adam_dense_39_kernel_mIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp(assignvariableop_30_adam_dense_39_bias_mIdentity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp+assignvariableop_31_adam_conv2d_72_kernel_vIdentity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp)assignvariableop_32_adam_conv2d_72_bias_vIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOp+assignvariableop_33_adam_conv2d_73_kernel_vIdentity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOp)assignvariableop_34_adam_conv2d_73_bias_vIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOp+assignvariableop_35_adam_conv2d_74_kernel_vIdentity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOp)assignvariableop_36_adam_conv2d_74_bias_vIdentity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOp*assignvariableop_37_adam_dense_38_kernel_vIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOp(assignvariableop_38_adam_dense_38_bias_vIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39�
AssignVariableOp_39AssignVariableOp*assignvariableop_39_adam_dense_39_kernel_vIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40�
AssignVariableOp_40AssignVariableOp(assignvariableop_40_adam_dense_39_bias_vIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_409
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_41Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_41f
Identity_42IdentityIdentity_41:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_42�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_42Identity_42:output:0*g
_input_shapesV
T: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
I
-__inference_sequential_11_layer_call_fn_61810

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_11_layer_call_and_return_conditional_losses_601762
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_62429

inputs
identity�
MaxPoolMaxPoolinputs*/
_output_shapes
:���������>>@*
ksize
*
paddingVALID*
strides
2	
MaxPooll
IdentityIdentityMaxPool:output:0*
T0*/
_output_shapes
:���������>>@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������||@:W S
/
_output_shapes
:���������||@
 
_user_specified_nameinputs
�
�
C__inference_dense_38_layer_call_and_return_conditional_losses_60690

inputs2
matmul_readvariableop_resource:
��@-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������@2
Relum
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������@2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*,
_input_shapes
:�����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:Q M
)
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
)__inference_conv2d_72_layer_call_fn_62358

inputs!
unknown: 
	unknown_0: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:����������� *$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_72_layer_call_and_return_conditional_losses_606132
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*1
_output_shapes
:����������� 2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�

n
H__inference_sequential_11_layer_call_and_return_conditional_losses_61845
resizing_1_input
identity�
resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2
resizing_1/resize/size�
 resizing_1/resize/ResizeBilinearResizeBilinearresizing_1_inputresizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(2"
 resizing_1/resize/ResizeBilinearm
rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2
rescaling_1/Cast/xq
rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2
rescaling_1/Cast_1/x�
rescaling_1/mulMul1resizing_1/resize/ResizeBilinear:resized_images:0rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2
rescaling_1/mul�
rescaling_1/addAddV2rescaling_1/mul:z:0rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
rescaling_1/addq
IdentityIdentityrescaling_1/add:z:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:c _
1
_output_shapes
:�����������
*
_user_specified_nameresizing_1_input
�
L
0__inference_max_pooling2d_72_layer_call_fn_62374

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_605362
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
F
*__inference_resizing_1_layer_call_fn_62525

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *N
fIRG
E__inference_resizing_1_layer_call_and_return_conditional_losses_601352
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
D__inference_conv2d_74_layer_call_and_return_conditional_losses_62449

inputs8
conv2d_readvariableop_resource:@@-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp�
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:@@*
dtype02
Conv2D/ReadVariableOp�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@*
paddingVALID*
strides
2
Conv2D�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@2	
BiasAdd`
ReluReluBiasAdd:output:0*
T0*/
_output_shapes
:���������<<@2
Reluu
IdentityIdentityRelu:activations:0^NoOp*
T0*/
_output_shapes
:���������<<@2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������>>@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:���������>>@
 
_user_specified_nameinputs
�
�
(__inference_dense_38_layer_call_fn_62489

inputs
unknown:
��@
	unknown_0:@
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *L
fGRE
C__inference_dense_38_layer_call_and_return_conditional_losses_606902
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������@2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*,
_input_shapes
:�����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
)
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
H__inference_sequential_12_layer_call_and_return_conditional_losses_60495

inputs!
random_flip_1_60488:	%
random_rotation_1_60491:	
identity��%random_flip_1/StatefulPartitionedCall�)random_rotation_1/StatefulPartitionedCall�
%random_flip_1/StatefulPartitionedCallStatefulPartitionedCallinputsrandom_flip_1_60488*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_random_flip_1_layer_call_and_return_conditional_losses_604732'
%random_flip_1/StatefulPartitionedCall�
)random_rotation_1/StatefulPartitionedCallStatefulPartitionedCall.random_flip_1/StatefulPartitionedCall:output:0random_rotation_1_60491*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *U
fPRN
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_603492+
)random_rotation_1/StatefulPartitionedCall�
IdentityIdentity2random_rotation_1/StatefulPartitionedCall:output:0^NoOp*
T0*1
_output_shapes
:�����������2

Identity�
NoOpNoOp&^random_flip_1/StatefulPartitionedCall*^random_rotation_1/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 2N
%random_flip_1/StatefulPartitionedCall%random_flip_1/StatefulPartitionedCall2V
)random_rotation_1/StatefulPartitionedCall)random_rotation_1/StatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
��
�
H__inference_sequential_12_layer_call_and_return_conditional_losses_62116

inputsM
?random_flip_1_stateful_uniform_full_int_rngreadandskip_resource:	H
:random_rotation_1_stateful_uniform_rngreadandskip_resource:	
identity��6random_flip_1/stateful_uniform_full_int/RngReadAndSkip�8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip�]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�drandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�Zrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�arandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�1random_rotation_1/stateful_uniform/RngReadAndSkip�
-random_flip_1/stateful_uniform_full_int/shapeConst*
_output_shapes
:*
dtype0*
valueB:2/
-random_flip_1/stateful_uniform_full_int/shape�
-random_flip_1/stateful_uniform_full_int/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2/
-random_flip_1/stateful_uniform_full_int/Const�
,random_flip_1/stateful_uniform_full_int/ProdProd6random_flip_1/stateful_uniform_full_int/shape:output:06random_flip_1/stateful_uniform_full_int/Const:output:0*
T0*
_output_shapes
: 2.
,random_flip_1/stateful_uniform_full_int/Prod�
.random_flip_1/stateful_uniform_full_int/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :20
.random_flip_1/stateful_uniform_full_int/Cast/x�
.random_flip_1/stateful_uniform_full_int/Cast_1Cast5random_flip_1/stateful_uniform_full_int/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 20
.random_flip_1/stateful_uniform_full_int/Cast_1�
6random_flip_1/stateful_uniform_full_int/RngReadAndSkipRngReadAndSkip?random_flip_1_stateful_uniform_full_int_rngreadandskip_resource7random_flip_1/stateful_uniform_full_int/Cast/x:output:02random_flip_1/stateful_uniform_full_int/Cast_1:y:0*
_output_shapes
:28
6random_flip_1/stateful_uniform_full_int/RngReadAndSkip�
;random_flip_1/stateful_uniform_full_int/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2=
;random_flip_1/stateful_uniform_full_int/strided_slice/stack�
=random_flip_1/stateful_uniform_full_int/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2?
=random_flip_1/stateful_uniform_full_int/strided_slice/stack_1�
=random_flip_1/stateful_uniform_full_int/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2?
=random_flip_1/stateful_uniform_full_int/strided_slice/stack_2�
5random_flip_1/stateful_uniform_full_int/strided_sliceStridedSlice>random_flip_1/stateful_uniform_full_int/RngReadAndSkip:value:0Drandom_flip_1/stateful_uniform_full_int/strided_slice/stack:output:0Frandom_flip_1/stateful_uniform_full_int/strided_slice/stack_1:output:0Frandom_flip_1/stateful_uniform_full_int/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask27
5random_flip_1/stateful_uniform_full_int/strided_slice�
/random_flip_1/stateful_uniform_full_int/BitcastBitcast>random_flip_1/stateful_uniform_full_int/strided_slice:output:0*
T0	*
_output_shapes
:*

type021
/random_flip_1/stateful_uniform_full_int/Bitcast�
=random_flip_1/stateful_uniform_full_int/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2?
=random_flip_1/stateful_uniform_full_int/strided_slice_1/stack�
?random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1�
?random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2�
7random_flip_1/stateful_uniform_full_int/strided_slice_1StridedSlice>random_flip_1/stateful_uniform_full_int/RngReadAndSkip:value:0Frandom_flip_1/stateful_uniform_full_int/strided_slice_1/stack:output:0Hrandom_flip_1/stateful_uniform_full_int/strided_slice_1/stack_1:output:0Hrandom_flip_1/stateful_uniform_full_int/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:29
7random_flip_1/stateful_uniform_full_int/strided_slice_1�
1random_flip_1/stateful_uniform_full_int/Bitcast_1Bitcast@random_flip_1/stateful_uniform_full_int/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type023
1random_flip_1/stateful_uniform_full_int/Bitcast_1�
+random_flip_1/stateful_uniform_full_int/algConst*
_output_shapes
: *
dtype0*
value	B :2-
+random_flip_1/stateful_uniform_full_int/alg�
'random_flip_1/stateful_uniform_full_intStatelessRandomUniformFullIntV26random_flip_1/stateful_uniform_full_int/shape:output:0:random_flip_1/stateful_uniform_full_int/Bitcast_1:output:08random_flip_1/stateful_uniform_full_int/Bitcast:output:04random_flip_1/stateful_uniform_full_int/alg:output:0*
_output_shapes
:*
dtype0	2)
'random_flip_1/stateful_uniform_full_int~
random_flip_1/zeros_likeConst*
_output_shapes
:*
dtype0	*
valueB	R 2
random_flip_1/zeros_like�
random_flip_1/stackPack0random_flip_1/stateful_uniform_full_int:output:0!random_flip_1/zeros_like:output:0*
N*
T0	*
_output_shapes

:2
random_flip_1/stack�
!random_flip_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2#
!random_flip_1/strided_slice/stack�
#random_flip_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2%
#random_flip_1/strided_slice/stack_1�
#random_flip_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2%
#random_flip_1/strided_slice/stack_2�
random_flip_1/strided_sliceStridedSlicerandom_flip_1/stack:output:0*random_flip_1/strided_slice/stack:output:0,random_flip_1/strided_slice/stack_1:output:0,random_flip_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2
random_flip_1/strided_slice�
Arandom_flip_1/stateless_random_flip_left_right/control_dependencyIdentityinputs*
T0*
_class
loc:@inputs*1
_output_shapes
:�����������2C
Arandom_flip_1/stateless_random_flip_left_right/control_dependency�
4random_flip_1/stateless_random_flip_left_right/ShapeShapeJrandom_flip_1/stateless_random_flip_left_right/control_dependency:output:0*
T0*
_output_shapes
:26
4random_flip_1/stateless_random_flip_left_right/Shape�
Brandom_flip_1/stateless_random_flip_left_right/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2D
Brandom_flip_1/stateless_random_flip_left_right/strided_slice/stack�
Drandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2F
Drandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_1�
Drandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2F
Drandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_2�
<random_flip_1/stateless_random_flip_left_right/strided_sliceStridedSlice=random_flip_1/stateless_random_flip_left_right/Shape:output:0Krandom_flip_1/stateless_random_flip_left_right/strided_slice/stack:output:0Mrandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_1:output:0Mrandom_flip_1/stateless_random_flip_left_right/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2>
<random_flip_1/stateless_random_flip_left_right/strided_slice�
Mrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shapePackErandom_flip_1/stateless_random_flip_left_right/strided_slice:output:0*
N*
T0*
_output_shapes
:2O
Mrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shape�
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2M
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min�
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2M
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/max�
drandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounter$random_flip_1/strided_slice:output:0* 
_output_shapes
::2f
drandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�
]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlge^random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2_
]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�
`random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2Vrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/shape:output:0jrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0nrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0crandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2b
`random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2�
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/subSubTrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/max:output:0Trandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2M
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/sub�
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mulMulirandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2:output:0Orandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2M
Krandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mul�
Grandom_flip_1/stateless_random_flip_left_right/stateless_random_uniformAddV2Orandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/mul:z:0Trandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2I
Grandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform�
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2@
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/1�
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2@
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/2�
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2@
>random_flip_1/stateless_random_flip_left_right/Reshape/shape/3�
<random_flip_1/stateless_random_flip_left_right/Reshape/shapePackErandom_flip_1/stateless_random_flip_left_right/strided_slice:output:0Grandom_flip_1/stateless_random_flip_left_right/Reshape/shape/1:output:0Grandom_flip_1/stateless_random_flip_left_right/Reshape/shape/2:output:0Grandom_flip_1/stateless_random_flip_left_right/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2>
<random_flip_1/stateless_random_flip_left_right/Reshape/shape�
6random_flip_1/stateless_random_flip_left_right/ReshapeReshapeKrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform:z:0Erandom_flip_1/stateless_random_flip_left_right/Reshape/shape:output:0*
T0*/
_output_shapes
:���������28
6random_flip_1/stateless_random_flip_left_right/Reshape�
4random_flip_1/stateless_random_flip_left_right/RoundRound?random_flip_1/stateless_random_flip_left_right/Reshape:output:0*
T0*/
_output_shapes
:���������26
4random_flip_1/stateless_random_flip_left_right/Round�
=random_flip_1/stateless_random_flip_left_right/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2?
=random_flip_1/stateless_random_flip_left_right/ReverseV2/axis�
8random_flip_1/stateless_random_flip_left_right/ReverseV2	ReverseV2Jrandom_flip_1/stateless_random_flip_left_right/control_dependency:output:0Frandom_flip_1/stateless_random_flip_left_right/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2:
8random_flip_1/stateless_random_flip_left_right/ReverseV2�
2random_flip_1/stateless_random_flip_left_right/mulMul8random_flip_1/stateless_random_flip_left_right/Round:y:0Arandom_flip_1/stateless_random_flip_left_right/ReverseV2:output:0*
T0*1
_output_shapes
:�����������24
2random_flip_1/stateless_random_flip_left_right/mul�
4random_flip_1/stateless_random_flip_left_right/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?26
4random_flip_1/stateless_random_flip_left_right/sub/x�
2random_flip_1/stateless_random_flip_left_right/subSub=random_flip_1/stateless_random_flip_left_right/sub/x:output:08random_flip_1/stateless_random_flip_left_right/Round:y:0*
T0*/
_output_shapes
:���������24
2random_flip_1/stateless_random_flip_left_right/sub�
4random_flip_1/stateless_random_flip_left_right/mul_1Mul6random_flip_1/stateless_random_flip_left_right/sub:z:0Jrandom_flip_1/stateless_random_flip_left_right/control_dependency:output:0*
T0*1
_output_shapes
:�����������26
4random_flip_1/stateless_random_flip_left_right/mul_1�
2random_flip_1/stateless_random_flip_left_right/addAddV26random_flip_1/stateless_random_flip_left_right/mul:z:08random_flip_1/stateless_random_flip_left_right/mul_1:z:0*
T0*1
_output_shapes
:�����������24
2random_flip_1/stateless_random_flip_left_right/add�
/random_flip_1/stateful_uniform_full_int_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:21
/random_flip_1/stateful_uniform_full_int_1/shape�
/random_flip_1/stateful_uniform_full_int_1/ConstConst*
_output_shapes
:*
dtype0*
valueB: 21
/random_flip_1/stateful_uniform_full_int_1/Const�
.random_flip_1/stateful_uniform_full_int_1/ProdProd8random_flip_1/stateful_uniform_full_int_1/shape:output:08random_flip_1/stateful_uniform_full_int_1/Const:output:0*
T0*
_output_shapes
: 20
.random_flip_1/stateful_uniform_full_int_1/Prod�
0random_flip_1/stateful_uniform_full_int_1/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :22
0random_flip_1/stateful_uniform_full_int_1/Cast/x�
0random_flip_1/stateful_uniform_full_int_1/Cast_1Cast7random_flip_1/stateful_uniform_full_int_1/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 22
0random_flip_1/stateful_uniform_full_int_1/Cast_1�
8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkipRngReadAndSkip?random_flip_1_stateful_uniform_full_int_rngreadandskip_resource9random_flip_1/stateful_uniform_full_int_1/Cast/x:output:04random_flip_1/stateful_uniform_full_int_1/Cast_1:y:07^random_flip_1/stateful_uniform_full_int/RngReadAndSkip*
_output_shapes
:2:
8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip�
=random_flip_1/stateful_uniform_full_int_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2?
=random_flip_1/stateful_uniform_full_int_1/strided_slice/stack�
?random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1�
?random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2�
7random_flip_1/stateful_uniform_full_int_1/strided_sliceStridedSlice@random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip:value:0Frandom_flip_1/stateful_uniform_full_int_1/strided_slice/stack:output:0Hrandom_flip_1/stateful_uniform_full_int_1/strided_slice/stack_1:output:0Hrandom_flip_1/stateful_uniform_full_int_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask29
7random_flip_1/stateful_uniform_full_int_1/strided_slice�
1random_flip_1/stateful_uniform_full_int_1/BitcastBitcast@random_flip_1/stateful_uniform_full_int_1/strided_slice:output:0*
T0	*
_output_shapes
:*

type023
1random_flip_1/stateful_uniform_full_int_1/Bitcast�
?random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2A
?random_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack�
Arandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2C
Arandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1�
Arandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2C
Arandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2�
9random_flip_1/stateful_uniform_full_int_1/strided_slice_1StridedSlice@random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip:value:0Hrandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack:output:0Jrandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_1:output:0Jrandom_flip_1/stateful_uniform_full_int_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2;
9random_flip_1/stateful_uniform_full_int_1/strided_slice_1�
3random_flip_1/stateful_uniform_full_int_1/Bitcast_1BitcastBrandom_flip_1/stateful_uniform_full_int_1/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type025
3random_flip_1/stateful_uniform_full_int_1/Bitcast_1�
-random_flip_1/stateful_uniform_full_int_1/algConst*
_output_shapes
: *
dtype0*
value	B :2/
-random_flip_1/stateful_uniform_full_int_1/alg�
)random_flip_1/stateful_uniform_full_int_1StatelessRandomUniformFullIntV28random_flip_1/stateful_uniform_full_int_1/shape:output:0<random_flip_1/stateful_uniform_full_int_1/Bitcast_1:output:0:random_flip_1/stateful_uniform_full_int_1/Bitcast:output:06random_flip_1/stateful_uniform_full_int_1/alg:output:0*
_output_shapes
:*
dtype0	2+
)random_flip_1/stateful_uniform_full_int_1�
random_flip_1/zeros_like_1Const*
_output_shapes
:*
dtype0	*
valueB	R 2
random_flip_1/zeros_like_1�
random_flip_1/stack_1Pack2random_flip_1/stateful_uniform_full_int_1:output:0#random_flip_1/zeros_like_1:output:0*
N*
T0	*
_output_shapes

:2
random_flip_1/stack_1�
#random_flip_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        2%
#random_flip_1/strided_slice_1/stack�
%random_flip_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2'
%random_flip_1/strided_slice_1/stack_1�
%random_flip_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2'
%random_flip_1/strided_slice_1/stack_2�
random_flip_1/strided_slice_1StridedSlicerandom_flip_1/stack_1:output:0,random_flip_1/strided_slice_1/stack:output:0.random_flip_1/strided_slice_1/stack_1:output:0.random_flip_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2
random_flip_1/strided_slice_1�
>random_flip_1/stateless_random_flip_up_down/control_dependencyIdentity6random_flip_1/stateless_random_flip_left_right/add:z:0*
T0*E
_class;
97loc:@random_flip_1/stateless_random_flip_left_right/add*1
_output_shapes
:�����������2@
>random_flip_1/stateless_random_flip_up_down/control_dependency�
1random_flip_1/stateless_random_flip_up_down/ShapeShapeGrandom_flip_1/stateless_random_flip_up_down/control_dependency:output:0*
T0*
_output_shapes
:23
1random_flip_1/stateless_random_flip_up_down/Shape�
?random_flip_1/stateless_random_flip_up_down/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2A
?random_flip_1/stateless_random_flip_up_down/strided_slice/stack�
Arandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2C
Arandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_1�
Arandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2C
Arandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_2�
9random_flip_1/stateless_random_flip_up_down/strided_sliceStridedSlice:random_flip_1/stateless_random_flip_up_down/Shape:output:0Hrandom_flip_1/stateless_random_flip_up_down/strided_slice/stack:output:0Jrandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_1:output:0Jrandom_flip_1/stateless_random_flip_up_down/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2;
9random_flip_1/stateless_random_flip_up_down/strided_slice�
Jrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shapePackBrandom_flip_1/stateless_random_flip_up_down/strided_slice:output:0*
N*
T0*
_output_shapes
:2L
Jrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shape�
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2J
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min�
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2J
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/max�
arandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounter&random_flip_1/strided_slice_1:output:0^^random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg* 
_output_shapes
::2c
arandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�
Zrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgb^random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2\
Zrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�
]random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2Srandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/shape:output:0grandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0krandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0`random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2_
]random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2�
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/subSubQrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/max:output:0Qrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2J
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/sub�
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mulMulfrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2:output:0Lrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2J
Hrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mul�
Drandom_flip_1/stateless_random_flip_up_down/stateless_random_uniformAddV2Lrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/mul:z:0Qrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2F
Drandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform�
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2=
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/1�
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2=
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/2�
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2=
;random_flip_1/stateless_random_flip_up_down/Reshape/shape/3�
9random_flip_1/stateless_random_flip_up_down/Reshape/shapePackBrandom_flip_1/stateless_random_flip_up_down/strided_slice:output:0Drandom_flip_1/stateless_random_flip_up_down/Reshape/shape/1:output:0Drandom_flip_1/stateless_random_flip_up_down/Reshape/shape/2:output:0Drandom_flip_1/stateless_random_flip_up_down/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2;
9random_flip_1/stateless_random_flip_up_down/Reshape/shape�
3random_flip_1/stateless_random_flip_up_down/ReshapeReshapeHrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform:z:0Brandom_flip_1/stateless_random_flip_up_down/Reshape/shape:output:0*
T0*/
_output_shapes
:���������25
3random_flip_1/stateless_random_flip_up_down/Reshape�
1random_flip_1/stateless_random_flip_up_down/RoundRound<random_flip_1/stateless_random_flip_up_down/Reshape:output:0*
T0*/
_output_shapes
:���������23
1random_flip_1/stateless_random_flip_up_down/Round�
:random_flip_1/stateless_random_flip_up_down/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2<
:random_flip_1/stateless_random_flip_up_down/ReverseV2/axis�
5random_flip_1/stateless_random_flip_up_down/ReverseV2	ReverseV2Grandom_flip_1/stateless_random_flip_up_down/control_dependency:output:0Crandom_flip_1/stateless_random_flip_up_down/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������27
5random_flip_1/stateless_random_flip_up_down/ReverseV2�
/random_flip_1/stateless_random_flip_up_down/mulMul5random_flip_1/stateless_random_flip_up_down/Round:y:0>random_flip_1/stateless_random_flip_up_down/ReverseV2:output:0*
T0*1
_output_shapes
:�����������21
/random_flip_1/stateless_random_flip_up_down/mul�
1random_flip_1/stateless_random_flip_up_down/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?23
1random_flip_1/stateless_random_flip_up_down/sub/x�
/random_flip_1/stateless_random_flip_up_down/subSub:random_flip_1/stateless_random_flip_up_down/sub/x:output:05random_flip_1/stateless_random_flip_up_down/Round:y:0*
T0*/
_output_shapes
:���������21
/random_flip_1/stateless_random_flip_up_down/sub�
1random_flip_1/stateless_random_flip_up_down/mul_1Mul3random_flip_1/stateless_random_flip_up_down/sub:z:0Grandom_flip_1/stateless_random_flip_up_down/control_dependency:output:0*
T0*1
_output_shapes
:�����������23
1random_flip_1/stateless_random_flip_up_down/mul_1�
/random_flip_1/stateless_random_flip_up_down/addAddV23random_flip_1/stateless_random_flip_up_down/mul:z:05random_flip_1/stateless_random_flip_up_down/mul_1:z:0*
T0*1
_output_shapes
:�����������21
/random_flip_1/stateless_random_flip_up_down/add�
random_rotation_1/ShapeShape3random_flip_1/stateless_random_flip_up_down/add:z:0*
T0*
_output_shapes
:2
random_rotation_1/Shape�
%random_rotation_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2'
%random_rotation_1/strided_slice/stack�
'random_rotation_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2)
'random_rotation_1/strided_slice/stack_1�
'random_rotation_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2)
'random_rotation_1/strided_slice/stack_2�
random_rotation_1/strided_sliceStridedSlice random_rotation_1/Shape:output:0.random_rotation_1/strided_slice/stack:output:00random_rotation_1/strided_slice/stack_1:output:00random_rotation_1/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2!
random_rotation_1/strided_slice�
'random_rotation_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������2)
'random_rotation_1/strided_slice_1/stack�
)random_rotation_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������2+
)random_rotation_1/strided_slice_1/stack_1�
)random_rotation_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2+
)random_rotation_1/strided_slice_1/stack_2�
!random_rotation_1/strided_slice_1StridedSlice random_rotation_1/Shape:output:00random_rotation_1/strided_slice_1/stack:output:02random_rotation_1/strided_slice_1/stack_1:output:02random_rotation_1/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2#
!random_rotation_1/strided_slice_1�
random_rotation_1/CastCast*random_rotation_1/strided_slice_1:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
random_rotation_1/Cast�
'random_rotation_1/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������2)
'random_rotation_1/strided_slice_2/stack�
)random_rotation_1/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������2+
)random_rotation_1/strided_slice_2/stack_1�
)random_rotation_1/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2+
)random_rotation_1/strided_slice_2/stack_2�
!random_rotation_1/strided_slice_2StridedSlice random_rotation_1/Shape:output:00random_rotation_1/strided_slice_2/stack:output:02random_rotation_1/strided_slice_2/stack_1:output:02random_rotation_1/strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2#
!random_rotation_1/strided_slice_2�
random_rotation_1/Cast_1Cast*random_rotation_1/strided_slice_2:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
random_rotation_1/Cast_1�
(random_rotation_1/stateful_uniform/shapePack(random_rotation_1/strided_slice:output:0*
N*
T0*
_output_shapes
:2*
(random_rotation_1/stateful_uniform/shape�
&random_rotation_1/stateful_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *|٠�2(
&random_rotation_1/stateful_uniform/min�
&random_rotation_1/stateful_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *|٠?2(
&random_rotation_1/stateful_uniform/max�
(random_rotation_1/stateful_uniform/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2*
(random_rotation_1/stateful_uniform/Const�
'random_rotation_1/stateful_uniform/ProdProd1random_rotation_1/stateful_uniform/shape:output:01random_rotation_1/stateful_uniform/Const:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/stateful_uniform/Prod�
)random_rotation_1/stateful_uniform/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2+
)random_rotation_1/stateful_uniform/Cast/x�
)random_rotation_1/stateful_uniform/Cast_1Cast0random_rotation_1/stateful_uniform/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2+
)random_rotation_1/stateful_uniform/Cast_1�
1random_rotation_1/stateful_uniform/RngReadAndSkipRngReadAndSkip:random_rotation_1_stateful_uniform_rngreadandskip_resource2random_rotation_1/stateful_uniform/Cast/x:output:0-random_rotation_1/stateful_uniform/Cast_1:y:0*
_output_shapes
:23
1random_rotation_1/stateful_uniform/RngReadAndSkip�
6random_rotation_1/stateful_uniform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 28
6random_rotation_1/stateful_uniform/strided_slice/stack�
8random_rotation_1/stateful_uniform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2:
8random_rotation_1/stateful_uniform/strided_slice/stack_1�
8random_rotation_1/stateful_uniform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2:
8random_rotation_1/stateful_uniform/strided_slice/stack_2�
0random_rotation_1/stateful_uniform/strided_sliceStridedSlice9random_rotation_1/stateful_uniform/RngReadAndSkip:value:0?random_rotation_1/stateful_uniform/strided_slice/stack:output:0Arandom_rotation_1/stateful_uniform/strided_slice/stack_1:output:0Arandom_rotation_1/stateful_uniform/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask22
0random_rotation_1/stateful_uniform/strided_slice�
*random_rotation_1/stateful_uniform/BitcastBitcast9random_rotation_1/stateful_uniform/strided_slice:output:0*
T0	*
_output_shapes
:*

type02,
*random_rotation_1/stateful_uniform/Bitcast�
8random_rotation_1/stateful_uniform/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2:
8random_rotation_1/stateful_uniform/strided_slice_1/stack�
:random_rotation_1/stateful_uniform/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2<
:random_rotation_1/stateful_uniform/strided_slice_1/stack_1�
:random_rotation_1/stateful_uniform/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2<
:random_rotation_1/stateful_uniform/strided_slice_1/stack_2�
2random_rotation_1/stateful_uniform/strided_slice_1StridedSlice9random_rotation_1/stateful_uniform/RngReadAndSkip:value:0Arandom_rotation_1/stateful_uniform/strided_slice_1/stack:output:0Crandom_rotation_1/stateful_uniform/strided_slice_1/stack_1:output:0Crandom_rotation_1/stateful_uniform/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:24
2random_rotation_1/stateful_uniform/strided_slice_1�
,random_rotation_1/stateful_uniform/Bitcast_1Bitcast;random_rotation_1/stateful_uniform/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02.
,random_rotation_1/stateful_uniform/Bitcast_1�
?random_rotation_1/stateful_uniform/StatelessRandomUniformV2/algConst*
_output_shapes
: *
dtype0*
value	B :2A
?random_rotation_1/stateful_uniform/StatelessRandomUniformV2/alg�
;random_rotation_1/stateful_uniform/StatelessRandomUniformV2StatelessRandomUniformV21random_rotation_1/stateful_uniform/shape:output:05random_rotation_1/stateful_uniform/Bitcast_1:output:03random_rotation_1/stateful_uniform/Bitcast:output:0Hrandom_rotation_1/stateful_uniform/StatelessRandomUniformV2/alg:output:0*#
_output_shapes
:���������2=
;random_rotation_1/stateful_uniform/StatelessRandomUniformV2�
&random_rotation_1/stateful_uniform/subSub/random_rotation_1/stateful_uniform/max:output:0/random_rotation_1/stateful_uniform/min:output:0*
T0*
_output_shapes
: 2(
&random_rotation_1/stateful_uniform/sub�
&random_rotation_1/stateful_uniform/mulMulDrandom_rotation_1/stateful_uniform/StatelessRandomUniformV2:output:0*random_rotation_1/stateful_uniform/sub:z:0*
T0*#
_output_shapes
:���������2(
&random_rotation_1/stateful_uniform/mul�
"random_rotation_1/stateful_uniformAddV2*random_rotation_1/stateful_uniform/mul:z:0/random_rotation_1/stateful_uniform/min:output:0*
T0*#
_output_shapes
:���������2$
"random_rotation_1/stateful_uniform�
'random_rotation_1/rotation_matrix/sub/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2)
'random_rotation_1/rotation_matrix/sub/y�
%random_rotation_1/rotation_matrix/subSubrandom_rotation_1/Cast_1:y:00random_rotation_1/rotation_matrix/sub/y:output:0*
T0*
_output_shapes
: 2'
%random_rotation_1/rotation_matrix/sub�
%random_rotation_1/rotation_matrix/CosCos&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/Cos�
)random_rotation_1/rotation_matrix/sub_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_1/y�
'random_rotation_1/rotation_matrix/sub_1Subrandom_rotation_1/Cast_1:y:02random_rotation_1/rotation_matrix/sub_1/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_1�
%random_rotation_1/rotation_matrix/mulMul)random_rotation_1/rotation_matrix/Cos:y:0+random_rotation_1/rotation_matrix/sub_1:z:0*
T0*#
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/mul�
%random_rotation_1/rotation_matrix/SinSin&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/Sin�
)random_rotation_1/rotation_matrix/sub_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_2/y�
'random_rotation_1/rotation_matrix/sub_2Subrandom_rotation_1/Cast:y:02random_rotation_1/rotation_matrix/sub_2/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_2�
'random_rotation_1/rotation_matrix/mul_1Mul)random_rotation_1/rotation_matrix/Sin:y:0+random_rotation_1/rotation_matrix/sub_2:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/mul_1�
'random_rotation_1/rotation_matrix/sub_3Sub)random_rotation_1/rotation_matrix/mul:z:0+random_rotation_1/rotation_matrix/mul_1:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/sub_3�
'random_rotation_1/rotation_matrix/sub_4Sub)random_rotation_1/rotation_matrix/sub:z:0+random_rotation_1/rotation_matrix/sub_3:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/sub_4�
+random_rotation_1/rotation_matrix/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2-
+random_rotation_1/rotation_matrix/truediv/y�
)random_rotation_1/rotation_matrix/truedivRealDiv+random_rotation_1/rotation_matrix/sub_4:z:04random_rotation_1/rotation_matrix/truediv/y:output:0*
T0*#
_output_shapes
:���������2+
)random_rotation_1/rotation_matrix/truediv�
)random_rotation_1/rotation_matrix/sub_5/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_5/y�
'random_rotation_1/rotation_matrix/sub_5Subrandom_rotation_1/Cast:y:02random_rotation_1/rotation_matrix/sub_5/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_5�
'random_rotation_1/rotation_matrix/Sin_1Sin&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Sin_1�
)random_rotation_1/rotation_matrix/sub_6/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_6/y�
'random_rotation_1/rotation_matrix/sub_6Subrandom_rotation_1/Cast_1:y:02random_rotation_1/rotation_matrix/sub_6/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_6�
'random_rotation_1/rotation_matrix/mul_2Mul+random_rotation_1/rotation_matrix/Sin_1:y:0+random_rotation_1/rotation_matrix/sub_6:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/mul_2�
'random_rotation_1/rotation_matrix/Cos_1Cos&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Cos_1�
)random_rotation_1/rotation_matrix/sub_7/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2+
)random_rotation_1/rotation_matrix/sub_7/y�
'random_rotation_1/rotation_matrix/sub_7Subrandom_rotation_1/Cast:y:02random_rotation_1/rotation_matrix/sub_7/y:output:0*
T0*
_output_shapes
: 2)
'random_rotation_1/rotation_matrix/sub_7�
'random_rotation_1/rotation_matrix/mul_3Mul+random_rotation_1/rotation_matrix/Cos_1:y:0+random_rotation_1/rotation_matrix/sub_7:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/mul_3�
%random_rotation_1/rotation_matrix/addAddV2+random_rotation_1/rotation_matrix/mul_2:z:0+random_rotation_1/rotation_matrix/mul_3:z:0*
T0*#
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/add�
'random_rotation_1/rotation_matrix/sub_8Sub+random_rotation_1/rotation_matrix/sub_5:z:0)random_rotation_1/rotation_matrix/add:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/sub_8�
-random_rotation_1/rotation_matrix/truediv_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2/
-random_rotation_1/rotation_matrix/truediv_1/y�
+random_rotation_1/rotation_matrix/truediv_1RealDiv+random_rotation_1/rotation_matrix/sub_8:z:06random_rotation_1/rotation_matrix/truediv_1/y:output:0*
T0*#
_output_shapes
:���������2-
+random_rotation_1/rotation_matrix/truediv_1�
'random_rotation_1/rotation_matrix/ShapeShape&random_rotation_1/stateful_uniform:z:0*
T0*
_output_shapes
:2)
'random_rotation_1/rotation_matrix/Shape�
5random_rotation_1/rotation_matrix/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 27
5random_rotation_1/rotation_matrix/strided_slice/stack�
7random_rotation_1/rotation_matrix/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:29
7random_rotation_1/rotation_matrix/strided_slice/stack_1�
7random_rotation_1/rotation_matrix/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:29
7random_rotation_1/rotation_matrix/strided_slice/stack_2�
/random_rotation_1/rotation_matrix/strided_sliceStridedSlice0random_rotation_1/rotation_matrix/Shape:output:0>random_rotation_1/rotation_matrix/strided_slice/stack:output:0@random_rotation_1/rotation_matrix/strided_slice/stack_1:output:0@random_rotation_1/rotation_matrix/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask21
/random_rotation_1/rotation_matrix/strided_slice�
'random_rotation_1/rotation_matrix/Cos_2Cos&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Cos_2�
7random_rotation_1/rotation_matrix/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_1/stack�
9random_rotation_1/rotation_matrix/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_1/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_1/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_1StridedSlice+random_rotation_1/rotation_matrix/Cos_2:y:0@random_rotation_1/rotation_matrix/strided_slice_1/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_1/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_1/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_1�
'random_rotation_1/rotation_matrix/Sin_2Sin&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Sin_2�
7random_rotation_1/rotation_matrix/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_2/stack�
9random_rotation_1/rotation_matrix/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_2/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_2/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_2StridedSlice+random_rotation_1/rotation_matrix/Sin_2:y:0@random_rotation_1/rotation_matrix/strided_slice_2/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_2/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_2�
%random_rotation_1/rotation_matrix/NegNeg:random_rotation_1/rotation_matrix/strided_slice_2:output:0*
T0*'
_output_shapes
:���������2'
%random_rotation_1/rotation_matrix/Neg�
7random_rotation_1/rotation_matrix/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_3/stack�
9random_rotation_1/rotation_matrix/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_3/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_3/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_3StridedSlice-random_rotation_1/rotation_matrix/truediv:z:0@random_rotation_1/rotation_matrix/strided_slice_3/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_3/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_3�
'random_rotation_1/rotation_matrix/Sin_3Sin&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Sin_3�
7random_rotation_1/rotation_matrix/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_4/stack�
9random_rotation_1/rotation_matrix/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_4/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_4/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_4StridedSlice+random_rotation_1/rotation_matrix/Sin_3:y:0@random_rotation_1/rotation_matrix/strided_slice_4/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_4/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_4/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_4�
'random_rotation_1/rotation_matrix/Cos_3Cos&random_rotation_1/stateful_uniform:z:0*
T0*#
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/Cos_3�
7random_rotation_1/rotation_matrix/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_5/stack�
9random_rotation_1/rotation_matrix/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_5/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_5/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_5StridedSlice+random_rotation_1/rotation_matrix/Cos_3:y:0@random_rotation_1/rotation_matrix/strided_slice_5/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_5/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_5/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_5�
7random_rotation_1/rotation_matrix/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB"        29
7random_rotation_1/rotation_matrix/strided_slice_6/stack�
9random_rotation_1/rotation_matrix/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2;
9random_rotation_1/rotation_matrix/strided_slice_6/stack_1�
9random_rotation_1/rotation_matrix/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2;
9random_rotation_1/rotation_matrix/strided_slice_6/stack_2�
1random_rotation_1/rotation_matrix/strided_slice_6StridedSlice/random_rotation_1/rotation_matrix/truediv_1:z:0@random_rotation_1/rotation_matrix/strided_slice_6/stack:output:0Brandom_rotation_1/rotation_matrix/strided_slice_6/stack_1:output:0Brandom_rotation_1/rotation_matrix/strided_slice_6/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask23
1random_rotation_1/rotation_matrix/strided_slice_6�
-random_rotation_1/rotation_matrix/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2/
-random_rotation_1/rotation_matrix/zeros/mul/y�
+random_rotation_1/rotation_matrix/zeros/mulMul8random_rotation_1/rotation_matrix/strided_slice:output:06random_rotation_1/rotation_matrix/zeros/mul/y:output:0*
T0*
_output_shapes
: 2-
+random_rotation_1/rotation_matrix/zeros/mul�
.random_rotation_1/rotation_matrix/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :�20
.random_rotation_1/rotation_matrix/zeros/Less/y�
,random_rotation_1/rotation_matrix/zeros/LessLess/random_rotation_1/rotation_matrix/zeros/mul:z:07random_rotation_1/rotation_matrix/zeros/Less/y:output:0*
T0*
_output_shapes
: 2.
,random_rotation_1/rotation_matrix/zeros/Less�
0random_rotation_1/rotation_matrix/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :22
0random_rotation_1/rotation_matrix/zeros/packed/1�
.random_rotation_1/rotation_matrix/zeros/packedPack8random_rotation_1/rotation_matrix/strided_slice:output:09random_rotation_1/rotation_matrix/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:20
.random_rotation_1/rotation_matrix/zeros/packed�
-random_rotation_1/rotation_matrix/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2/
-random_rotation_1/rotation_matrix/zeros/Const�
'random_rotation_1/rotation_matrix/zerosFill7random_rotation_1/rotation_matrix/zeros/packed:output:06random_rotation_1/rotation_matrix/zeros/Const:output:0*
T0*'
_output_shapes
:���������2)
'random_rotation_1/rotation_matrix/zeros�
-random_rotation_1/rotation_matrix/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2/
-random_rotation_1/rotation_matrix/concat/axis�
(random_rotation_1/rotation_matrix/concatConcatV2:random_rotation_1/rotation_matrix/strided_slice_1:output:0)random_rotation_1/rotation_matrix/Neg:y:0:random_rotation_1/rotation_matrix/strided_slice_3:output:0:random_rotation_1/rotation_matrix/strided_slice_4:output:0:random_rotation_1/rotation_matrix/strided_slice_5:output:0:random_rotation_1/rotation_matrix/strided_slice_6:output:00random_rotation_1/rotation_matrix/zeros:output:06random_rotation_1/rotation_matrix/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2*
(random_rotation_1/rotation_matrix/concat�
!random_rotation_1/transform/ShapeShape3random_flip_1/stateless_random_flip_up_down/add:z:0*
T0*
_output_shapes
:2#
!random_rotation_1/transform/Shape�
/random_rotation_1/transform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:21
/random_rotation_1/transform/strided_slice/stack�
1random_rotation_1/transform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:23
1random_rotation_1/transform/strided_slice/stack_1�
1random_rotation_1/transform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:23
1random_rotation_1/transform/strided_slice/stack_2�
)random_rotation_1/transform/strided_sliceStridedSlice*random_rotation_1/transform/Shape:output:08random_rotation_1/transform/strided_slice/stack:output:0:random_rotation_1/transform/strided_slice/stack_1:output:0:random_rotation_1/transform/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2+
)random_rotation_1/transform/strided_slice�
&random_rotation_1/transform/fill_valueConst*
_output_shapes
: *
dtype0*
valueB
 *    2(
&random_rotation_1/transform/fill_value�
6random_rotation_1/transform/ImageProjectiveTransformV3ImageProjectiveTransformV33random_flip_1/stateless_random_flip_up_down/add:z:01random_rotation_1/rotation_matrix/concat:output:02random_rotation_1/transform/strided_slice:output:0/random_rotation_1/transform/fill_value:output:0*1
_output_shapes
:�����������*
dtype0*
	fill_mode	REFLECT*
interpolation
BILINEAR28
6random_rotation_1/transform/ImageProjectiveTransformV3�
IdentityIdentityKrandom_rotation_1/transform/ImageProjectiveTransformV3:transformed_images:0^NoOp*
T0*1
_output_shapes
:�����������2

Identity�
NoOpNoOp7^random_flip_1/stateful_uniform_full_int/RngReadAndSkip9^random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip^^random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlge^random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter[^random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgb^random_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter2^random_rotation_1/stateful_uniform/RngReadAndSkip*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 2p
6random_flip_1/stateful_uniform_full_int/RngReadAndSkip6random_flip_1/stateful_uniform_full_int/RngReadAndSkip2t
8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip8random_flip_1/stateful_uniform_full_int_1/RngReadAndSkip2�
]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg]random_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg2�
drandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterdrandom_flip_1/stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter2�
Zrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgZrandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg2�
arandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterarandom_flip_1/stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter2f
1random_rotation_1/stateful_uniform/RngReadAndSkip1random_rotation_1/stateful_uniform/RngReadAndSkip:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
D__inference_conv2d_73_layer_call_and_return_conditional_losses_62409

inputs8
conv2d_readvariableop_resource: @-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp�
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: @*
dtype02
Conv2D/ReadVariableOp�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@*
paddingVALID*
strides
2
Conv2D�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������||@2	
BiasAdd`
ReluReluBiasAdd:output:0*
T0*/
_output_shapes
:���������||@2
Reluu
IdentityIdentityRelu:activations:0^NoOp*
T0*/
_output_shapes
:���������||@2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������~~ : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:���������~~ 
 
_user_specified_nameinputs
�
h
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_60213

inputs
identityd
IdentityIdentityinputs*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_60669

inputs
identity�
MaxPoolMaxPoolinputs*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
2	
MaxPooll
IdentityIdentityMaxPool:output:0*
T0*/
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������<<@:W S
/
_output_shapes
:���������<<@
 
_user_specified_nameinputs
�
�
-__inference_sequential_23_layer_call_fn_61116

inputs
unknown:	
	unknown_0:	#
	unknown_1: 
	unknown_2: #
	unknown_3: @
	unknown_4:@#
	unknown_5:@@
	unknown_6:@
	unknown_7:
��@
	unknown_8:@
	unknown_9:@


unknown_10:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*,
_read_only_resource_inputs

	
*/
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_23_layer_call_and_return_conditional_losses_608742
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:�����������: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�0
�
H__inference_sequential_23_layer_call_and_return_conditional_losses_60714

inputs)
conv2d_72_60614: 
conv2d_72_60616: )
conv2d_73_60637: @
conv2d_73_60639:@)
conv2d_74_60660:@@
conv2d_74_60662:@"
dense_38_60691:
��@
dense_38_60693:@ 
dense_39_60708:@

dense_39_60710:

identity��!conv2d_72/StatefulPartitionedCall�!conv2d_73/StatefulPartitionedCall�!conv2d_74/StatefulPartitionedCall� dense_38/StatefulPartitionedCall� dense_39/StatefulPartitionedCall�
sequential_11/PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_11_layer_call_and_return_conditional_losses_601482
sequential_11/PartitionedCall�
sequential_12/PartitionedCallPartitionedCall&sequential_11/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_12_layer_call_and_return_conditional_losses_602162
sequential_12/PartitionedCall�
!conv2d_72/StatefulPartitionedCallStatefulPartitionedCall&sequential_12/PartitionedCall:output:0conv2d_72_60614conv2d_72_60616*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:����������� *$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_72_layer_call_and_return_conditional_losses_606132#
!conv2d_72/StatefulPartitionedCall�
 max_pooling2d_72/PartitionedCallPartitionedCall*conv2d_72/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������~~ * 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_606232"
 max_pooling2d_72/PartitionedCall�
!conv2d_73/StatefulPartitionedCallStatefulPartitionedCall)max_pooling2d_72/PartitionedCall:output:0conv2d_73_60637conv2d_73_60639*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������||@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_73_layer_call_and_return_conditional_losses_606362#
!conv2d_73/StatefulPartitionedCall�
 max_pooling2d_73/PartitionedCallPartitionedCall*conv2d_73/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������>>@* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_606462"
 max_pooling2d_73/PartitionedCall�
!conv2d_74/StatefulPartitionedCallStatefulPartitionedCall)max_pooling2d_73/PartitionedCall:output:0conv2d_74_60660conv2d_74_60662*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<<@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_74_layer_call_and_return_conditional_losses_606592#
!conv2d_74/StatefulPartitionedCall�
 max_pooling2d_74/PartitionedCallPartitionedCall*conv2d_74/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_606692"
 max_pooling2d_74/PartitionedCall�
flatten_19/PartitionedCallPartitionedCall)max_pooling2d_74/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *)
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *N
fIRG
E__inference_flatten_19_layer_call_and_return_conditional_losses_606772
flatten_19/PartitionedCall�
 dense_38/StatefulPartitionedCallStatefulPartitionedCall#flatten_19/PartitionedCall:output:0dense_38_60691dense_38_60693*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *L
fGRE
C__inference_dense_38_layer_call_and_return_conditional_losses_606902"
 dense_38/StatefulPartitionedCall�
 dense_39/StatefulPartitionedCallStatefulPartitionedCall)dense_38/StatefulPartitionedCall:output:0dense_39_60708dense_39_60710*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *L
fGRE
C__inference_dense_39_layer_call_and_return_conditional_losses_607072"
 dense_39/StatefulPartitionedCall�
IdentityIdentity)dense_39/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity�
NoOpNoOp"^conv2d_72/StatefulPartitionedCall"^conv2d_73/StatefulPartitionedCall"^conv2d_74/StatefulPartitionedCall!^dense_38/StatefulPartitionedCall!^dense_39/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:�����������: : : : : : : : : : 2F
!conv2d_72/StatefulPartitionedCall!conv2d_72/StatefulPartitionedCall2F
!conv2d_73/StatefulPartitionedCall!conv2d_73/StatefulPartitionedCall2F
!conv2d_74/StatefulPartitionedCall!conv2d_74/StatefulPartitionedCall2D
 dense_38/StatefulPartitionedCall dense_38/StatefulPartitionedCall2D
 dense_39/StatefulPartitionedCall dense_39/StatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�

d
H__inference_sequential_11_layer_call_and_return_conditional_losses_61825

inputs
identity�
resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2
resizing_1/resize/size�
 resizing_1/resize/ResizeBilinearResizeBilinearinputsresizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(2"
 resizing_1/resize/ResizeBilinearm
rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2
rescaling_1/Cast/xq
rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2
rescaling_1/Cast_1/x�
rescaling_1/mulMul1resizing_1/resize/ResizeBilinear:resized_images:0rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2
rescaling_1/mul�
rescaling_1/addAddV2rescaling_1/mul:z:0rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
rescaling_1/addq
IdentityIdentityrescaling_1/add:z:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_62469

inputs
identity�
MaxPoolMaxPoolinputs*/
_output_shapes
:���������@*
ksize
*
paddingVALID*
strides
2	
MaxPooll
IdentityIdentityMaxPool:output:0*
T0*/
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������<<@:W S
/
_output_shapes
:���������<<@
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_60646

inputs
identity�
MaxPoolMaxPoolinputs*/
_output_shapes
:���������>>@*
ksize
*
paddingVALID*
strides
2	
MaxPooll
IdentityIdentityMaxPool:output:0*
T0*/
_output_shapes
:���������>>@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������||@:W S
/
_output_shapes
:���������||@
 
_user_specified_nameinputs
�3
�
H__inference_sequential_23_layer_call_and_return_conditional_losses_60874

inputs!
sequential_12_60839:	!
sequential_12_60841:	)
conv2d_72_60844: 
conv2d_72_60846: )
conv2d_73_60850: @
conv2d_73_60852:@)
conv2d_74_60856:@@
conv2d_74_60858:@"
dense_38_60863:
��@
dense_38_60865:@ 
dense_39_60868:@

dense_39_60870:

identity��!conv2d_72/StatefulPartitionedCall�!conv2d_73/StatefulPartitionedCall�!conv2d_74/StatefulPartitionedCall� dense_38/StatefulPartitionedCall� dense_39/StatefulPartitionedCall�%sequential_12/StatefulPartitionedCall�
sequential_11/PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_11_layer_call_and_return_conditional_losses_601762
sequential_11/PartitionedCall�
%sequential_12/StatefulPartitionedCallStatefulPartitionedCall&sequential_11/PartitionedCall:output:0sequential_12_60839sequential_12_60841*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_12_layer_call_and_return_conditional_losses_604952'
%sequential_12/StatefulPartitionedCall�
!conv2d_72/StatefulPartitionedCallStatefulPartitionedCall.sequential_12/StatefulPartitionedCall:output:0conv2d_72_60844conv2d_72_60846*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:����������� *$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_72_layer_call_and_return_conditional_losses_606132#
!conv2d_72/StatefulPartitionedCall�
 max_pooling2d_72/PartitionedCallPartitionedCall*conv2d_72/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������~~ * 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_606232"
 max_pooling2d_72/PartitionedCall�
!conv2d_73/StatefulPartitionedCallStatefulPartitionedCall)max_pooling2d_72/PartitionedCall:output:0conv2d_73_60850conv2d_73_60852*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������||@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_73_layer_call_and_return_conditional_losses_606362#
!conv2d_73/StatefulPartitionedCall�
 max_pooling2d_73/PartitionedCallPartitionedCall*conv2d_73/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������>>@* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_606462"
 max_pooling2d_73/PartitionedCall�
!conv2d_74/StatefulPartitionedCallStatefulPartitionedCall)max_pooling2d_73/PartitionedCall:output:0conv2d_74_60856conv2d_74_60858*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<<@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *M
fHRF
D__inference_conv2d_74_layer_call_and_return_conditional_losses_606592#
!conv2d_74/StatefulPartitionedCall�
 max_pooling2d_74/PartitionedCallPartitionedCall*conv2d_74/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_606692"
 max_pooling2d_74/PartitionedCall�
flatten_19/PartitionedCallPartitionedCall)max_pooling2d_74/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *)
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *N
fIRG
E__inference_flatten_19_layer_call_and_return_conditional_losses_606772
flatten_19/PartitionedCall�
 dense_38/StatefulPartitionedCallStatefulPartitionedCall#flatten_19/PartitionedCall:output:0dense_38_60863dense_38_60865*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *L
fGRE
C__inference_dense_38_layer_call_and_return_conditional_losses_606902"
 dense_38/StatefulPartitionedCall�
 dense_39/StatefulPartitionedCallStatefulPartitionedCall)dense_38/StatefulPartitionedCall:output:0dense_39_60868dense_39_60870*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*/
config_proto

CPU

GPU 2J 8@� *L
fGRE
C__inference_dense_39_layer_call_and_return_conditional_losses_607072"
 dense_39/StatefulPartitionedCall�
IdentityIdentity)dense_39/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
2

Identity�
NoOpNoOp"^conv2d_72/StatefulPartitionedCall"^conv2d_73/StatefulPartitionedCall"^conv2d_74/StatefulPartitionedCall!^dense_38/StatefulPartitionedCall!^dense_39/StatefulPartitionedCall&^sequential_12/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*H
_input_shapes7
5:�����������: : : : : : : : : : : : 2F
!conv2d_72/StatefulPartitionedCall!conv2d_72/StatefulPartitionedCall2F
!conv2d_73/StatefulPartitionedCall!conv2d_73/StatefulPartitionedCall2F
!conv2d_74/StatefulPartitionedCall!conv2d_74/StatefulPartitionedCall2D
 dense_38/StatefulPartitionedCall dense_38/StatefulPartitionedCall2D
 dense_39/StatefulPartitionedCall dense_39/StatefulPartitionedCall2N
%sequential_12/StatefulPartitionedCall%sequential_12/StatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�V
�
__inference__traced_save_62965
file_prefix/
+savev2_conv2d_72_kernel_read_readvariableop-
)savev2_conv2d_72_bias_read_readvariableop/
+savev2_conv2d_73_kernel_read_readvariableop-
)savev2_conv2d_73_bias_read_readvariableop/
+savev2_conv2d_74_kernel_read_readvariableop-
)savev2_conv2d_74_bias_read_readvariableop.
*savev2_dense_38_kernel_read_readvariableop,
(savev2_dense_38_bias_read_readvariableop.
*savev2_dense_39_kernel_read_readvariableop,
(savev2_dense_39_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop'
#savev2_variable_read_readvariableop	)
%savev2_variable_1_read_readvariableop	$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop6
2savev2_adam_conv2d_72_kernel_m_read_readvariableop4
0savev2_adam_conv2d_72_bias_m_read_readvariableop6
2savev2_adam_conv2d_73_kernel_m_read_readvariableop4
0savev2_adam_conv2d_73_bias_m_read_readvariableop6
2savev2_adam_conv2d_74_kernel_m_read_readvariableop4
0savev2_adam_conv2d_74_bias_m_read_readvariableop5
1savev2_adam_dense_38_kernel_m_read_readvariableop3
/savev2_adam_dense_38_bias_m_read_readvariableop5
1savev2_adam_dense_39_kernel_m_read_readvariableop3
/savev2_adam_dense_39_bias_m_read_readvariableop6
2savev2_adam_conv2d_72_kernel_v_read_readvariableop4
0savev2_adam_conv2d_72_bias_v_read_readvariableop6
2savev2_adam_conv2d_73_kernel_v_read_readvariableop4
0savev2_adam_conv2d_73_bias_v_read_readvariableop6
2savev2_adam_conv2d_74_kernel_v_read_readvariableop4
0savev2_adam_conv2d_74_bias_v_read_readvariableop5
1savev2_adam_dense_38_kernel_v_read_readvariableop3
/savev2_adam_dense_38_bias_v_read_readvariableop5
1savev2_adam_dense_39_kernel_v_read_readvariableop3
/savev2_adam_dense_39_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:**
dtype0*�
value�B�*B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB:layer-1/layer-0/_rng/_state_var/.ATTRIBUTES/VARIABLE_VALUEB:layer-1/layer-1/_rng/_state_var/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:**
dtype0*g
value^B\*B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0+savev2_conv2d_72_kernel_read_readvariableop)savev2_conv2d_72_bias_read_readvariableop+savev2_conv2d_73_kernel_read_readvariableop)savev2_conv2d_73_bias_read_readvariableop+savev2_conv2d_74_kernel_read_readvariableop)savev2_conv2d_74_bias_read_readvariableop*savev2_dense_38_kernel_read_readvariableop(savev2_dense_38_bias_read_readvariableop*savev2_dense_39_kernel_read_readvariableop(savev2_dense_39_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop#savev2_variable_read_readvariableop%savev2_variable_1_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop2savev2_adam_conv2d_72_kernel_m_read_readvariableop0savev2_adam_conv2d_72_bias_m_read_readvariableop2savev2_adam_conv2d_73_kernel_m_read_readvariableop0savev2_adam_conv2d_73_bias_m_read_readvariableop2savev2_adam_conv2d_74_kernel_m_read_readvariableop0savev2_adam_conv2d_74_bias_m_read_readvariableop1savev2_adam_dense_38_kernel_m_read_readvariableop/savev2_adam_dense_38_bias_m_read_readvariableop1savev2_adam_dense_39_kernel_m_read_readvariableop/savev2_adam_dense_39_bias_m_read_readvariableop2savev2_adam_conv2d_72_kernel_v_read_readvariableop0savev2_adam_conv2d_72_bias_v_read_readvariableop2savev2_adam_conv2d_73_kernel_v_read_readvariableop0savev2_adam_conv2d_73_bias_v_read_readvariableop2savev2_adam_conv2d_74_kernel_v_read_readvariableop0savev2_adam_conv2d_74_bias_v_read_readvariableop1savev2_adam_dense_38_kernel_v_read_readvariableop/savev2_adam_dense_38_bias_v_read_readvariableop1savev2_adam_dense_39_kernel_v_read_readvariableop/savev2_adam_dense_39_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *8
dtypes.
,2*			2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: : : : @:@:@@:@:
��@:@:@
:
: : : : : ::: : : : : : : @:@:@@:@:
��@:@:@
:
: : : @:@:@@:@:
��@:@:@
:
: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:,(
&
_output_shapes
: : 

_output_shapes
: :,(
&
_output_shapes
: @: 

_output_shapes
:@:,(
&
_output_shapes
:@@: 

_output_shapes
:@:&"
 
_output_shapes
:
��@: 

_output_shapes
:@:$	 

_output_shapes

:@
: 


_output_shapes
:
:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: : 

_output_shapes
:: 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :,(
&
_output_shapes
: : 

_output_shapes
: :,(
&
_output_shapes
: @: 

_output_shapes
:@:,(
&
_output_shapes
:@@: 

_output_shapes
:@:&"
 
_output_shapes
:
��@: 

_output_shapes
:@:$ 

_output_shapes

:@
: 

_output_shapes
:
:, (
&
_output_shapes
: : !

_output_shapes
: :,"(
&
_output_shapes
: @: #

_output_shapes
:@:,$(
&
_output_shapes
:@@: %

_output_shapes
:@:&&"
 
_output_shapes
:
��@: '

_output_shapes
:@:$( 

_output_shapes

:@
: )

_output_shapes
:
:*

_output_shapes
: 
�
�
-__inference_sequential_12_layer_call_fn_61874

inputs
unknown:	
	unknown_0:	
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_12_layer_call_and_return_conditional_losses_604952
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*1
_output_shapes
:�����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_62384

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
MaxPool�
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
-__inference_sequential_12_layer_call_fn_61883
random_flip_1_input
unknown:	
	unknown_0:	
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallrandom_flip_1_inputunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_12_layer_call_and_return_conditional_losses_604952
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*1
_output_shapes
:�����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
1
_output_shapes
:�����������
-
_user_specified_namerandom_flip_1_input
�
�
D__inference_conv2d_72_layer_call_and_return_conditional_losses_62369

inputs8
conv2d_readvariableop_resource: -
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp�
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: *
dtype02
Conv2D/ReadVariableOp�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� *
paddingVALID*
strides
2
Conv2D�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*1
_output_shapes
:����������� 2	
BiasAddb
ReluReluBiasAdd:output:0*
T0*1
_output_shapes
:����������� 2
Reluw
IdentityIdentityRelu:activations:0^NoOp*
T0*1
_output_shapes
:����������� 2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*4
_input_shapes#
!:�����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�

n
H__inference_sequential_11_layer_call_and_return_conditional_losses_61855
resizing_1_input
identity�
resizing_1/resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2
resizing_1/resize/size�
 resizing_1/resize/ResizeBilinearResizeBilinearresizing_1_inputresizing_1/resize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(2"
 resizing_1/resize/ResizeBilinearm
rescaling_1/Cast/xConst*
_output_shapes
: *
dtype0*
valueB
 *���;2
rescaling_1/Cast/xq
rescaling_1/Cast_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *    2
rescaling_1/Cast_1/x�
rescaling_1/mulMul1resizing_1/resize/ResizeBilinear:resized_images:0rescaling_1/Cast/x:output:0*
T0*1
_output_shapes
:�����������2
rescaling_1/mul�
rescaling_1/addAddV2rescaling_1/mul:z:0rescaling_1/Cast_1/x:output:0*
T0*1
_output_shapes
:�����������2
rescaling_1/addq
IdentityIdentityrescaling_1/add:z:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:c _
1
_output_shapes
:�����������
*
_user_specified_nameresizing_1_input
�
L
0__inference_max_pooling2d_74_layer_call_fn_62454

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_605802
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
C__inference_dense_38_layer_call_and_return_conditional_losses_62500

inputs2
matmul_readvariableop_resource:
��@-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������@2
Relum
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������@2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*,
_input_shapes
:�����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:Q M
)
_output_shapes
:�����������
 
_user_specified_nameinputs
�
L
0__inference_max_pooling2d_73_layer_call_fn_62414

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *T
fORM
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_605582
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_60558

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
MaxPool�
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_62464

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
MaxPool�
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
g
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_60536

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
MaxPool�
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
a
E__inference_resizing_1_layer_call_and_return_conditional_losses_60135

inputs
identityk
resize/sizeConst*
_output_shapes
:*
dtype0*
valueB"      2
resize/size�
resize/ResizeBilinearResizeBilinearinputsresize/size:output:0*
T0*1
_output_shapes
:�����������*
half_pixel_centers(2
resize/ResizeBilinear�
IdentityIdentity&resize/ResizeBilinear:resized_images:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
��
�
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_60349

inputs6
(stateful_uniform_rngreadandskip_resource:	
identity��stateful_uniform/RngReadAndSkipD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice�
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������2
strided_slice_1/stack�
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������2
strided_slice_1/stack_1|
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_1/stack_2�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_1^
CastCaststrided_slice_1:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast�
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������2
strided_slice_2/stack�
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
���������2
strided_slice_2/stack_1|
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice_2/stack_2�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice_2b
Cast_1Caststrided_slice_2:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
Cast_1~
stateful_uniform/shapePackstrided_slice:output:0*
N*
T0*
_output_shapes
:2
stateful_uniform/shapeq
stateful_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *|٠�2
stateful_uniform/minq
stateful_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *|٠?2
stateful_uniform/maxz
stateful_uniform/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
stateful_uniform/Const�
stateful_uniform/ProdProdstateful_uniform/shape:output:0stateful_uniform/Const:output:0*
T0*
_output_shapes
: 2
stateful_uniform/Prodt
stateful_uniform/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2
stateful_uniform/Cast/x�
stateful_uniform/Cast_1Caststateful_uniform/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2
stateful_uniform/Cast_1�
stateful_uniform/RngReadAndSkipRngReadAndSkip(stateful_uniform_rngreadandskip_resource stateful_uniform/Cast/x:output:0stateful_uniform/Cast_1:y:0*
_output_shapes
:2!
stateful_uniform/RngReadAndSkip�
$stateful_uniform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2&
$stateful_uniform/strided_slice/stack�
&stateful_uniform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2(
&stateful_uniform/strided_slice/stack_1�
&stateful_uniform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2(
&stateful_uniform/strided_slice/stack_2�
stateful_uniform/strided_sliceStridedSlice'stateful_uniform/RngReadAndSkip:value:0-stateful_uniform/strided_slice/stack:output:0/stateful_uniform/strided_slice/stack_1:output:0/stateful_uniform/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2 
stateful_uniform/strided_slice�
stateful_uniform/BitcastBitcast'stateful_uniform/strided_slice:output:0*
T0	*
_output_shapes
:*

type02
stateful_uniform/Bitcast�
&stateful_uniform/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:2(
&stateful_uniform/strided_slice_1/stack�
(stateful_uniform/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(stateful_uniform/strided_slice_1/stack_1�
(stateful_uniform/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(stateful_uniform/strided_slice_1/stack_2�
 stateful_uniform/strided_slice_1StridedSlice'stateful_uniform/RngReadAndSkip:value:0/stateful_uniform/strided_slice_1/stack:output:01stateful_uniform/strided_slice_1/stack_1:output:01stateful_uniform/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2"
 stateful_uniform/strided_slice_1�
stateful_uniform/Bitcast_1Bitcast)stateful_uniform/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02
stateful_uniform/Bitcast_1�
-stateful_uniform/StatelessRandomUniformV2/algConst*
_output_shapes
: *
dtype0*
value	B :2/
-stateful_uniform/StatelessRandomUniformV2/alg�
)stateful_uniform/StatelessRandomUniformV2StatelessRandomUniformV2stateful_uniform/shape:output:0#stateful_uniform/Bitcast_1:output:0!stateful_uniform/Bitcast:output:06stateful_uniform/StatelessRandomUniformV2/alg:output:0*#
_output_shapes
:���������2+
)stateful_uniform/StatelessRandomUniformV2�
stateful_uniform/subSubstateful_uniform/max:output:0stateful_uniform/min:output:0*
T0*
_output_shapes
: 2
stateful_uniform/sub�
stateful_uniform/mulMul2stateful_uniform/StatelessRandomUniformV2:output:0stateful_uniform/sub:z:0*
T0*#
_output_shapes
:���������2
stateful_uniform/mul�
stateful_uniformAddV2stateful_uniform/mul:z:0stateful_uniform/min:output:0*
T0*#
_output_shapes
:���������2
stateful_uniforms
rotation_matrix/sub/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub/y~
rotation_matrix/subSub
Cast_1:y:0rotation_matrix/sub/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/subu
rotation_matrix/CosCosstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Cosw
rotation_matrix/sub_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_1/y�
rotation_matrix/sub_1Sub
Cast_1:y:0 rotation_matrix/sub_1/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_1�
rotation_matrix/mulMulrotation_matrix/Cos:y:0rotation_matrix/sub_1:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/mulu
rotation_matrix/SinSinstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Sinw
rotation_matrix/sub_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_2/y�
rotation_matrix/sub_2SubCast:y:0 rotation_matrix/sub_2/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_2�
rotation_matrix/mul_1Mulrotation_matrix/Sin:y:0rotation_matrix/sub_2:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/mul_1�
rotation_matrix/sub_3Subrotation_matrix/mul:z:0rotation_matrix/mul_1:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/sub_3�
rotation_matrix/sub_4Subrotation_matrix/sub:z:0rotation_matrix/sub_3:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/sub_4{
rotation_matrix/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2
rotation_matrix/truediv/y�
rotation_matrix/truedivRealDivrotation_matrix/sub_4:z:0"rotation_matrix/truediv/y:output:0*
T0*#
_output_shapes
:���������2
rotation_matrix/truedivw
rotation_matrix/sub_5/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_5/y�
rotation_matrix/sub_5SubCast:y:0 rotation_matrix/sub_5/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_5y
rotation_matrix/Sin_1Sinstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Sin_1w
rotation_matrix/sub_6/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_6/y�
rotation_matrix/sub_6Sub
Cast_1:y:0 rotation_matrix/sub_6/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_6�
rotation_matrix/mul_2Mulrotation_matrix/Sin_1:y:0rotation_matrix/sub_6:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/mul_2y
rotation_matrix/Cos_1Cosstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Cos_1w
rotation_matrix/sub_7/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
rotation_matrix/sub_7/y�
rotation_matrix/sub_7SubCast:y:0 rotation_matrix/sub_7/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/sub_7�
rotation_matrix/mul_3Mulrotation_matrix/Cos_1:y:0rotation_matrix/sub_7:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/mul_3�
rotation_matrix/addAddV2rotation_matrix/mul_2:z:0rotation_matrix/mul_3:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/add�
rotation_matrix/sub_8Subrotation_matrix/sub_5:z:0rotation_matrix/add:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/sub_8
rotation_matrix/truediv_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @2
rotation_matrix/truediv_1/y�
rotation_matrix/truediv_1RealDivrotation_matrix/sub_8:z:0$rotation_matrix/truediv_1/y:output:0*
T0*#
_output_shapes
:���������2
rotation_matrix/truediv_1r
rotation_matrix/ShapeShapestateful_uniform:z:0*
T0*
_output_shapes
:2
rotation_matrix/Shape�
#rotation_matrix/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2%
#rotation_matrix/strided_slice/stack�
%rotation_matrix/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2'
%rotation_matrix/strided_slice/stack_1�
%rotation_matrix/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2'
%rotation_matrix/strided_slice/stack_2�
rotation_matrix/strided_sliceStridedSlicerotation_matrix/Shape:output:0,rotation_matrix/strided_slice/stack:output:0.rotation_matrix/strided_slice/stack_1:output:0.rotation_matrix/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
rotation_matrix/strided_slicey
rotation_matrix/Cos_2Cosstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Cos_2�
%rotation_matrix/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_1/stack�
'rotation_matrix/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_1/stack_1�
'rotation_matrix/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_1/stack_2�
rotation_matrix/strided_slice_1StridedSlicerotation_matrix/Cos_2:y:0.rotation_matrix/strided_slice_1/stack:output:00rotation_matrix/strided_slice_1/stack_1:output:00rotation_matrix/strided_slice_1/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_1y
rotation_matrix/Sin_2Sinstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Sin_2�
%rotation_matrix/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_2/stack�
'rotation_matrix/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_2/stack_1�
'rotation_matrix/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_2/stack_2�
rotation_matrix/strided_slice_2StridedSlicerotation_matrix/Sin_2:y:0.rotation_matrix/strided_slice_2/stack:output:00rotation_matrix/strided_slice_2/stack_1:output:00rotation_matrix/strided_slice_2/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_2�
rotation_matrix/NegNeg(rotation_matrix/strided_slice_2:output:0*
T0*'
_output_shapes
:���������2
rotation_matrix/Neg�
%rotation_matrix/strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_3/stack�
'rotation_matrix/strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_3/stack_1�
'rotation_matrix/strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_3/stack_2�
rotation_matrix/strided_slice_3StridedSlicerotation_matrix/truediv:z:0.rotation_matrix/strided_slice_3/stack:output:00rotation_matrix/strided_slice_3/stack_1:output:00rotation_matrix/strided_slice_3/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_3y
rotation_matrix/Sin_3Sinstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Sin_3�
%rotation_matrix/strided_slice_4/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_4/stack�
'rotation_matrix/strided_slice_4/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_4/stack_1�
'rotation_matrix/strided_slice_4/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_4/stack_2�
rotation_matrix/strided_slice_4StridedSlicerotation_matrix/Sin_3:y:0.rotation_matrix/strided_slice_4/stack:output:00rotation_matrix/strided_slice_4/stack_1:output:00rotation_matrix/strided_slice_4/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_4y
rotation_matrix/Cos_3Cosstateful_uniform:z:0*
T0*#
_output_shapes
:���������2
rotation_matrix/Cos_3�
%rotation_matrix/strided_slice_5/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_5/stack�
'rotation_matrix/strided_slice_5/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_5/stack_1�
'rotation_matrix/strided_slice_5/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_5/stack_2�
rotation_matrix/strided_slice_5StridedSlicerotation_matrix/Cos_3:y:0.rotation_matrix/strided_slice_5/stack:output:00rotation_matrix/strided_slice_5/stack_1:output:00rotation_matrix/strided_slice_5/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_5�
%rotation_matrix/strided_slice_6/stackConst*
_output_shapes
:*
dtype0*
valueB"        2'
%rotation_matrix/strided_slice_6/stack�
'rotation_matrix/strided_slice_6/stack_1Const*
_output_shapes
:*
dtype0*
valueB"        2)
'rotation_matrix/strided_slice_6/stack_1�
'rotation_matrix/strided_slice_6/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2)
'rotation_matrix/strided_slice_6/stack_2�
rotation_matrix/strided_slice_6StridedSlicerotation_matrix/truediv_1:z:0.rotation_matrix/strided_slice_6/stack:output:00rotation_matrix/strided_slice_6/stack_1:output:00rotation_matrix/strided_slice_6/stack_2:output:0*
Index0*
T0*'
_output_shapes
:���������*

begin_mask*
end_mask*
new_axis_mask2!
rotation_matrix/strided_slice_6|
rotation_matrix/zeros/mul/yConst*
_output_shapes
: *
dtype0*
value	B :2
rotation_matrix/zeros/mul/y�
rotation_matrix/zeros/mulMul&rotation_matrix/strided_slice:output:0$rotation_matrix/zeros/mul/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/zeros/mul
rotation_matrix/zeros/Less/yConst*
_output_shapes
: *
dtype0*
value
B :�2
rotation_matrix/zeros/Less/y�
rotation_matrix/zeros/LessLessrotation_matrix/zeros/mul:z:0%rotation_matrix/zeros/Less/y:output:0*
T0*
_output_shapes
: 2
rotation_matrix/zeros/Less�
rotation_matrix/zeros/packed/1Const*
_output_shapes
: *
dtype0*
value	B :2 
rotation_matrix/zeros/packed/1�
rotation_matrix/zeros/packedPack&rotation_matrix/strided_slice:output:0'rotation_matrix/zeros/packed/1:output:0*
N*
T0*
_output_shapes
:2
rotation_matrix/zeros/packed
rotation_matrix/zeros/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *    2
rotation_matrix/zeros/Const�
rotation_matrix/zerosFill%rotation_matrix/zeros/packed:output:0$rotation_matrix/zeros/Const:output:0*
T0*'
_output_shapes
:���������2
rotation_matrix/zeros|
rotation_matrix/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
rotation_matrix/concat/axis�
rotation_matrix/concatConcatV2(rotation_matrix/strided_slice_1:output:0rotation_matrix/Neg:y:0(rotation_matrix/strided_slice_3:output:0(rotation_matrix/strided_slice_4:output:0(rotation_matrix/strided_slice_5:output:0(rotation_matrix/strided_slice_6:output:0rotation_matrix/zeros:output:0$rotation_matrix/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������2
rotation_matrix/concatX
transform/ShapeShapeinputs*
T0*
_output_shapes
:2
transform/Shape�
transform/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB:2
transform/strided_slice/stack�
transform/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2!
transform/strided_slice/stack_1�
transform/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2!
transform/strided_slice/stack_2�
transform/strided_sliceStridedSlicetransform/Shape:output:0&transform/strided_slice/stack:output:0(transform/strided_slice/stack_1:output:0(transform/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:2
transform/strided_sliceq
transform/fill_valueConst*
_output_shapes
: *
dtype0*
valueB
 *    2
transform/fill_value�
$transform/ImageProjectiveTransformV3ImageProjectiveTransformV3inputsrotation_matrix/concat:output:0 transform/strided_slice:output:0transform/fill_value:output:0*1
_output_shapes
:�����������*
dtype0*
	fill_mode	REFLECT*
interpolation
BILINEAR2&
$transform/ImageProjectiveTransformV3�
IdentityIdentity9transform/ImageProjectiveTransformV3:transformed_images:0^NoOp*
T0*1
_output_shapes
:�����������2

Identityp
NoOpNoOp ^stateful_uniform/RngReadAndSkip*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:�����������: 2B
stateful_uniform/RngReadAndSkipstateful_uniform/RngReadAndSkip:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
h
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_62687

inputs
identityd
IdentityIdentityinputs*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
M
1__inference_random_rotation_1_layer_call_fn_62676

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *U
fPRN
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_602132
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
��
�
H__inference_random_flip_1_layer_call_and_return_conditional_losses_62671

inputs?
1stateful_uniform_full_int_rngreadandskip_resource:	
identity��(stateful_uniform_full_int/RngReadAndSkip�*stateful_uniform_full_int_1/RngReadAndSkip�Ostateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�Vstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�Lstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�Sstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�
stateful_uniform_full_int/shapeConst*
_output_shapes
:*
dtype0*
valueB:2!
stateful_uniform_full_int/shape�
stateful_uniform_full_int/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2!
stateful_uniform_full_int/Const�
stateful_uniform_full_int/ProdProd(stateful_uniform_full_int/shape:output:0(stateful_uniform_full_int/Const:output:0*
T0*
_output_shapes
: 2 
stateful_uniform_full_int/Prod�
 stateful_uniform_full_int/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2"
 stateful_uniform_full_int/Cast/x�
 stateful_uniform_full_int/Cast_1Cast'stateful_uniform_full_int/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2"
 stateful_uniform_full_int/Cast_1�
(stateful_uniform_full_int/RngReadAndSkipRngReadAndSkip1stateful_uniform_full_int_rngreadandskip_resource)stateful_uniform_full_int/Cast/x:output:0$stateful_uniform_full_int/Cast_1:y:0*
_output_shapes
:2*
(stateful_uniform_full_int/RngReadAndSkip�
-stateful_uniform_full_int/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2/
-stateful_uniform_full_int/strided_slice/stack�
/stateful_uniform_full_int/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:21
/stateful_uniform_full_int/strided_slice/stack_1�
/stateful_uniform_full_int/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:21
/stateful_uniform_full_int/strided_slice/stack_2�
'stateful_uniform_full_int/strided_sliceStridedSlice0stateful_uniform_full_int/RngReadAndSkip:value:06stateful_uniform_full_int/strided_slice/stack:output:08stateful_uniform_full_int/strided_slice/stack_1:output:08stateful_uniform_full_int/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2)
'stateful_uniform_full_int/strided_slice�
!stateful_uniform_full_int/BitcastBitcast0stateful_uniform_full_int/strided_slice:output:0*
T0	*
_output_shapes
:*

type02#
!stateful_uniform_full_int/Bitcast�
/stateful_uniform_full_int/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:21
/stateful_uniform_full_int/strided_slice_1/stack�
1stateful_uniform_full_int/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int/strided_slice_1/stack_1�
1stateful_uniform_full_int/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int/strided_slice_1/stack_2�
)stateful_uniform_full_int/strided_slice_1StridedSlice0stateful_uniform_full_int/RngReadAndSkip:value:08stateful_uniform_full_int/strided_slice_1/stack:output:0:stateful_uniform_full_int/strided_slice_1/stack_1:output:0:stateful_uniform_full_int/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2+
)stateful_uniform_full_int/strided_slice_1�
#stateful_uniform_full_int/Bitcast_1Bitcast2stateful_uniform_full_int/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02%
#stateful_uniform_full_int/Bitcast_1�
stateful_uniform_full_int/algConst*
_output_shapes
: *
dtype0*
value	B :2
stateful_uniform_full_int/alg�
stateful_uniform_full_intStatelessRandomUniformFullIntV2(stateful_uniform_full_int/shape:output:0,stateful_uniform_full_int/Bitcast_1:output:0*stateful_uniform_full_int/Bitcast:output:0&stateful_uniform_full_int/alg:output:0*
_output_shapes
:*
dtype0	2
stateful_uniform_full_intb

zeros_likeConst*
_output_shapes
:*
dtype0	*
valueB	R 2

zeros_like�
stackPack"stateful_uniform_full_int:output:0zeros_like:output:0*
N*
T0	*
_output_shapes

:2
stack{
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice/stack
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2
strided_slice/stack_1
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice/stack_2�
strided_sliceStridedSlicestack:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice�
3stateless_random_flip_left_right/control_dependencyIdentityinputs*
T0*
_class
loc:@inputs*1
_output_shapes
:�����������25
3stateless_random_flip_left_right/control_dependency�
&stateless_random_flip_left_right/ShapeShape<stateless_random_flip_left_right/control_dependency:output:0*
T0*
_output_shapes
:2(
&stateless_random_flip_left_right/Shape�
4stateless_random_flip_left_right/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 26
4stateless_random_flip_left_right/strided_slice/stack�
6stateless_random_flip_left_right/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:28
6stateless_random_flip_left_right/strided_slice/stack_1�
6stateless_random_flip_left_right/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:28
6stateless_random_flip_left_right/strided_slice/stack_2�
.stateless_random_flip_left_right/strided_sliceStridedSlice/stateless_random_flip_left_right/Shape:output:0=stateless_random_flip_left_right/strided_slice/stack:output:0?stateless_random_flip_left_right/strided_slice/stack_1:output:0?stateless_random_flip_left_right/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask20
.stateless_random_flip_left_right/strided_slice�
?stateless_random_flip_left_right/stateless_random_uniform/shapePack7stateless_random_flip_left_right/strided_slice:output:0*
N*
T0*
_output_shapes
:2A
?stateless_random_flip_left_right/stateless_random_uniform/shape�
=stateless_random_flip_left_right/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2?
=stateless_random_flip_left_right/stateless_random_uniform/min�
=stateless_random_flip_left_right/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2?
=stateless_random_flip_left_right/stateless_random_uniform/max�
Vstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounterstrided_slice:output:0* 
_output_shapes
::2X
Vstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter�
Ostateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgW^stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2Q
Ostateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg�
Rstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2Hstateless_random_flip_left_right/stateless_random_uniform/shape:output:0\stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0`stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0Ustateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2T
Rstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2�
=stateless_random_flip_left_right/stateless_random_uniform/subSubFstateless_random_flip_left_right/stateless_random_uniform/max:output:0Fstateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2?
=stateless_random_flip_left_right/stateless_random_uniform/sub�
=stateless_random_flip_left_right/stateless_random_uniform/mulMul[stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomUniformV2:output:0Astateless_random_flip_left_right/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2?
=stateless_random_flip_left_right/stateless_random_uniform/mul�
9stateless_random_flip_left_right/stateless_random_uniformAddV2Astateless_random_flip_left_right/stateless_random_uniform/mul:z:0Fstateless_random_flip_left_right/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������2;
9stateless_random_flip_left_right/stateless_random_uniform�
0stateless_random_flip_left_right/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :22
0stateless_random_flip_left_right/Reshape/shape/1�
0stateless_random_flip_left_right/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :22
0stateless_random_flip_left_right/Reshape/shape/2�
0stateless_random_flip_left_right/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :22
0stateless_random_flip_left_right/Reshape/shape/3�
.stateless_random_flip_left_right/Reshape/shapePack7stateless_random_flip_left_right/strided_slice:output:09stateless_random_flip_left_right/Reshape/shape/1:output:09stateless_random_flip_left_right/Reshape/shape/2:output:09stateless_random_flip_left_right/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:20
.stateless_random_flip_left_right/Reshape/shape�
(stateless_random_flip_left_right/ReshapeReshape=stateless_random_flip_left_right/stateless_random_uniform:z:07stateless_random_flip_left_right/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2*
(stateless_random_flip_left_right/Reshape�
&stateless_random_flip_left_right/RoundRound1stateless_random_flip_left_right/Reshape:output:0*
T0*/
_output_shapes
:���������2(
&stateless_random_flip_left_right/Round�
/stateless_random_flip_left_right/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:21
/stateless_random_flip_left_right/ReverseV2/axis�
*stateless_random_flip_left_right/ReverseV2	ReverseV2<stateless_random_flip_left_right/control_dependency:output:08stateless_random_flip_left_right/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2,
*stateless_random_flip_left_right/ReverseV2�
$stateless_random_flip_left_right/mulMul*stateless_random_flip_left_right/Round:y:03stateless_random_flip_left_right/ReverseV2:output:0*
T0*1
_output_shapes
:�����������2&
$stateless_random_flip_left_right/mul�
&stateless_random_flip_left_right/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2(
&stateless_random_flip_left_right/sub/x�
$stateless_random_flip_left_right/subSub/stateless_random_flip_left_right/sub/x:output:0*stateless_random_flip_left_right/Round:y:0*
T0*/
_output_shapes
:���������2&
$stateless_random_flip_left_right/sub�
&stateless_random_flip_left_right/mul_1Mul(stateless_random_flip_left_right/sub:z:0<stateless_random_flip_left_right/control_dependency:output:0*
T0*1
_output_shapes
:�����������2(
&stateless_random_flip_left_right/mul_1�
$stateless_random_flip_left_right/addAddV2(stateless_random_flip_left_right/mul:z:0*stateless_random_flip_left_right/mul_1:z:0*
T0*1
_output_shapes
:�����������2&
$stateless_random_flip_left_right/add�
!stateful_uniform_full_int_1/shapeConst*
_output_shapes
:*
dtype0*
valueB:2#
!stateful_uniform_full_int_1/shape�
!stateful_uniform_full_int_1/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2#
!stateful_uniform_full_int_1/Const�
 stateful_uniform_full_int_1/ProdProd*stateful_uniform_full_int_1/shape:output:0*stateful_uniform_full_int_1/Const:output:0*
T0*
_output_shapes
: 2"
 stateful_uniform_full_int_1/Prod�
"stateful_uniform_full_int_1/Cast/xConst*
_output_shapes
: *
dtype0*
value	B :2$
"stateful_uniform_full_int_1/Cast/x�
"stateful_uniform_full_int_1/Cast_1Cast)stateful_uniform_full_int_1/Prod:output:0*

DstT0*

SrcT0*
_output_shapes
: 2$
"stateful_uniform_full_int_1/Cast_1�
*stateful_uniform_full_int_1/RngReadAndSkipRngReadAndSkip1stateful_uniform_full_int_rngreadandskip_resource+stateful_uniform_full_int_1/Cast/x:output:0&stateful_uniform_full_int_1/Cast_1:y:0)^stateful_uniform_full_int/RngReadAndSkip*
_output_shapes
:2,
*stateful_uniform_full_int_1/RngReadAndSkip�
/stateful_uniform_full_int_1/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 21
/stateful_uniform_full_int_1/strided_slice/stack�
1stateful_uniform_full_int_1/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int_1/strided_slice/stack_1�
1stateful_uniform_full_int_1/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int_1/strided_slice/stack_2�
)stateful_uniform_full_int_1/strided_sliceStridedSlice2stateful_uniform_full_int_1/RngReadAndSkip:value:08stateful_uniform_full_int_1/strided_slice/stack:output:0:stateful_uniform_full_int_1/strided_slice/stack_1:output:0:stateful_uniform_full_int_1/strided_slice/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask2+
)stateful_uniform_full_int_1/strided_slice�
#stateful_uniform_full_int_1/BitcastBitcast2stateful_uniform_full_int_1/strided_slice:output:0*
T0	*
_output_shapes
:*

type02%
#stateful_uniform_full_int_1/Bitcast�
1stateful_uniform_full_int_1/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:23
1stateful_uniform_full_int_1/strided_slice_1/stack�
3stateful_uniform_full_int_1/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:25
3stateful_uniform_full_int_1/strided_slice_1/stack_1�
3stateful_uniform_full_int_1/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:25
3stateful_uniform_full_int_1/strided_slice_1/stack_2�
+stateful_uniform_full_int_1/strided_slice_1StridedSlice2stateful_uniform_full_int_1/RngReadAndSkip:value:0:stateful_uniform_full_int_1/strided_slice_1/stack:output:0<stateful_uniform_full_int_1/strided_slice_1/stack_1:output:0<stateful_uniform_full_int_1/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:2-
+stateful_uniform_full_int_1/strided_slice_1�
%stateful_uniform_full_int_1/Bitcast_1Bitcast4stateful_uniform_full_int_1/strided_slice_1:output:0*
T0	*
_output_shapes
:*

type02'
%stateful_uniform_full_int_1/Bitcast_1�
stateful_uniform_full_int_1/algConst*
_output_shapes
: *
dtype0*
value	B :2!
stateful_uniform_full_int_1/alg�
stateful_uniform_full_int_1StatelessRandomUniformFullIntV2*stateful_uniform_full_int_1/shape:output:0.stateful_uniform_full_int_1/Bitcast_1:output:0,stateful_uniform_full_int_1/Bitcast:output:0(stateful_uniform_full_int_1/alg:output:0*
_output_shapes
:*
dtype0	2
stateful_uniform_full_int_1f
zeros_like_1Const*
_output_shapes
:*
dtype0	*
valueB	R 2
zeros_like_1�
stack_1Pack$stateful_uniform_full_int_1:output:0zeros_like_1:output:0*
N*
T0	*
_output_shapes

:2	
stack_1
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB"        2
strided_slice_1/stack�
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       2
strided_slice_1/stack_1�
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      2
strided_slice_1/stack_2�
strided_slice_1StridedSlicestack_1:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
:*

begin_mask*
end_mask*
shrink_axis_mask2
strided_slice_1�
0stateless_random_flip_up_down/control_dependencyIdentity(stateless_random_flip_left_right/add:z:0*
T0*7
_class-
+)loc:@stateless_random_flip_left_right/add*1
_output_shapes
:�����������22
0stateless_random_flip_up_down/control_dependency�
#stateless_random_flip_up_down/ShapeShape9stateless_random_flip_up_down/control_dependency:output:0*
T0*
_output_shapes
:2%
#stateless_random_flip_up_down/Shape�
1stateless_random_flip_up_down/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 23
1stateless_random_flip_up_down/strided_slice/stack�
3stateless_random_flip_up_down/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:25
3stateless_random_flip_up_down/strided_slice/stack_1�
3stateless_random_flip_up_down/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:25
3stateless_random_flip_up_down/strided_slice/stack_2�
+stateless_random_flip_up_down/strided_sliceStridedSlice,stateless_random_flip_up_down/Shape:output:0:stateless_random_flip_up_down/strided_slice/stack:output:0<stateless_random_flip_up_down/strided_slice/stack_1:output:0<stateless_random_flip_up_down/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2-
+stateless_random_flip_up_down/strided_slice�
<stateless_random_flip_up_down/stateless_random_uniform/shapePack4stateless_random_flip_up_down/strided_slice:output:0*
N*
T0*
_output_shapes
:2>
<stateless_random_flip_up_down/stateless_random_uniform/shape�
:stateless_random_flip_up_down/stateless_random_uniform/minConst*
_output_shapes
: *
dtype0*
valueB
 *    2<
:stateless_random_flip_up_down/stateless_random_uniform/min�
:stateless_random_flip_up_down/stateless_random_uniform/maxConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2<
:stateless_random_flip_up_down/stateless_random_uniform/max�
Sstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterStatelessRandomGetKeyCounterstrided_slice_1:output:0P^stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg* 
_output_shapes
::2U
Sstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter�
Lstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgStatelessRandomGetAlgT^stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter*
_output_shapes
: 2N
Lstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg�
Ostateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2StatelessRandomUniformV2Estateless_random_flip_up_down/stateless_random_uniform/shape:output:0Ystateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:key:0]stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:counter:0Rstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg:alg:0*#
_output_shapes
:���������2Q
Ostateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2�
:stateless_random_flip_up_down/stateless_random_uniform/subSubCstateless_random_flip_up_down/stateless_random_uniform/max:output:0Cstateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*
_output_shapes
: 2<
:stateless_random_flip_up_down/stateless_random_uniform/sub�
:stateless_random_flip_up_down/stateless_random_uniform/mulMulXstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomUniformV2:output:0>stateless_random_flip_up_down/stateless_random_uniform/sub:z:0*
T0*#
_output_shapes
:���������2<
:stateless_random_flip_up_down/stateless_random_uniform/mul�
6stateless_random_flip_up_down/stateless_random_uniformAddV2>stateless_random_flip_up_down/stateless_random_uniform/mul:z:0Cstateless_random_flip_up_down/stateless_random_uniform/min:output:0*
T0*#
_output_shapes
:���������28
6stateless_random_flip_up_down/stateless_random_uniform�
-stateless_random_flip_up_down/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2/
-stateless_random_flip_up_down/Reshape/shape/1�
-stateless_random_flip_up_down/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2/
-stateless_random_flip_up_down/Reshape/shape/2�
-stateless_random_flip_up_down/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2/
-stateless_random_flip_up_down/Reshape/shape/3�
+stateless_random_flip_up_down/Reshape/shapePack4stateless_random_flip_up_down/strided_slice:output:06stateless_random_flip_up_down/Reshape/shape/1:output:06stateless_random_flip_up_down/Reshape/shape/2:output:06stateless_random_flip_up_down/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2-
+stateless_random_flip_up_down/Reshape/shape�
%stateless_random_flip_up_down/ReshapeReshape:stateless_random_flip_up_down/stateless_random_uniform:z:04stateless_random_flip_up_down/Reshape/shape:output:0*
T0*/
_output_shapes
:���������2'
%stateless_random_flip_up_down/Reshape�
#stateless_random_flip_up_down/RoundRound.stateless_random_flip_up_down/Reshape:output:0*
T0*/
_output_shapes
:���������2%
#stateless_random_flip_up_down/Round�
,stateless_random_flip_up_down/ReverseV2/axisConst*
_output_shapes
:*
dtype0*
valueB:2.
,stateless_random_flip_up_down/ReverseV2/axis�
'stateless_random_flip_up_down/ReverseV2	ReverseV29stateless_random_flip_up_down/control_dependency:output:05stateless_random_flip_up_down/ReverseV2/axis:output:0*
T0*1
_output_shapes
:�����������2)
'stateless_random_flip_up_down/ReverseV2�
!stateless_random_flip_up_down/mulMul'stateless_random_flip_up_down/Round:y:00stateless_random_flip_up_down/ReverseV2:output:0*
T0*1
_output_shapes
:�����������2#
!stateless_random_flip_up_down/mul�
#stateless_random_flip_up_down/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2%
#stateless_random_flip_up_down/sub/x�
!stateless_random_flip_up_down/subSub,stateless_random_flip_up_down/sub/x:output:0'stateless_random_flip_up_down/Round:y:0*
T0*/
_output_shapes
:���������2#
!stateless_random_flip_up_down/sub�
#stateless_random_flip_up_down/mul_1Mul%stateless_random_flip_up_down/sub:z:09stateless_random_flip_up_down/control_dependency:output:0*
T0*1
_output_shapes
:�����������2%
#stateless_random_flip_up_down/mul_1�
!stateless_random_flip_up_down/addAddV2%stateless_random_flip_up_down/mul:z:0'stateless_random_flip_up_down/mul_1:z:0*
T0*1
_output_shapes
:�����������2#
!stateless_random_flip_up_down/add�
IdentityIdentity%stateless_random_flip_up_down/add:z:0^NoOp*
T0*1
_output_shapes
:�����������2

Identity�
NoOpNoOp)^stateful_uniform_full_int/RngReadAndSkip+^stateful_uniform_full_int_1/RngReadAndSkipP^stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgW^stateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterM^stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgT^stateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:�����������: 2T
(stateful_uniform_full_int/RngReadAndSkip(stateful_uniform_full_int/RngReadAndSkip2X
*stateful_uniform_full_int_1/RngReadAndSkip*stateful_uniform_full_int_1/RngReadAndSkip2�
Ostateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlgOstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetAlg2�
Vstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounterVstateless_random_flip_left_right/stateless_random_uniform/StatelessRandomGetKeyCounter2�
Lstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlgLstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetAlg2�
Sstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounterSstateless_random_flip_up_down/stateless_random_uniform/StatelessRandomGetKeyCounter:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
�
D__inference_conv2d_74_layer_call_and_return_conditional_losses_60659

inputs8
conv2d_readvariableop_resource:@@-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp�
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:@@*
dtype02
Conv2D/ReadVariableOp�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@*
paddingVALID*
strides
2
Conv2D�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<<@2	
BiasAdd`
ReluReluBiasAdd:output:0*
T0*/
_output_shapes
:���������<<@2
Reluu
IdentityIdentityRelu:activations:0^NoOp*
T0*/
_output_shapes
:���������<<@2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*2
_input_shapes!
:���������>>@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:W S
/
_output_shapes
:���������>>@
 
_user_specified_nameinputs
�
a
E__inference_flatten_19_layer_call_and_return_conditional_losses_60677

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"���� �  2
Consti
ReshapeReshapeinputsConst:output:0*
T0*)
_output_shapes
:�����������2	
Reshapef
IdentityIdentityReshape:output:0*
T0*)
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������@:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
�
d
H__inference_sequential_12_layer_call_and_return_conditional_losses_61887

inputs
identityd
IdentityIdentityinputs*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs
�
S
-__inference_sequential_11_layer_call_fn_61800
resizing_1_input
identity�
PartitionedCallPartitionedCallresizing_1_input*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *Q
fLRJ
H__inference_sequential_11_layer_call_and_return_conditional_losses_601482
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:c _
1
_output_shapes
:�����������
*
_user_specified_nameresizing_1_input
�
G
+__inference_rescaling_1_layer_call_fn_62536

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *1
_output_shapes
:�����������* 
_read_only_resource_inputs
 */
config_proto

CPU

GPU 2J 8@� *O
fJRH
F__inference_rescaling_1_layer_call_and_return_conditional_losses_601452
PartitionedCallv
IdentityIdentityPartitionedCall:output:0*
T0*1
_output_shapes
:�����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:�����������:Y U
1
_output_shapes
:�����������
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
]
sequential_11_inputF
%serving_default_sequential_11_input:0�����������<
dense_390
StatefulPartitionedCall:0���������
tensorflow/serving/predict:�
�
layer-0
layer-1
layer_with_weights-0
layer-2
layer-3
layer_with_weights-1
layer-4
layer-5
layer_with_weights-2
layer-6
layer-7
	layer-8

layer_with_weights-3

layer-9
layer_with_weights-4
layer-10
	optimizer
trainable_variables
	variables
regularization_losses
	keras_api

signatures
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses"
_tf_keras_sequential
�
layer-0
layer-1
trainable_variables
	variables
regularization_losses
	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_sequential
�
layer-0
layer-1
trainable_variables
	variables
regularization_losses
	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_sequential
�

kernel
bias
 trainable_variables
!	variables
"regularization_losses
#	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
$trainable_variables
%	variables
&regularization_losses
'	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

(kernel
)bias
*trainable_variables
+	variables
,regularization_losses
-	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
.trainable_variables
/	variables
0regularization_losses
1	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

2kernel
3bias
4trainable_variables
5	variables
6regularization_losses
7	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
8trainable_variables
9	variables
:regularization_losses
;	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
<trainable_variables
=	variables
>regularization_losses
?	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

@kernel
Abias
Btrainable_variables
C	variables
Dregularization_losses
E	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

Fkernel
Gbias
Htrainable_variables
I	variables
Jregularization_losses
K	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
Liter

Mbeta_1

Nbeta_2
	Odecay
Plearning_ratem�m�(m�)m�2m�3m�@m�Am�Fm�Gm�v�v�(v�)v�2v�3v�@v�Av�Fv�Gv�"
	optimizer
f
0
1
(2
)3
24
35
@6
A7
F8
G9"
trackable_list_wrapper
f
0
1
(2
)3
24
35
@6
A7
F8
G9"
trackable_list_wrapper
 "
trackable_list_wrapper
�
trainable_variables
Qmetrics
Rlayer_metrics
Snon_trainable_variables
	variables
regularization_losses

Tlayers
Ulayer_regularization_losses
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
�
Vtrainable_variables
W	variables
Xregularization_losses
Y	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
Ztrainable_variables
[	variables
\regularization_losses
]	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
trainable_variables
^metrics
_layer_metrics
`non_trainable_variables
	variables
regularization_losses

alayers
blayer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
c_rng
dtrainable_variables
e	variables
fregularization_losses
g	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
h_rng
itrainable_variables
j	variables
kregularization_losses
l	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
trainable_variables
mmetrics
nlayer_metrics
onon_trainable_variables
	variables
regularization_losses

players
qlayer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
*:( 2conv2d_72/kernel
: 2conv2d_72/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
 trainable_variables
rmetrics
slayer_metrics
tnon_trainable_variables
!	variables
"regularization_losses

ulayers
vlayer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
$trainable_variables
wmetrics
xlayer_metrics
ynon_trainable_variables
%	variables
&regularization_losses

zlayers
{layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
*:( @2conv2d_73/kernel
:@2conv2d_73/bias
.
(0
)1"
trackable_list_wrapper
.
(0
)1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
*trainable_variables
|metrics
}layer_metrics
~non_trainable_variables
+	variables
,regularization_losses

layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
.trainable_variables
�metrics
�layer_metrics
�non_trainable_variables
/	variables
0regularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
*:(@@2conv2d_74/kernel
:@2conv2d_74/bias
.
20
31"
trackable_list_wrapper
.
20
31"
trackable_list_wrapper
 "
trackable_list_wrapper
�
4trainable_variables
�metrics
�layer_metrics
�non_trainable_variables
5	variables
6regularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
8trainable_variables
�metrics
�layer_metrics
�non_trainable_variables
9	variables
:regularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
<trainable_variables
�metrics
�layer_metrics
�non_trainable_variables
=	variables
>regularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
#:!
��@2dense_38/kernel
:@2dense_38/bias
.
@0
A1"
trackable_list_wrapper
.
@0
A1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Btrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
C	variables
Dregularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
!:@
2dense_39/kernel
:
2dense_39/bias
.
F0
G1"
trackable_list_wrapper
.
F0
G1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Htrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
I	variables
Jregularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
0
�0
�1"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
n
0
1
2
3
4
5
6
7
	8

9
10"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Vtrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
W	variables
Xregularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Ztrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
[	variables
\regularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
/
�
_state_var"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
dtrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
e	variables
fregularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
/
�
_state_var"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
itrainable_variables
�metrics
�layer_metrics
�non_trainable_variables
j	variables
kregularization_losses
�layers
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
R

�total

�count
�	variables
�	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
:	2Variable
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
:	2Variable
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
/:- 2Adam/conv2d_72/kernel/m
!: 2Adam/conv2d_72/bias/m
/:- @2Adam/conv2d_73/kernel/m
!:@2Adam/conv2d_73/bias/m
/:-@@2Adam/conv2d_74/kernel/m
!:@2Adam/conv2d_74/bias/m
(:&
��@2Adam/dense_38/kernel/m
 :@2Adam/dense_38/bias/m
&:$@
2Adam/dense_39/kernel/m
 :
2Adam/dense_39/bias/m
/:- 2Adam/conv2d_72/kernel/v
!: 2Adam/conv2d_72/bias/v
/:- @2Adam/conv2d_73/kernel/v
!:@2Adam/conv2d_73/bias/v
/:-@@2Adam/conv2d_74/kernel/v
!:@2Adam/conv2d_74/bias/v
(:&
��@2Adam/dense_38/kernel/v
 :@2Adam/dense_38/bias/v
&:$@
2Adam/dense_39/kernel/v
 :
2Adam/dense_39/bias/v
�2�
-__inference_sequential_23_layer_call_fn_61062
-__inference_sequential_23_layer_call_fn_61087
-__inference_sequential_23_layer_call_fn_61116
-__inference_sequential_23_layer_call_fn_61145�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
 __inference__wrapped_model_60122sequential_11_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
H__inference_sequential_23_layer_call_and_return_conditional_losses_61195
H__inference_sequential_23_layer_call_and_return_conditional_losses_61470
H__inference_sequential_23_layer_call_and_return_conditional_losses_61520
H__inference_sequential_23_layer_call_and_return_conditional_losses_61795�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
-__inference_sequential_11_layer_call_fn_61800
-__inference_sequential_11_layer_call_fn_61805
-__inference_sequential_11_layer_call_fn_61810
-__inference_sequential_11_layer_call_fn_61815�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
H__inference_sequential_11_layer_call_and_return_conditional_losses_61825
H__inference_sequential_11_layer_call_and_return_conditional_losses_61835
H__inference_sequential_11_layer_call_and_return_conditional_losses_61845
H__inference_sequential_11_layer_call_and_return_conditional_losses_61855�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
-__inference_sequential_12_layer_call_fn_61860
-__inference_sequential_12_layer_call_fn_61865
-__inference_sequential_12_layer_call_fn_61874
-__inference_sequential_12_layer_call_fn_61883�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
H__inference_sequential_12_layer_call_and_return_conditional_losses_61887
H__inference_sequential_12_layer_call_and_return_conditional_losses_62116
H__inference_sequential_12_layer_call_and_return_conditional_losses_62120
H__inference_sequential_12_layer_call_and_return_conditional_losses_62349�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
)__inference_conv2d_72_layer_call_fn_62358�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_conv2d_72_layer_call_and_return_conditional_losses_62369�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
0__inference_max_pooling2d_72_layer_call_fn_62374
0__inference_max_pooling2d_72_layer_call_fn_62379�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_62384
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_62389�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_conv2d_73_layer_call_fn_62398�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_conv2d_73_layer_call_and_return_conditional_losses_62409�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
0__inference_max_pooling2d_73_layer_call_fn_62414
0__inference_max_pooling2d_73_layer_call_fn_62419�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_62424
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_62429�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_conv2d_74_layer_call_fn_62438�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_conv2d_74_layer_call_and_return_conditional_losses_62449�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
0__inference_max_pooling2d_74_layer_call_fn_62454
0__inference_max_pooling2d_74_layer_call_fn_62459�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_62464
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_62469�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_flatten_19_layer_call_fn_62474�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_flatten_19_layer_call_and_return_conditional_losses_62480�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
(__inference_dense_38_layer_call_fn_62489�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_dense_38_layer_call_and_return_conditional_losses_62500�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
(__inference_dense_39_layer_call_fn_62509�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_dense_39_layer_call_and_return_conditional_losses_62520�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference_signature_wrapper_61037sequential_11_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_resizing_1_layer_call_fn_62525�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_resizing_1_layer_call_and_return_conditional_losses_62531�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
+__inference_rescaling_1_layer_call_fn_62536�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
F__inference_rescaling_1_layer_call_and_return_conditional_losses_62544�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
-__inference_random_flip_1_layer_call_fn_62549
-__inference_random_flip_1_layer_call_fn_62556�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
H__inference_random_flip_1_layer_call_and_return_conditional_losses_62560
H__inference_random_flip_1_layer_call_and_return_conditional_losses_62671�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
1__inference_random_rotation_1_layer_call_fn_62676
1__inference_random_rotation_1_layer_call_fn_62683�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_62687
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_62809�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p

kwonlyargs� 
kwonlydefaults� 
annotations� *
 �
 __inference__wrapped_model_60122�
()23@AFGF�C
<�9
7�4
sequential_11_input�����������
� "3�0
.
dense_39"�
dense_39���������
�
D__inference_conv2d_72_layer_call_and_return_conditional_losses_62369p9�6
/�,
*�'
inputs�����������
� "/�,
%�"
0����������� 
� �
)__inference_conv2d_72_layer_call_fn_62358c9�6
/�,
*�'
inputs�����������
� ""������������ �
D__inference_conv2d_73_layer_call_and_return_conditional_losses_62409l()7�4
-�*
(�%
inputs���������~~ 
� "-�*
#� 
0���������||@
� �
)__inference_conv2d_73_layer_call_fn_62398_()7�4
-�*
(�%
inputs���������~~ 
� " ����������||@�
D__inference_conv2d_74_layer_call_and_return_conditional_losses_62449l237�4
-�*
(�%
inputs���������>>@
� "-�*
#� 
0���������<<@
� �
)__inference_conv2d_74_layer_call_fn_62438_237�4
-�*
(�%
inputs���������>>@
� " ����������<<@�
C__inference_dense_38_layer_call_and_return_conditional_losses_62500^@A1�.
'�$
"�
inputs�����������
� "%�"
�
0���������@
� }
(__inference_dense_38_layer_call_fn_62489Q@A1�.
'�$
"�
inputs�����������
� "����������@�
C__inference_dense_39_layer_call_and_return_conditional_losses_62520\FG/�,
%�"
 �
inputs���������@
� "%�"
�
0���������

� {
(__inference_dense_39_layer_call_fn_62509OFG/�,
%�"
 �
inputs���������@
� "����������
�
E__inference_flatten_19_layer_call_and_return_conditional_losses_62480b7�4
-�*
(�%
inputs���������@
� "'�$
�
0�����������
� �
*__inference_flatten_19_layer_call_fn_62474U7�4
-�*
(�%
inputs���������@
� "�������������
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_62384�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
K__inference_max_pooling2d_72_layer_call_and_return_conditional_losses_62389j9�6
/�,
*�'
inputs����������� 
� "-�*
#� 
0���������~~ 
� �
0__inference_max_pooling2d_72_layer_call_fn_62374�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
0__inference_max_pooling2d_72_layer_call_fn_62379]9�6
/�,
*�'
inputs����������� 
� " ����������~~ �
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_62424�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
K__inference_max_pooling2d_73_layer_call_and_return_conditional_losses_62429h7�4
-�*
(�%
inputs���������||@
� "-�*
#� 
0���������>>@
� �
0__inference_max_pooling2d_73_layer_call_fn_62414�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
0__inference_max_pooling2d_73_layer_call_fn_62419[7�4
-�*
(�%
inputs���������||@
� " ����������>>@�
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_62464�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
K__inference_max_pooling2d_74_layer_call_and_return_conditional_losses_62469h7�4
-�*
(�%
inputs���������<<@
� "-�*
#� 
0���������@
� �
0__inference_max_pooling2d_74_layer_call_fn_62454�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
0__inference_max_pooling2d_74_layer_call_fn_62459[7�4
-�*
(�%
inputs���������<<@
� " ����������@�
H__inference_random_flip_1_layer_call_and_return_conditional_losses_62560p=�:
3�0
*�'
inputs�����������
p 
� "/�,
%�"
0�����������
� �
H__inference_random_flip_1_layer_call_and_return_conditional_losses_62671t�=�:
3�0
*�'
inputs�����������
p
� "/�,
%�"
0�����������
� �
-__inference_random_flip_1_layer_call_fn_62549c=�:
3�0
*�'
inputs�����������
p 
� ""�������������
-__inference_random_flip_1_layer_call_fn_62556g�=�:
3�0
*�'
inputs�����������
p
� ""�������������
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_62687p=�:
3�0
*�'
inputs�����������
p 
� "/�,
%�"
0�����������
� �
L__inference_random_rotation_1_layer_call_and_return_conditional_losses_62809t�=�:
3�0
*�'
inputs�����������
p
� "/�,
%�"
0�����������
� �
1__inference_random_rotation_1_layer_call_fn_62676c=�:
3�0
*�'
inputs�����������
p 
� ""�������������
1__inference_random_rotation_1_layer_call_fn_62683g�=�:
3�0
*�'
inputs�����������
p
� ""�������������
F__inference_rescaling_1_layer_call_and_return_conditional_losses_62544l9�6
/�,
*�'
inputs�����������
� "/�,
%�"
0�����������
� �
+__inference_rescaling_1_layer_call_fn_62536_9�6
/�,
*�'
inputs�����������
� ""�������������
E__inference_resizing_1_layer_call_and_return_conditional_losses_62531l9�6
/�,
*�'
inputs�����������
� "/�,
%�"
0�����������
� �
*__inference_resizing_1_layer_call_fn_62525_9�6
/�,
*�'
inputs�����������
� ""�������������
H__inference_sequential_11_layer_call_and_return_conditional_losses_61825tA�>
7�4
*�'
inputs�����������
p 

 
� "/�,
%�"
0�����������
� �
H__inference_sequential_11_layer_call_and_return_conditional_losses_61835tA�>
7�4
*�'
inputs�����������
p

 
� "/�,
%�"
0�����������
� �
H__inference_sequential_11_layer_call_and_return_conditional_losses_61845~K�H
A�>
4�1
resizing_1_input�����������
p 

 
� "/�,
%�"
0�����������
� �
H__inference_sequential_11_layer_call_and_return_conditional_losses_61855~K�H
A�>
4�1
resizing_1_input�����������
p

 
� "/�,
%�"
0�����������
� �
-__inference_sequential_11_layer_call_fn_61800qK�H
A�>
4�1
resizing_1_input�����������
p 

 
� ""�������������
-__inference_sequential_11_layer_call_fn_61805gA�>
7�4
*�'
inputs�����������
p 

 
� ""�������������
-__inference_sequential_11_layer_call_fn_61810gA�>
7�4
*�'
inputs�����������
p

 
� ""�������������
-__inference_sequential_11_layer_call_fn_61815qK�H
A�>
4�1
resizing_1_input�����������
p

 
� ""�������������
H__inference_sequential_12_layer_call_and_return_conditional_losses_61887tA�>
7�4
*�'
inputs�����������
p 

 
� "/�,
%�"
0�����������
� �
H__inference_sequential_12_layer_call_and_return_conditional_losses_62116z��A�>
7�4
*�'
inputs�����������
p

 
� "/�,
%�"
0�����������
� �
H__inference_sequential_12_layer_call_and_return_conditional_losses_62120�N�K
D�A
7�4
random_flip_1_input�����������
p 

 
� "/�,
%�"
0�����������
� �
H__inference_sequential_12_layer_call_and_return_conditional_losses_62349���N�K
D�A
7�4
random_flip_1_input�����������
p

 
� "/�,
%�"
0�����������
� �
-__inference_sequential_12_layer_call_fn_61860tN�K
D�A
7�4
random_flip_1_input�����������
p 

 
� ""�������������
-__inference_sequential_12_layer_call_fn_61865gA�>
7�4
*�'
inputs�����������
p 

 
� ""�������������
-__inference_sequential_12_layer_call_fn_61874m��A�>
7�4
*�'
inputs�����������
p

 
� ""�������������
-__inference_sequential_12_layer_call_fn_61883z��N�K
D�A
7�4
random_flip_1_input�����������
p

 
� ""�������������
H__inference_sequential_23_layer_call_and_return_conditional_losses_61195v
()23@AFGA�>
7�4
*�'
inputs�����������
p 

 
� "%�"
�
0���������

� �
H__inference_sequential_23_layer_call_and_return_conditional_losses_61470z��()23@AFGA�>
7�4
*�'
inputs�����������
p

 
� "%�"
�
0���������

� �
H__inference_sequential_23_layer_call_and_return_conditional_losses_61520�
()23@AFGN�K
D�A
7�4
sequential_11_input�����������
p 

 
� "%�"
�
0���������

� �
H__inference_sequential_23_layer_call_and_return_conditional_losses_61795���()23@AFGN�K
D�A
7�4
sequential_11_input�����������
p

 
� "%�"
�
0���������

� �
-__inference_sequential_23_layer_call_fn_61062v
()23@AFGN�K
D�A
7�4
sequential_11_input�����������
p 

 
� "����������
�
-__inference_sequential_23_layer_call_fn_61087i
()23@AFGA�>
7�4
*�'
inputs�����������
p 

 
� "����������
�
-__inference_sequential_23_layer_call_fn_61116m��()23@AFGA�>
7�4
*�'
inputs�����������
p

 
� "����������
�
-__inference_sequential_23_layer_call_fn_61145z��()23@AFGN�K
D�A
7�4
sequential_11_input�����������
p

 
� "����������
�
#__inference_signature_wrapper_61037�
()23@AFG]�Z
� 
S�P
N
sequential_11_input7�4
sequential_11_input�����������"3�0
.
dense_39"�
dense_39���������
